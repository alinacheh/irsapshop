<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Авторизация");
?>
<div class="container">
	<?if(!$USER->IsAuthorized()){
		$APPLICATION->IncludeComponent(
			"bitrix:system.auth.form",
			"main",
			Array(
				"REGISTER_URL" => SITE_DIR."auth/registration/",
				"FORGOT_PASSWORD_URL" => SITE_DIR."auth/forgot-password/",
				"PROFILE_URL" => SITE_DIR."personal/",
				"SHOW_ERRORS" => "Y"
			)
		);
	}elseif( !empty( $_REQUEST["backurl"] ) ){
		LocalRedirect( $_REQUEST["backurl"] );
	}else{
		LocalRedirect(SITE_DIR.'personal/');
	}?>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>