<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Смена пароля");?>

<div class="container">
    <?
    if(!$USER->IsAuthorized()){$APPLICATION->IncludeComponent( "bitrix:system.auth.changepasswd", "main", array('AUTH_RESULT' => $APPLICATION->arAuthResult, 'AUTH_URL' => SITE_DIR.'auth/'), false );}
    elseif( !empty( $_REQUEST["backurl"] ) ){ LocalRedirect( $_REQUEST["backurl"] );}
    else{ LocalRedirect(SITE_DIR.'personal/');}
    ?>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>