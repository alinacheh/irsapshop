<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Регистрация"); ?>

<div class="container">
    <?$APPLICATION->IncludeComponent("bitrix:main.register", "main", Array(
        "USER_PROPERTY_NAME" => "",	// Название блока пользовательских свойств
            "SEF_MODE" => "Y",
            "SHOW_FIELDS" => array(	// Поля, которые показывать в форме
                0 => "LOGIN",
                1 => "EMAIL",
                2 => "PASSWORD",
                3 => "CONFIRM_PASSWORD",
            ),
            "REQUIRED_FIELDS" => array(	// Поля, обязательные для заполнения
            ),
            "AUTH" => "Y",	// Автоматически авторизовать пользователей
            "USE_BACKURL" => "Y",	// Отправлять пользователя по обратной ссылке, если она есть
            "SUCCESS_PAGE" => "",	// Страница окончания регистрации
            "SET_TITLE" => "N",	// Устанавливать заголовок страницы
            "USER_PROPERTY" => "",	// Показывать доп. свойства
            "SEF_FOLDER" => "/",
            "VARIABLE_ALIASES" => "",
            "AUTH_URL" => "/auth/"
        ),
        false
    );
    ?>
</div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>