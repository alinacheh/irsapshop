<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "Официальный интернет-магазин стальных радиаторов Irsap");
$APPLICATION->SetPageProperty("description", "Официальный интернет-магазин стальных радиаторов Irsap. Широкий ассортимент радиаторов отопления Ирсап. Итальянское качество. Выгодные цены. Доставка и самовывоз.");
$APPLICATION->SetTitle("Официальный интернет-магазин стальных радиаторов Irsap");

include($_SERVER["DOCUMENT_ROOT"]."/include/index/slider.php");
include($_SERVER["DOCUMENT_ROOT"]."/include/index/about.php");
include($_SERVER["DOCUMENT_ROOT"]."/include/index/popular.php");
include($_SERVER["DOCUMENT_ROOT"]."/include/index/special.php");
include($_SERVER["DOCUMENT_ROOT"]."/include/index/warranty.php");
include($_SERVER["DOCUMENT_ROOT"]."/include/index/instock.php");
include($_SERVER["DOCUMENT_ROOT"]."/include/index/italy.php");
include($_SERVER["DOCUMENT_ROOT"]."/include/index/design.php");
include($_SERVER["DOCUMENT_ROOT"]."/include/index/colors.php");
include($_SERVER["DOCUMENT_ROOT"]."/include/index/news.php");
include($_SERVER["DOCUMENT_ROOT"]."/include/index/history.php");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>