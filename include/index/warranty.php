<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();?>

<?$APPLICATION->IncludeComponent(
	"irsap:variable.set", 
	"warranty_index", 
	array(
		"COMPONENT_TEMPLATE" => "warranty_index",
		"header" => "Гарантия 20 лет",
		"text" => "Производитель радиаторов IRSAP проинформировал об увеличении срока гарантии на радиаторы Tesi ГОСТ Р, который теперь составляет 20 лет и на остальную продукцию 2 года с даты продажи покупателю.

Компания IRSAP — известный производитель качественных тепловых приборов. За более чем полувековую история компания заняла значительное место на международном рынке отопительного оборудования.

Радиаторы и полотенцесушители IRSAP отличаются высоким качеством и прекрасными внешними характеристиками.

Вся продукция производится в Италии на современном оборудовании под строгим контролем качества.",
		"pic" => "/local/templates/irsap/images/garant_img.png",
		"bg" => "/local/templates/irsap/images/garant-des_bg.png",
		"pic_title" => "TITLE картинки"
	),
	false
);?>