<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();?>

<?$APPLICATION->IncludeComponent(
	"irsap:variable.set", 
	"colors_index", 
	array(
		"COMPONENT_TEMPLATE" => "colors_index",
		"header" => "Океан цветов",
		"text" => "На сегодняшний день есть более 500 000 разных вариантов исполнения радиаторов Irsap, способных удовлетворить потребности каждого клиента. Компания Irsap постоянно увеличивает модельный ряд, находится в поиске новых цветовых решений, фактур, форм и размеров.",
		"bg" => "/local/templates/irsap/images/ocean-des_bg.png"
	),
	false
);?>