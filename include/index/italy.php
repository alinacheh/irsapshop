<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();?>

<?$APPLICATION->IncludeComponent(
	"irsap:variable.set", 
	"italy_index", 
	array(
		"COMPONENT_TEMPLATE" => "italy_index",
		"header" => "Итальянский бренд",
		"text" => "Компания IRSAP была основана в 1963 году в г. Аркуа Полезине провинции Ровиго. Компания начала свою деятельность с производства радиаторов из штамповочной стали. Спустя три года, в 1967г. Irsap выпустила TESI. В то время трубчатые радиаторы были еще неизвестны в Италии, поэтому компания стала первой на рынке, тем самым предвосхищая будущий успех преимущественно многоколончатых радиаторов.",
		"bg" => "/local/templates/irsap/images/Layer 19.png"
	),
	false
);?>