<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "Гарантия на радиаторы отопления Irsap");
$APPLICATION->SetPageProperty("description", "Гарантия на радиаторы отопления Irsap. Широкий ассортимент радиаторов отопления Irsap. Итальянское качество. Выгодные цены. Доставка и самовывоз.");
$APPLICATION->SetTitle("Гарантия на радиаторы отопления Irsap");
?><section class="garant sec-padding__null">
<?$APPLICATION->IncludeComponent(
	"irsap:variable.set",
	"top_banner",
	Array(
		"COMPONENT_TEMPLATE" => "top_banner",
		"image" => "/local/templates/irsap/images/garant-banner.png",
		"image_alt_attr" => "ALT фоновой картинки",
		"image_title_attr" => "TITLE фоновой картинки",
		"mobile_image" => "/local/templates/irsap/images/mob-garant-banner.png",
		"title" => "Гарантия"
	)
);?> </section> <section class="garant-info__box section-box">
<div class="container">
	<div class="garant-info">
		<div class="garant-info__banner flex49">
 <img src="/local/templates/irsap/images/garant-info__banner.png" alt="" class="garant-info__img">
		</div>
		<div class="garant-info__text flex49">
			<h2 class="garant-info__title">Гарантия на товар</h2>
			<div class="garant-info__desp">
				<p>
					 Компания Irsapshop является официальным представителем изготовителей оборудования, размещенного в нашем каталоге. Каждому покупателю обеспечивается заводская гарантия на приобретенную продукцию. Каждый радиатор застрахован на 10 000 000 евро европейской страховой компанией, которая имеет официальное представительство на территории РФ.
				</p>
				<p>
					 При покупке отопительного оборудования необходимо учесть, что гарантия сохраняется при условии, что его монтаж осуществляется сертифицированными специалистами. Если установка производилась компанией, которая не имеет лицензии на осуществление такой деятельности, гарантия является недействительной. Это обуславливается тем, что при монтаже могут быть не соблюдены действия, предусмотренные инструкцией.
				</p>
				<p>
					 Гарантийные случаи рассматриваются в авторизованных сервисных центрах, которые обычно указываются в паспорте изделия.
				</p>
				<div class="garant-info__list-title text-bold">
					 Файлы для скачивания:
				</div>
				<ul>
					<li class="garant-list__item list-li__red"> <a href="#">Рамочный договор поставки</a> </li>
					<li class="garant-list__item list-li__red"> <a href="#">Учетная карточка предприятия</a> </li>
				</ul>
			</div>
		</div>
	</div>
</div>
 </section><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>