<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "Доставка и оплата в интернет-магазине Irsap");
$APPLICATION->SetPageProperty("description", "Доставка и оплата в интернет-магазине Irsap. Широкий ассортимент радиаторов отопления Irsap. Итальянское качество. Выгодные цены. Доставка и самовывоз.");
$APPLICATION->SetTitle("Доставка и оплата в интернет-магазине Irsap");
?><section class="ship-pay sec-padding__null">
<?$APPLICATION->IncludeComponent(
	"irsap:variable.set",
	"top_banner",
	Array(
		"COMPONENT_TEMPLATE" => "top_banner",
		"image" => "/local/templates/irsap/images/ship-pay__banner.png",
		"image_alt_attr" => "ALT фоновой картинки",
		"image_title_attr" => "TITLE фоновой картинки",
		"mobile_image" => "/local/templates/irsap/images/mob-ship-pay__banner.png",
		"title" => "Доставка и оплата"
	)
);?> </section> <section class="section-box">
<div class="container">
	<div class="shipping-payment__box">
		<div class="shipping flex49">
			<h2 class="shippng-title">Условия доставки</h2>
			<div class="shipping-payment__info">
				<p>
					 Компания Irsapshop имеет собственный отдел доставки. Четко налаженный механизм работы позволяет получить заказ всегда вовремя, оставляя довольными наших покупателей. Курьеры-профессионалы предупредят о времени доставки заранее своим звонком. Четко, быстро и профессионально мы доставим Ваш заказ!
				</p>
				<p>
					 Внимание! При заказе товара от 10000 рублей доставка осуществляется бесплатно.
				</p>
				<p class="text-bold">
					 Отдел доставки компании Irsapshop работает ежедневно:
				</p>
				<ul>
					<li class="list-li__red">c 10:00 до 21:00 пн-пт</li>
					<li class="list-li__red">с 10:00 до 16:00 сб, вс</li>
				</ul>
				<p class="text-bold">
					 Доставка по Москве и области осуществляется в течении 1-2 дней после оформления заказа.
				</p>
				<ul>
					<li class="list-li__red">Стоимость доставки - 700 руб. </li>
					<li class="list-li__red">Доставка за МКАД - 30 руб/км</li>
					<li class="list-li__red">
					Доставка осуществляется в течение дня в удобное для вас время </li>
				</ul>
				<p>
 <span class="text-bold">Внимание!</span> Доставка осуществляется по Москве и Московской области до подъезда или до места, куда может беспрепятственно подъехать грузовой автомобиль и где можно беспрепятственно произвести выгрузку товара, а так же до пунктов приема груза транспортными компаниями, осуществляющими перевозку в регионы. Сотрудники компании НЕ выполняют обязанности грузчиков и НЕ перемещают доставленный товар от места выгрузки. Выгрузка и дальнейшая транспортировка товара осуществляется силами покупателя.
				</p>
				<p class="hide-text">
					 Если осуществляется доставка на закрытую для въезда территорию, для получения товара встретьте курьера у ворот или заранее оформите ему пропуск, разрешающий въезд. В данном случае все расходы по оформлению необходимых для допуска документов берет на себя покупатель.
				</p>
				<p class="hide-text">
					 Если заказ был оформлен от имени юридического лица, для получения товара необходимо предоставить доверенность от организации или печать. Без необходимых документов или печати отгрузка товаров не производится. Повторная доставка является платной услугой и оплачивается отдельно.
				</p>
 <a href="#" class="view-block style_btn2 hidden-des">Читать далее</a>
			</div>
			<div class="shipping-payment__info">
				<h3 class="h3 shippng-info__title">Заказ общим весом:</h3>
				<ul>
					<li class="list-li__red">
					до 12 кг— доставляется до квартиры; </li>
					<li class="list-li__red">
					от 12 до 20 кг— доставка до подъезда. Подъем нашим курьером на лифте - 300 руб. за единицу. </li>
					<li class="list-li__red">
					от 20 до 50 кг— наш курьер доставит до подъезда и поможет сгрузить с машины. </li>
				</ul>
				<p>
					 Подъем предварительно можно согласовать с Вашим менеджером. более 50 кг— разгрузка силами заказчика. Если в доме не работает лифт или невозможно поместить в него крупногабаритный товар, — разгрузка силами заказчика.
				</p>
			</div>
			<div class="shipping-payment__info">
				<h3 class="h3 shippng-info__title">Возможно ли приобретение товара под заказ?</h3>
				<p>
					 Статус товара "Под заказ" означает, что магазин готов принять заказ и осуществить доставку товара в течение согласованного с покупателем срока, не превышающего двух месяцев (за исключением товаров, изготавливаемых на заказ, ориентировочный срок поставки которых оговаривается с покупателем во время заказа), при условии предоплаты от 30% до 100% по предварительному согласованию с менеджерами нашего магазина.
				</p>
			</div>
			<div class="shipping-payment__info">
				<h3 class="h3 shippng-info__title">Доставка в регионы РФ</h3>
				<p>
					 Заказывая в нашей компании доставку по России, вы можете быть полностью уверенными в том, что получите свой заказ в целости и точно в срок. Перевозка оборудования в регионы осуществляется нашими проверенными партнерами — транспортными компаниями, которые обязуются доставить любой заказ в предельно сжатые сроки. Товар в регионы отправляется только после 100% предоплаты за оборудование. Доставка оплачивается отдельно и производится непосредственно транспортной компании (как правило, по факту прихода в Ваш регион товара).
				</p>
				<p class="hide-text">
					 Наша компания производит тщательный контроль каждого заказа перед тем, как он будет отгружен в транспортную компанию (проверяется комплектность, качество и надежность упаковки).
				</p>
				<p class="hide-text">
					 Заказы в регионы РФ отправляются следующими транспортными компаниями: ПЭК, Автотрейдинг, Деловые линии, Желдорэкспедиция, Байкал Сервис, Кит. При необходимости вы можете выбрать любую другую транспортную компанию, заранее уведомив об этом наших сотрудников отдела продаж.
				</p>
				<p class="hide-text">
					 С условиями работы, сроками и стоимостью грузоперевозок вы можете ознакомиться на сайте выбранной вами транспортной компании.
				</p>
 <a href="#" class="view-block style_btn2 hidden-des">Читать далее</a>
			</div>
			<div class="shipping-payment__info">
				<h3 class="h3 shippng-info__title">Самостоятельное получение заказов (самовывоз)</h3>
				<p>
					 Irsapshop предлагает Вам выгодную услугу — самовывоз товара. Просто выберите удобное время и сами заберите свою покупку.
				</p>
			</div>
			<div class="shipping-payment__info">
 <img src="/local/templates/irsap/images/ship-pay__maket.png" alt="" class="shippng-info__img">
			</div>
		</div>
		<div class="payment flex49">
			<h2 class="payment-title">Условия оплаты</h2>
			<div class="shipping-payment__info">
				<p>
					 Вы можете выбрать один из трёх вариантов оплаты:
				</p>
			</div>
			<div class="shipping-payment__info">
				<h3 class="h3 payment-info__title">Оплата наличными</h3>
				<p>
					 При выборе варианта оплаты наличными, вы дожидаетесь приезда курьера и передаёте ему сумму за товар в рублях. Курьер предоставляет товар, который можно осмотреть на предмет повреждений, соответствие указанным условиям. Покупатель подписывает товаросопроводительные документы, вносит денежные средства и получает чек.
				</p>
				<p>
					 Также оплата наличными доступна при самовывозе из магазина.
				</p>
			</div>
			<div class="shipping-payment__info">
				<h3 class="h3 payment-info__title">Безналичный расчёт</h3>
				<p>
					 Вы можете оплатить заказ в любом банке, для этого при оформлении заказа выберите способ оплаты квитанцией в любом банке. Срок зачисления: 1-3 дня
				</p>
				<div class="payment-info__props">
					<p class="text-bold">
						 Реквизиты для оплаты:
					</p>
					<div class="rekv-info">
						<p>
							 ООО «БИЛАР»
						</p>
						<p>
							 ОГРН: 5137746097650
						</p>
						<p>
							 ИНН:7717769089 КПП: 771501001
						</p>
						<p>
							 Р/с 40702810602290003090 в АО «Альфа-банк» г. Химки
						</p>
						<p>
							 К/с 30101810200000000593
						</p>
						<p>
							 БИК 044525593
						</p>
					</div>
				</div>
			</div>
			<div class="shipping-payment__info">
				<h3 class="h3 payment-info__title">Оплата банковскими картами</h3>
				<p>
					 Для оплаты вы можете воспользоваться одной из электронных платёжных систем:
				</p>
				<div class="payment-info__type">
					<div class="payment-type">
 <img src="/local/templates/irsap/images/payments.png" alt="" class="payment-type__img">
					</div>
				</div>
			</div>
			<div class="shipping-payment__info">
				<h3 class="h3 payment-info__title">Возврат и обмен</h3>
				<p>
					 Согласно закону "О защите прав потребителей" потребитель вправе отказаться от товара в любое время до его передачи, если товар не имеет специальных свойств, которые могут быть использованы только данным потребителем. После передачи товара, согласно Постановлению Правительства РФ № 55 от 19.01.1998 г, Возврат и обмен товара надлежащего качества возможен в течение 14 дней с момента покупки, при условии что сохранены его товарный вид и потребительские свойства ( в соответствии с Постановлением от 27 сентября 2007 г. N 612 Правительства РФ "Об утверждении Правил продажи товаров дистанционным способом" и Законом РФ от 7 февраля 1992 г. N 2300-I "О защите прав потребителей").
				</p>
				<p>
					 Возврат и обмен единицы товара производится только при условии:
				</p>
			</div>
			<div class="shipping-payment__info">
				<ul class="payments-info">
					<li class="list-li__red">
					Товар не имеет механических, термических, цветовых и иных изменений и полностью сохранил свой первоначальный внешний вид; </li>
					<li class="list-li__red">Непосредственно на товаре сохранены все ярлыки, бирки, ценники и т.д.</li>
					<li class="list-li__red">Сохранена упаковка товара;</li>
					<li class="list-li__red">Товар не использовался;</li>
					<li class="list-li__red">При товаре в наличии вся документация, которая шла в комплекте</li>
					<li class="list-li__red">Наличие документов, подтверждающих факт покупки у нас</li>
				</ul>
			</div>
		</div>
	</div>
</div>
 </section><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>