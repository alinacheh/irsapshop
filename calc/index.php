<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "Расчет радиатора Irsap");
$APPLICATION->SetPageProperty("description", "Расчет радиатора Irsap. Широкий ассортимент радиаторов отопления Irsap. Итальянское качество. Выгодные цены. Доставка и самовывоз.");
$APPLICATION->SetTitle("Калькулятор расчета радиаторов Irsap");
?>

<section class="sec-padding__null">
    <?$APPLICATION->IncludeComponent(
        "irsap:variable.set",
        "top_banner",
        array(
            "COMPONENT_TEMPLATE" => "top_banner",
            "title" => "Расчет радиатора",
            "image" => "/local/templates/irsap/images/calc.png",
            "mobile_image" => "/local/templates/irsap/images/mob-calc.png",
            "image_title_attr" => "TITLE фоновой картинки",
            "image_alt_attr" => "ALT фоновой картинки"
        ),
        false
    );?>
</section>

<section class="calc">
    <?$APPLICATION->IncludeComponent(
        "irsap:radiator.calc",
        "new2",
        array(
            "COMPONENT_TEMPLATE" => "new2",
            "IBLOCK_ID" => "1",
        ),
        false
    );?>
</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
