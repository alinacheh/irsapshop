<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Сервисная страница");

use Bitrix\Main\Loader;
use Bitrix\Main\Application;
use Bitrix\Catalog\Model\Price;

global $USER;

if (!$USER->IsAdmin()) {
    die();
}

$messages = $errors = [];
$request = Application::getInstance()->getContext()->getRequest();

if ($request->isPost() && check_bitrix_sessid()) {
    
    $action = $request->get('action');
    
    switch ($action) {
        
        case 'update_prices':
        
            $file = $request->getFile('file');
            if (empty($file['size'])) {
                $errors[] = 'Файл не выбран';
                break;
            }
            
            $handle = fopen($file['tmp_name'], "r");
            if (!$handle) {
                $errors[] = 'Ошибка загрузки файла';
                break;
            }
            
            Loader::includeModule('iblock');
            Loader::includeModule('catalog');
            
            $row = 0;
            $updated = 0;
            $catalogGroupId = 1;
            
            while (($data = fgetcsv($handle, 1000, ";")) !== false) {
                
                $row++;
                $artnumber = trim($data[7] ?? '');
                $price = (float) str_replace(',', '.', $data[8] ?? '');
                $currency = trim($data[9] ?? 'EUR');            
			
                if (!$artnumber || !$price) {
                    $errors[] = "Строка {$row}: Некорректные значения артикула или цены";
                    continue;
                }
                
                $elements = CIBlockElement::GetList(
                    ['ID' => 'DESC'],
                    ['IBLOCK_ID' => CATALOG_OFFERS_ID, 'PROPERTY_ARTNUMBER' => $artnumber],
                    false,
                    false,
                    ['ID', 'IBLOCK_ID', 'PROPERTY_ARTNUMBER',]
                );
                
                $founded = 0;
                
                while ($element = $elements->GetNext()) {
                    
					//echo "<pre>";
					//var_dump($element);
					//echo "</pre>";


                    $founded++;                         
                    
                    $arPrice = Price::getList([
                        'filter' => ['PRODUCT_ID' => $element['ID'], 'CATALOG_GROUP_ID' => $catalogGroupId],
                        'select' => ['ID', 'CATALOG_GROUP_ID']
                    ])->fetch();
                    
                    if (!$arPrice) {
                        $errors[] = "Строка {$row}: Цена не найдена в базе"; 
                        continue;
                    }
                    
                    $result = Price::update($arPrice['ID'], [
                        'PRICE' => $price,
                        'CATALOG_GROUP_ID' => $arPrice['CATALOG_GROUP_ID'],
                        'CURRENCY' => $currency,
                    ]);
                    
                    if ($result->isSuccess()) {
                        $updated++;
                    } else {
                        $errors[] = "Строка {$row}: Ошибка при обновлении цены ({$result->getErrorMessages()})";   
                    }
                
                }
                
                if (!$founded) {
                    $errors[] = "Строка {$row}: По артикулу \"{$artnumber}\" не найдено ни одного товара";
                }
            }
            
            CIBlock::clearIblockTagCache(CATALOG_OFFERS_ID);
            
            $messages[] = "Обработано товаров: {$updated}";
        
            break;
    }
}

?>

<div class="container" style="padding: 20px 0"><?php
        if (!empty($errors)) {
            foreach ($errors as $error) {
                ShowError($error);
            }
        }
        if (!empty($messages)) {
            foreach ($messages as $message) {
                ShowNote($message);
            }
        }
    ?>
    <h2>Обновление цен</h2>
    <p><i>Файл должен быть в формате CSV (разделитель - точка с запятой, кодировка - UTF-8). 
        <br/>Каждая строка должна содержать 3 столбца: Артикул, Цена (именно 1-го ТОРГОВОГО ПРЕДЛОЖЕНИЯ), Валюта (евро - "EUR", рубли - "RUB")
    </i></p>
    <form method="post" accept-charset="utf-8" enctype="multipart/form-data">
    	<?=bitrix_sessid_post()?>
    	<input type="hidden" name="action" value="update_prices"/>
    	<input type="file" name="file" accept=".csv" />
    	<input type="submit" value="Отправить" />
    </form>
</div>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>