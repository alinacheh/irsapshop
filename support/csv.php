<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Сервисная страница");

use Bitrix\Main\Loader;
use Bitrix\Main\Application;
use Bitrix\Catalog\Model\Price;

global $USER;

if (!$USER->IsAdmin()) {
    die();
}

$request = Application::getInstance()->getContext()->getRequest();

if ($request->isPost() && check_bitrix_sessid()) {
    
    $action = $request->get('action');
    
    switch ($action) {
        
        case 'csv_prices':		

			Loader::includeModule('iblock');
			Loader::includeModule('catalog');
			
			$row = 0;
			$catalogGroupId = 1;
							
			//$arSectionName = [];
			$arSectionNav = [];
			$rsSect = \CIBlockSection::GetList(array('left_margin' => 'asc'), array(
			  'IBLOCK_ID' => CATALOG_OFFERS_ID,
			  "<=LEFT_BORDER" => $thisSection["LEFT_MARGIN"],
			  ">=RIGHT_BORDER" => $thisSection["RIGHT_MARGIN"],
			  "<DEPTH_LEVEL" => $thisSection["DEPTH_LEVEL"]
			));
			while($arSect = $rsSect->Fetch())
			{
				//$arSectionName[$arSect['ID']] = $arSect['NAME'];
				$nav = CIBlockSection::GetNavChain(false, $arSect['ID']);
				while($arItem = $nav->Fetch()){
					$arSectionNav[$arSect['ID']][] = $arItem["NAME"];
				}
			} 


			//echo "<pre>";
			//var_dump($arSectionName);
			//echo "</pre>";

			//echo "<pre>";
			//var_dump($arSectionNav);
			//echo "</pre>";			
				
			//$artnumber = "RR 6 1800 30 01 A4 25 N";
			$elements = CIBlockElement::GetList(
				['ID' => 'DESC'],
				//['IBLOCK_ID' => CATALOG_OFFERS_ID, 'ACTIVE' => 'Y', 'PROPERTY_ARTNUMBER' => $artnumber],
				['IBLOCK_ID' => CATALOG_OFFERS_ID, 'ACTIVE' => 'Y'],
				false,
				["nTopCount" => 10000],
				['ID', 'IBLOCK_ID', 'ACTIVE', 'PROPERTY_ARTNUMBER', 'IBLOCK_SECTION_ID', 'NAME', 'PROPERTY_TIP_PODCKLUCHENIA',
				'PROPERTY_VISOTA', 'PROPERTY_KOLICHESTVO_SEKCIY']
			);
			
			$arElementCSV = [];
			//раздел;подраздел;группа;название;тип подключения;высота;количество секций;артикул;цена
			/*
			$arElementCSV[0]['section1'] = 'раздел';
			$arElementCSV[0]['section2'] = 'подраздел';
			$arElementCSV[0]['section3'] = 'группа';
			$arElementCSV[0]['name'] = 'название';
			$arElementCSV[0]['tip'] = 'тип подключения';
			$arElementCSV[0]['visota'] = 'высота';
			$arElementCSV[0]['sekciy'] = 'количество секций';
			$arElementCSV[0]['art'] = 'артикул';
			$arElementCSV[0]['price'] = 'цена';
			$arElementCSV[0]['currency'] = 'валюта';
			*/
			
			while ($element = $elements->GetNext()) {
				
				$arPrice = Price::getList([
					'filter' => ['PRODUCT_ID' => $element['ID'], 'CATALOG_GROUP_ID' => $catalogGroupId],
					'select' => ['ID', 'CATALOG_GROUP_ID', 'PRICE', 'CURRENCY']
				])->fetch();
				
				$arElementCSV[$element['ID']]['section1'] = $arSectionNav[$element['IBLOCK_SECTION_ID']][0];
				$arElementCSV[$element['ID']]['section2'] = $arSectionNav[$element['IBLOCK_SECTION_ID']][1];
				$arElementCSV[$element['ID']]['section3'] = $arSectionNav[$element['IBLOCK_SECTION_ID']][2];
				$arElementCSV[$element['ID']]['name'] = $element['NAME'];
				$arElementCSV[$element['ID']]['tip'] = $element['PROPERTY_TIP_PODCKLUCHENIA_VALUE'];
				$arElementCSV[$element['ID']]['visota'] = $element['PROPERTY_VISOTA_VALUE'];
				$arElementCSV[$element['ID']]['sekciy'] = $element['PROPERTY_KOLICHESTVO_SEKCIY_VALUE'];
				$arElementCSV[$element['ID']]['art'] = $element['PROPERTY_ARTNUMBER_VALUE'];
				$arElementCSV[$element['ID']]['price'] = $arPrice['PRICE'];
				$arElementCSV[$element['ID']]['currency'] = $arPrice['CURRENCY']; 		
			}


			require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/classes/general/csv_data.php");
			$filePath = $_SERVER['DOCUMENT_ROOT'] . '/support/' . 'csv_new.csv';
			$fp = fopen($filePath, 'w+');
			@fclose($fp);
			
			$fields_type = 'R'; //дописываем строки в файл
			$delimiter = ";";   //разделитель для csv-файла
			$csvFile = new \CCSVData($fields_type, false);
			$csvFile->SetFieldsType($fields_type);
			$csvFile->SetDelimiter($delimiter);
			$csvFile->SetFirstHeader(true);
			
			$arrHeaderCSV = array("раздел","подраздел","группа","название","тип подключения", "высота", "количество секций", "артикул", "цена", "валюта");
			$csvFile->SaveFile($filePath, $arrHeaderCSV);
			
			foreach ($arElementCSV as $row)
			{			
				$arrHeaderCSV = array($row['section1'], $row['section2'], $row['section3'], $row['name'], $row['tip'], 
					$row['visota'], $row['sekciy'], $row['art'], $row['price'], $row['currency']);
				
				$csvFile->SaveFile($filePath, $arrHeaderCSV);	
			}

			//echo "<pre>";
			//var_dump($arElementCSV);
			//echo "</pre>";         
			
		break;
    }
}
?>


<div class="container" style="padding: 20px 0">
    <h2>Получить актуальную выгрузку цен</h2>
    <p><i>Файл в формате CSV (разделитель - точка с запятой, кодировка - UTF-8). 
        <br/>Каждая строка = 10 столбцов: 
		<br/>"раздел","подраздел","группа","название","тип подключения", "высота", "количество секций", "артикул", "цена"(именно 1-го ТОРГОВОГО ПРЕДЛОЖЕНИЯ), "валюта" (евро - "EUR", рубли - "RUB")
		
    </i></p>
	<?if($action != 'csv_prices'){?>
		<form method="post" accept-charset="utf-8" enctype="multipart/form-data">
			<?=bitrix_sessid_post()?>
			<input type="hidden" name="action" value="csv_prices"/>    	
			<input type="submit" value="Получить ссылку на актуальную выгрузку цен, ждать 5-20 секунд" />
		</form>
	<?}?>
		
		
	<?if($action == 'csv_prices'){?>
		<a href="/support/csv_new.csv" style="border-bottom: 2px solid #bdbdbd;
    margin-left: 8px;
    font-size: 20px;">Скачать файл CSV от <?=date('Y-m-d H:i:s')?></a>
	<?}?>        
	
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>