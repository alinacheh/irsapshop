<div class="contact-requisite__item">
	<div class="contactm-title text-bold">
		 ООО «БИЛАР»
	</div>
	<div class="contact-requisite__info hide-text">
		<p class="contact-requisite__text">
			 Полное наименование предприятия: <span class="text-bold">Общество с ограниченной ответственностью «БИЛАР»</span>
		</p>
		<p class="contact-requisite__text">
			 Сокращенное наименование предприятия:<span class="text-bold">ООО «БИЛАР»</span>
		</p>
		<p class="contact-requisite__text">
			 Юридический адрес:<span class="text-bold"> 127410, г. Москва, ул. Инженерная, д.15, кв.43</span>
		</p>
		<p class="contact-requisite__text">
			 Фактический адрес: <span class="text-bold">127410, г. Москва, ул. Инженерная, д.15, кв.43</span>
		</p>
		<p class="contact-requisite__text">
			 Почтовый адрес: <span class="text-bold">127410, г. Москва, ул. Инженерная, д.15, кв.43</span>
		</p>
		<p class="contact-requisite__text">
			 ИНН:<span class="text-bold">7717769089</span>
		</p>
		<p class="contact-requisite__text">
			 КПП: <span class="text-bold">771501001</span>
		</p>
		<p class="contact-requisite__text">
			 ОГРН: <span class="text-bold">5137746097650</span>
		</p>
		<p class="contact-requisite__text">
			 ОКПО: <span class="text-bold">20752446</span>
		</p>
		<p class="contact-requisite__text">
			 Расчетный счет: <span class="text-bold">40702810602290003090</span>
		</p>
		<p class="contact-requisite__text">
			 Наименование и место нахождения банка в АО «Альфа-банк»
		</p>
		<p class="contact-requisite__text">
			 Корреспондентский счет: <span class="text-bold">30101810200000000593</span>
		</p>
		<p class="contact-requisite__text">
			 БИК:<span class="text-bold">044525593</span>
		</p>
		<p class="contact-requisite__text">
			 Генеральный директор: <span class="text-bold">Марат Харисович Сафиуллин</span>
		</p>
		<p class="contact-requisite__text">
			 Главный бухгалтер: <span class="text-bold">Марат Харисович Сафиуллин</span>
		</p>
	</div>
 <a href="#" class="view-block style_btn2 hidden-des">Читать далее</a>
</div>