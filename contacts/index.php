<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "Контакты интернет-магазина Irsap");
$APPLICATION->SetPageProperty("description", "Контакты интернет-магазина Irsap. Широкий ассортимент радиаторов отопления Irsap. Итальянское качество. Выгодные цены. Доставка и самовывоз.");
$APPLICATION->SetTitle("Контакты интернет-магазина Irsap");
?>
<section class="garant sec-padding__null">
    <?$APPLICATION->IncludeComponent(
        "irsap:variable.set",
        "top_banner",
        array(
            "COMPONENT_TEMPLATE" => "top_banner",
            "title" => "Контакты",
            "image" => "/local/templates/irsap/images/ship-pay__banner.png",
            "mobile_image" => "/local/templates/irsap/images/mob-ship-pay__banner.png",
            "image_title_attr" => "TITLE фоновой картинки",
            "image_alt_attr" => "ALT фоновой картинки"
        ),
        false
    );?>
</section>

<section class="section-box contact-page">
    <div class="container">
        <div class="contact-box">
            <div class="contact-store flex49">
                <div class="contact-store__item">
                    <h2 class="bold textleft">Контакты магазина</h2>
                    <?$APPLICATION->IncludeComponent(
	"irsap:variable.set", 
	"address_contacts", 
	array(
		"COMPONENT_TEMPLATE" => "address_contacts",
		"address" => "Москва, Нахимовский проспект, д.24, ЦДиИ EXPOSTROY",
		"stores" => array(
			0 => "Павильон 3, 1 этаж, блок 29D, место 456",
		)
	),
	false
);?>
                    <?$APPLICATION->IncludeComponent(
	"irsap:variable.set", 
	"contacts_contacts", 
	array(
		"COMPONENT_TEMPLATE" => "contacts_contacts",
		"phones" => array(
			0 => "8 (800) <span class=\"text-bold\">600-61-49</span>",
			1 => "8 (985) <span class=\"text-bold\">299-77-66</span>",
			2 => "",
		),
		"emails" => array(
			0 => "info@irsapshop.ru",
			1 => "",
		),
		"schedules" => array(
			0 => "<span class=\"text-bold\">10:00 - 19:30</span> без выходных",
			1 => "",
		)
	),
	false
);?>
                    <div class="contact-store__info maps">
                        {{MAP_SCRIPT}}
                        <a href="" class="maps__link"><img src="<?=SITE_TEMPLATE_PATH;?>/images/maps__icon.png" alt="" class="maps__icon">открыть на картах</a>
                    </div>
                </div>
                <div class="contact-store__item">
                    <?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => "route.inc.php",
                            "EDIT_TEMPLATE" => "standard.php"
                        )
                    );?>
                </div>
            </div>
            <div class="contact-requisite flex49">
                <h2 class="bold textleft">Реквизиты</h2>
                <?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
                        "AREA_FILE_SHOW" => "file",
                        "PATH" => "requisits.inc.php",
                        "EDIT_TEMPLATE" => "standard.php"
                    )
                );?>
                <div class="contact-store__item">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:form",
                        "callback",
                        array(
                            "AJAX_MODE" => "Y",
                            "SEF_MODE" => "N",
                            "WEB_FORM_ID" => 1,
                            "RESULT_ID" => $_REQUEST["RESULT_ID"],
                            "START_PAGE" => "new",
                            "SHOW_LIST_PAGE" => "N",
                            "SHOW_EDIT_PAGE" => "N",
                            "SHOW_VIEW_PAGE" => "N",
                            "SUCCESS_URL" => "",
                            "SUCCESS_TEXT" => "Ваше сообщение успешно отправлено. Наши менеджеры ответят Вам в ближайшее время.",
                            "SHOW_ANSWER_VALUE" => "Y",
                            "SHOW_ADDITIONAL" => "Y",
                            "SHOW_STATUS" => "Y",
                            "EDIT_ADDITIONAL" => "Y",
                            "EDIT_STATUS" => "Y",
                            "NOT_SHOW_FILTER" => array(
                                0 => "",
                                1 => "",
                            ),
                            "NOT_SHOW_TABLE" => array(
                                0 => "",
                                1 => "",
                            ),
                            "CHAIN_ITEM_TEXT" => "",
                            "CHAIN_ITEM_LINK" => "",
                            "IGNORE_CUSTOM_TEMPLATE" => "Y",
                            "USE_EXTENDED_ERRORS" => "Y",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "3600",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "AJAX_OPTION_HISTORY" => "N",
                            "SEF_FOLDER" => "/communication/web-forms/",
                            "AJAX_OPTION_ADDITIONAL" => "",
                            "VARIABLE_ALIASES" => array(
                                "action" => "action",
                            ),
                            "PRIVACY_LINK" => "/privacy/",
                        ),
                        false
                    );?>
                </div>

            </div>

        </div>
    </div>
</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
