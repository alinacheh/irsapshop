<h2 class="contact-store__title">Как нас найти</h2>
<div class="contact-store__info">
    <div class="contact-store__text text-bold">На машине:</div>
    <p class="contact-store__text">С ТТК повернуть на Севастопольский проспект и ехать прямо 15 минут до ЦДиИ EXPOSTROY, который будет по правую сторону по ходу движения.</p>
</div>
<div class="contact-store__info">
    <br/><br/>
    <div class="contact-store__text text-bold">Пешком:</div>
    <ol class="list">
        <li class="">Наиболее просто и быстро – от ст. <span class="text-bold">м. Нахимовский проспект.</span></li>
        <li class="">Выход со ст. м. Нахимовский проспект, последний вагон из центра.</li>
        <li class="">От турникетов направо, в сторону Нахимовского проспекта д.9.</li>
        <li class="">Из метро идти прямо до пешеходного перехода, по светофору перейти на противоположную сторону.</li>
        <li class="">Идти к остановкам в сторону Севастопольского проспекта.</li>
        <li class="">Далее на <span class="text-bold">автобусе № 219, 487, 908</span> или <span class="text-bold">троллейбусе №52</span> до остановки Севастопольский проспект.</li>
    </ol>
</div>
<div class="contact-store__info">
    <img src="<?=SITE_TEMPLATE_PATH;?>/images/contact-store__img.png" alt="" class="contact-store__img">
    <p class="contact-store__text">Остановка находится прямо у здания ЦДиИ EXPOSTROY.</p>
</div>