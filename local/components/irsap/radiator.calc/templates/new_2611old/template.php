<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

?>
<div id="calc-wrap" data-entity="calc">
    <div class="calc-head">
        <div class="container">
            <div class="flex_center_between title-calc">
                <div class="flex50"><h2>Данные для расчета</h2></div>
                <div class="flex50 hidden-mob" data-entity="offer-header" style="display: none;"><h2>Подходящая модель</h2></div>
            </div>
        </div>
    </div>
    <div class="calc-body">
        <div class="container">
            <div class="flex_stretch_between calc-wrap">
                <div class="flex50">
                    <div class="calc-box__item calc-column">
                        <div class="calc-box__pay flex_center_start">
                            <label for="" class="calc-box__pay-label">Модель радиатора:</label>
                            <select data-entity="model-select" class="select_style model-rad-select" onchange="calcComponent.changeModel(this.options[this.selectedIndex].value)">
                                <option value="0">Выберите модель...</option>
                                <?foreach ($arResult['collections'] as $collection):?>
                                    <option value="<?=$collection['ID'];?>"><?=$collection['NAME'];?></option>
                                <?endforeach;?>
                            </select>
                        </div>
                        <div class="calc-box__pay flex_center_start" >
                            <label for="" class="calc-box__pay-label">Высота радиатора:</label>
                            <select data-entity="height-select" class="select_style char-rad" disabled onchange="calcComponent.calculateSections()">
                                <option value="0">Выберите высоту...</option>
                            </select>
                            <span class="unit">мм</span>
                        </div>
                        <div class="calc-box__pay flex_center_start">
                            <label for="" class="calc-box__pay-label">Площадь помещения</label>
                            <input type="text" class="input_style char-pom" id="room-square" placeholder="10" data-entity="room-square-input" onchange="calcComponent.calculatePower()">
                            <span class="unit">м<sup><small>2</small></sup></span>
                        </div>
                        <div class="calc-box__pay flex_center_start">
                            <label for="" class="calc-box__pay-label">Высота потолка:</label>
                            <input type="text" class="input_style char-pom" id="room-height" placeholder="260" data-entity="room-height-input" onchange="calcComponent.calculatePower()"><span class="unit">см</span>
                        </div>
                        <div class="calc-box__pay flex_start">
                            <label for="" class="calc-box__pay-label label-count">
                                Количество радиаторов в комнате:
                                <span class="calc-box__text">(количество стояков в Вашей комнате или количество окон)</span>
                            </label>
                            <select class="select_style char-pom" data-entity="room-number-input" onchange="calcComponent.calculatePower()">
                                <?foreach (range(1, 10) as $digit):?>
                                    <option value="<?=$digit;?>"><?=$digit;?></option>
                                <?endforeach;?>
                            </select>
                            <span class="unit">шт</span>

                        </div>
                    </div>
                    <div class="calc-box__total flex_start">
                        <div class="total-power">
                            <div class="total-power__name">Мощность (ватт)</div>
                            <div class="total-power__value" data-entity="total-power">-</div>
                        </div>
                        <div class="total-section">
                            <div class="total-section__value" data-entity="total-section">-</div>
                            <div class="total-section__name">Кол-во секций<br>одного радиатора</div>
                        </div>
                    </div>
                </div>
                <div class="flex50">
                    <div class="calc-box__item calc-box__model" data-entity="offer-block" style="display: none;">
                        <div class="calc-total__order">
                            <div class="calc-total__desp">
                                <div class="total-value__text">
                                    <span class="value-name">Артикул:</span>
                                    <span class="value-text" data-entity="artnumber"></span>
                                </div>
                                <div class="total-value__text" data-entity="values-block"></div>
                                <div class="total-value__text">
                                    <span class="value-name">Цвет: </span><span class="value-text vert-line" data-entity="color-name"></span>
                                    <span class="value-name">RAL: </span><span class="value-text vert-line" data-entity="color-ral"></span>
                                    <span class="value-name">Код: </span><span class="value-text vert-line" data-entity="color-code"></span>
                                    <span class="value-name">Серия: </span><span class="value-text vert-line" data-entity="color-series"></span>
                                    <span class="value-name">Покрытие: </span><span class="value-text" data-entity="color-coating"></span>
                                </div>
                            </div>
                            <div class="calc-total__tags" data-entity="labels-block"></div>
                        </div>
                        <div class="calc-total__result">
                            <div class="flex_center_between">
                                <div class="count flex_center_start">
                                    <div class="kolst">Кол-во шт.:</div>
                                    <select name="" id="" class="select_style" data-entity="item-quantity" onchange="calcComponent.updatePrice()">
                                        <? $maxCount = 10;
                                        for ($i = 1; $i <= $maxCount; $i++):?>
                                            <option value="<?=$i;?>"><?=$i;?></option>
                                        <?endfor;?>
                                    </select>
                                </div>
                                <div class="total-result__price">
                                    <span class="price-text">Цена:</span>
                                    <span class="price"><span data-entity="price"></span>&nbsp;<span class="currency">руб</span></span>
                                </div>
                            </div>
                            <div class="flex_center_between">
                                <div class="popup-link total-result__stock">
                                    <span class="color-help__link color-default" data-entity="in-stock" style="display: none;">
                                        <span><img src="<?=SITE_TEMPLATE_PATH;?>/images/nal.png" alt=""></span>В наличии
                                    </span>
                                    <span class="color-help__link color-default" data-entity="out-stock" style="display: none;">
                                        Под заказ
                                    </span>
                                </div>
                                <div class="total-result__btn">
                                    <a href="javascript:void(0)" data-entity="cart-btn" class="style_btn3">Добавить в заказ</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="calc-box__item" data-entity="offer-stub">
                        <img src="<?=SITE_TEMPLATE_PATH;?>/images/cacl_bg.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php

$jsParams = [
    'blockSelector' => '#calc-wrap',
    'signedParameters' => $this->getComponent()->getSignedParameters(),
    'constants' => [
        'in_stock' => STOCK_IN_STOCK,
    ],
];

?>
    <script>
        var calcComponent = new JCIrsapCalcComponent(<?=CUtil::PhpToJSObject($jsParams, false, true);?>);
    </script>
<?php

unset($jsParams);

