// JCIrsapCalcComponent - компонент для обработки логики расчета радиаторов

(function() {
    'use strict';

    if (!!window.JCIrsapCalcComponent)
        return;

    if (!window.BX)
        return;

    window.JCIrsapCalcComponent = function (params) {
        this.blockSelector = params.blockSelector;
        this.constants = params.constants;
        this.signedParameters = params.signedParameters;
        this.modelId = null;
        this.offerId = null;
        this.colorId = null;
        this.modelsCache = {};
        this.colorsCache = {};
        this.totalPower = null;
        this.totalSections = null;
        this.initHandlers();
    };

    window.JCIrsapCalcComponent.prototype = {

        entity: function (entity, selector) {
            if (!entity) {
                return null;
            }
            return $(selector || this.blockSelector).find('[data-entity="' + entity + '"]');
        },

        initHandlers : function () {
            var self = this;

            $(this.blockSelector).on('click', '[data-entity="cart-btn"]', function(e) {
                e.preventDefault();
                e.stopPropagation();
                self.addToBasket();
            });
        },

        addToBasket : function() {
            var self = this;
            var btn = self.entity('cart-btn');

            var quantity = parseInt(this.entity('item-quantity').val());
            if (!quantity) {
                return;
            }

            $.post(
                '/local/templates/irsap/ajax/actions.php',
                {
                    action : 'addToBasket',
                    id: this.offerId,
                    quantity: quantity,
                    colorId: this.colorId
                },
                function (data) {
                    if (data.successful) {
                        BX.onCustomEvent('OnBasketChange');
                        if (!btn) {
                            return;
                        }
                        btn.text('Добавлено');
                        btn.addClass('cart-btn-added');
                        setTimeout(function () {
                            btn.text('Добавить в заказ');
                            btn.removeClass('cart-btn-added');
                        }, 1000);
                    }
                },
                'json'
            );
        },

        changeModel : function (modelId) {
            this.modelId = parseInt(modelId) || null;
            this.clearOffer();

            if (!this.modelId) {
                this.resetModel();
                return;
            }

            if (!this.modelsCache[this.modelId]) {
                this.fetchModel(this.modelId);
                return;
            }

            this.updateModel(this.modelId);
        },

        fetchModel : function (modelId) {
            var self = this;
            var request = BX.ajax.runComponentAction('irsap:radiator.calc', 'getModel', {
                mode: 'class',
                signedParameters: this.signedParameters,
                data: {
                    id: modelId,
                }
            });

            request.then(function (response) {
                if (!response || response.status != 'success') {
                    alert('Произола ошибка! Попробуйте еще раз выбрать модель');
                }
                self.modelsCache[modelId] = response.data;
                self.updateModel(modelId);
            });
        },

        updateModel : function (modelId) {

            var model = this.modelsCache[modelId];
            if (!model) {
                return;
            }

            // update heights
            var heights = [];
            for (var offerId in model.OFFERS) {
                if (!model.OFFERS.hasOwnProperty(offerId)) {
                    continue;
                }
                var offer = model.OFFERS[offerId];
                if (offer && offer.PROPERTIES.VISOTA && offer.PROPERTIES.VISOTA.VALUE) {
                    heights.push(parseFloat(offer.PROPERTIES.VISOTA.VALUE.replace(',', '.')));
                }
            }
            heights = heights.unique();
            heights.sort(function(a, b) {
                return a - b;
            });

            $('[data-entity="height-select"] option').not('[value=0]').remove();
            var heightSelect = this.entity('height-select');
            heightSelect.attr('disabled', false);
            heights.forEach(function (height) {
                heightSelect.append('<option value="' + height + '">' + height + '</option>');
            });

            if (model['DEFAULT_COLOR'] && model['DEFAULT_COLOR'].ID) {
                this.colorId = parseInt(model['DEFAULT_COLOR'].ID);
                this.updateColorInfo(this.colorId);
            }

        },

        resetModel : function () {
            var heightSelect = this.entity('height-select');
            heightSelect.attr('disabled', true);
            $('[data-entity="height-select"] option').not('[value=0]').remove();
        },

        calculatePower : function () {

            var roomSquare = this.entity('room-square-input')[0].value;
            var roomHeight = this.entity('room-height-input')[0].value;
            var roomRadiatorsNumber = this.entity('room-number-input')[0].value;
            var totalPower = null;
            var totalViewPower = '-';

            if (roomSquare && roomHeight && roomRadiatorsNumber) {
                totalViewPower = Math.round(roomSquare * (roomHeight / 100) * 30);
                totalPower = Math.round((roomSquare * (roomHeight / 100) * 30) / roomRadiatorsNumber);
            }

            this.totalPower = totalPower;
            this.entity('total-power').text(totalViewPower);

            this.calculateSections();

        },

        calculateSections : function () {

            this.totalSections = null;

            var totalSectionView = this.entity('total-section');

            if (!this.totalPower || !this.modelId) {
                totalSectionView.text('-');
                this.clearOffer();
                return;
            }

            if (!this.modelsCache[this.modelId]) {
                totalSectionView.text('-');
                this.clearOffer();
                return;
            }

            var offer;
            var model = this.modelsCache[this.modelId];

            if (!model) {
                this.clearOffer();
                return;
            }

            var selectedHeight = this.entity('height-select')[0].value;

            for (var offerId in model.OFFERS) {
                if (! model.OFFERS.hasOwnProperty(offerId)) {
                    continue;
                }
                if (model.OFFERS[offerId].PROPERTIES['VISOTA'].VALUE != selectedHeight) {
                    continue;
                }
				
				console.log("offerId_2");
				console.log(offerId);				
				console.log(model.OFFERS[offerId].PROPERTIES.NOM_TEPL_MOSHNOST_ALL.VALUE);
				
                offer = model.OFFERS[offerId];
            }

            if (!offer) {
                this.clearOffer();
                return;
            }
			
			console.log("selectedHeight");
			console.log(selectedHeight);//1802
			//dop
			var selectedModel = this.entity('model-select')[0].value;
				console.log("selectedModel");
			console.log(selectedModel);//8306
			

            var coefficient = parseFloat(offer.PROPERTIES['KOEF_MODIFICATION'].VALUE.replace(',', '.'));
            var nominalPower = parseFloat(offer.PROPERTIES['NOM_TEPL_MOSHNOST'].VALUE.replace(',', '.'));
            var nominalPowerALL = parseFloat(offer.PROPERTIES['NOM_TEPL_MOSHNOST_ALL'].VALUE);

		
			
			console.log("nominalPower");
			console.log(nominalPower);
			console.log("nominalPowerALL");
			console.log(nominalPowerALL);

            if (!coefficient || !nominalPower) {
                totalSectionView.text('-');
                this.clearOffer();
                return;
            }
			
			console.log("coefficient");
			console.log(coefficient);
			console.log(Math.pow(70/60, coefficient));
            var sectionPower = (nominalPower * Math.pow(70/60, coefficient));
			
			if(selectedModel == 8306 && selectedHeight == 1802){
				 var sectionPower = nominalPower;				
				console.log("sectionPower2");
				console.log(sectionPower);
			}else{
				 var sectionPower = (nominalPower * Math.pow(70/60, coefficient));
				console.log("sectionPower1");
				console.log(sectionPower);			
			}
			
            //var sectionPower = (nominalPower);
			console.log("sectionPower");
			console.log(sectionPower);
			
			
		this.totalSections = Math.round(this.totalPower / sectionPower);
			
			if(selectedModel == 8306 && selectedHeight == 1802){
				//this.totalSections = Math.round(this.totalPower);				
				console.log("totalSections2");
				console.log(this.totalSections);
			}else{
				this.totalSections = Math.round(this.totalPower / sectionPower);
				console.log("totalSections1");
				console.log(this.totalSections);				
			}
			
			
			

            totalSectionView.text(this.totalSections || '-');

            if (!this.totalSections) {
                this.clearOffer();
                return;
            }

            this.findOffer();
        },

        findOffer : function (nearest) {

            this.offerId = null;

            if (!this.totalPower || !this.totalSections) {
                return;
            }
			
			var sections = !!nearest ? (this.totalSections + 1) : this.totalSections;

            if (!this.modelsCache[this.modelId]) {
                return;
            }

            var offer = null;
            var model = this.modelsCache[this.modelId];
            var selectedHeight = this.entity('height-select')[0].value;

            for (var offerId in model.OFFERS) {
                if (! model.OFFERS.hasOwnProperty(offerId)) {
                    continue;
                }
                if (model.OFFERS[offerId].PROPERTIES['VISOTA'].VALUE != selectedHeight) {
                    continue;
                }
                if (model.OFFERS[offerId].PROPERTIES['KOLICHESTVO_SEKCIY'].VALUE != sections) {
                    continue;
                }
				console.log("offerId_1");
				console.log(offerId);				
				console.log(model.OFFERS[offerId].PROPERTIES.TIP_PODCKLUCHENIA.VALUE);
				
                offer = model.OFFERS[offerId];
				
				if(model.OFFERS[offerId].PROPERTIES.TIP_PODCKLUCHENIA.VALUE == "боковое")
					break;
            }

            if (!offer) {
				if (!nearest) {
					this.findOffer(true);
					return;
				}
                this.clearOffer();
                return;
            }

            this.offerId = parseInt(offer.ID);
            this.redrawOffer(offer);
        },

        redrawOffer : function (offer) {

            var offerBlock = this.entity('offer-block');

			console.log("offer.PROPERTIES.NOM_TEPL_MOSHNOST_ALL.VALUE");
			console.log(offer.PROPERTIES.NOM_TEPL_MOSHNOST_ALL.VALUE);
			
            this.entity('artnumber', offerBlock).text(offer.PROPERTIES.ARTNUMBER.VALUE);
            this.entity('telall', offerBlock).text(offer.PROPERTIES.NOM_TEPL_MOSHNOST_ALL.VALUE);

            /** Replace price */
            this.updatePrice();

            /** Replace stock */
            this.entity('in-stock', offerBlock).hide();
            this.entity('out-stock', offerBlock).hide();

            var stock = offer.PROPERTIES.STOCK.VALUE_XML_ID;
            if (stock === this.constants['in_stock']) {
                this.entity('in-stock', offerBlock).show();
            } else {
                this.entity('out-stock', offerBlock).show();
            }

            /** Replace properties */
            var propertiesBlock = this.entity('values-block');
            propertiesBlock.html('');


            for (var code in offer.DISPLAY_PROPERTIES) {
                if (!offer.DISPLAY_PROPERTIES[code] || !offer.DISPLAY_PROPERTIES[code].VALUE) {
                    continue;
                }
                var isLast = Object.keys(offer.DISPLAY_PROPERTIES).findIndex(function(entry) {
                    return entry === code;
                }) === (Object.keys(offer.DISPLAY_PROPERTIES).length - 1);
                var suffix = !isLast ? ', ' : '';
                propertiesBlock.append('<span class="value-name">' + offer.DISPLAY_PROPERTIES[code].NAME + ': </span>');
                propertiesBlock.append('<span class="value-text">' + offer.DISPLAY_PROPERTIES[code].VALUE + suffix + '</span>');
            }

            /** Replace labels */
            var labelsBlock = this.entity('labels-block', offerBlock);
            labelsBlock.html('');
            if (offer.PROPERTIES.LABEL && offer.PROPERTIES.LABEL.VALUE) {
                offer.PROPERTIES.LABEL.VALUE.forEach(function(value) {
                    labelsBlock.append('<div class="total__tags">' + value + '</div>');
                });
            }

            this.entity('offer-stub').hide();
            this.entity('offer-header').show();
            this.entity('offer-block').show();
        },

        updateColorInfo : function (colorId) {

            var self = this;
            colorId = parseInt(colorId);

            if (!colorId) {
                return;
            }

            if (!this.colorsCache[colorId]) {
                var request = BX.ajax.runComponentAction('irsap:radiator.calc', 'getColor', {
                    mode: 'class',
                    signedParameters: this.signedParameters,
                    data: {
                        id: colorId
                    }
                });

                request.then(function (response) {
                    if (!response || response.status != 'success' || !response.data) {
                        return;
                    }
                    self.colorsCache[colorId] = response.data;
                    self.updateColorInfo(colorId);
                });
                return;
            }

            var color = this.colorsCache[colorId];

            this.entity('color-name', this.entity('offer-block')).text(color.UF_LIST_NAME);
            this.entity('color-ral', this.entity('offer-block')).text(color.UF_RAL);
            this.entity('color-code', this.entity('offer-block')).text(color.UF_CODE);
            this.entity('color-series', this.entity('offer-block')).text(color.UF_SERIES);
            this.entity('color-coating', this.entity('offer-block')).text(color.UF_COATING);

        },

        updatePrice : function () {

            if (!this.offerId || !this.modelId || !this.modelsCache[this.modelId]) {
                return;
            }

            var offerBlock = this.entity('offer-block');
            var offer = this.modelsCache[this.modelId].OFFERS[this.offerId];
            var price = offer.MIN_PRICE.DISCOUNT_VALUE;
            var quantity = parseInt(this.entity('item-quantity').val());

            if (this.colorId && this.colorsCache[this.colorId] && this.colorsCache[this.colorId]['UF_MARKUP'].length) {
                var markup = parseFloat(this.colorsCache[this.colorId]['UF_MARKUP']);
                if (markup > 0) {
                    price = parseFloat(price) * markup;
                }
            }
            this.entity('price', offerBlock).text(this.roundNumber(price * quantity, 2));
        },

        clearOffer : function () {
            this.entity('offer-header').hide();
            this.entity('offer-block').hide();
            this.entity('offer-stub').show();
        },

        roundNumber : function(value, precision) {
            return +parseFloat(value).toFixed(precision);
        },

    };

})();