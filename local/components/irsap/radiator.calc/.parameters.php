<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = [
    "PARAMETERS" => [
        "IBLOCK_ID" => array(
            "PARENT" => "MAIN",
            "NAME" => "ID инфоблока коллекций",
            "TYPE" => "STRING",
            "DEFAULT" => ""
        ),
    ]
];

return $arComponentParameters;
