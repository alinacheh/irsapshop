<?php

use Bitrix\Main\Loader;
use Bitrix\Main\Engine\Contract\Controllerable;

class RadiatorCalc extends CBitrixComponent implements Controllerable
{
    /**
     * @return array
     */
    public function configureActions()
    {
        return [
			'getModel' => [
				'prefilters' => [],
			],
			'getColor' => [
				'prefilters' => [],
			],
        ];
    }

    public function onPrepareComponentParams($arParams)
    {
        return parent::onPrepareComponentParams($arParams);
    }

    public function getCollections()
    {
        Loader::includeModule('iblock');

        $collections = [];
        $arParams = $this->arParams;

        $rsCollections = \CIBlockElement::GetList(
            ['SORT' => 'ASC'],
            [
                'IBLOCK_ID' => $arParams['IBLOCK_ID'],
                'ACTIVE' => 'Y',
                'PROPERTY_COLLECTION' => ['4', '6']
            ]
        );

        while ($obCollection = $rsCollections->GetNextElement()) {
            $collection = $obCollection->GetFields();
            $collection['PROPERTIES'] = $obCollection->GetProperties();
            $collections[] = $collection;
        }

        return $collections;
    }

    public function executeComponent()
	{
		$this->arResult = [
		    'collections' => $this->getCollections(),
        ];

	    $this->includeComponentTemplate();
	}

	public function getModelAction($id)
    {
        $model = [];

        Loader::includeModule('catalog');
        Loader::includeModule('iblock');

        $arParams = $this->arParams;

        $rsCollections = \CIBlockElement::GetList(
            ['SORT' => 'ASC'],
            [
                'IBLOCK_ID' => $arParams['IBLOCK_ID'],
                'ACTIVE' => 'Y',
                'ID' => $id,
            ]
        );

        if ($obCollection = $rsCollections->GetNextElement()) {
            $model = $obCollection->GetFields();
            $model['PROPERTIES'] = $obCollection->GetProperties();
        }

        // Get default color
        $model['DEFAULT_COLOR'] = null;
        if (!empty($model['PROPERTIES']['DEFAULT_COLOR']) && !empty($model['PROPERTIES']['DEFAULT_COLOR']['VALUE'])) {
            $colorRow = AppMainHelper::getHlBlockRows(COLORS_HIGHLOADBLOCK_ID, [$model['PROPERTIES']['DEFAULT_COLOR']['VALUE']], true);
            $model['DEFAULT_COLOR'] = isset($colorRow[0]) ? $colorRow[0] : null;
            $model['DEFAULT_COLOR']['UF_FILE'] = CFile::GetPath($model['DEFAULT_COLOR']['UF_FILE']);
        }

        if (!empty($model)) {
            $offers = CIBlockPriceTools::GetOffersArray(
                $arParams['IBLOCK_ID'],
                [$id],
                ['SORT' => 'ASC'],
                //['SORT' => 'DESC'],
                //['catalog_PRICE_1' => 'ASC'],
                ['IBLOCK_ID', 'ID', 'NAME', 'catalog_PRICE_1', 'CATALOG_GROUP_1'],
                ['TIP_PODCKLUCHENIA', 'VISOTA', 'KOLICHESTVO_SEKCIY'],
                0,
                CIBlockPriceTools::GetCatalogPrices($arParams['IBLOCK_ID'], ['BASE']),
                true,
                ['CURRENCY_ID' => 'RUB']
            );
            if (is_array($offers)) {
                $model['OFFERS'] = [];
                foreach ($offers as $offer) {
                    $model['OFFERS'][$offer['ID']] = $offer;
                }
            }
        }

        return $model;
    }

    public function getColorAction($id)
    {
        $colorRow = AppMainHelper::getHlBlockRows(COLORS_HIGHLOADBLOCK_ID, [$id]);
        $result = isset($colorRow[0]) ? $colorRow[0] : null;

        return $result;
    }

    protected function listKeysSignedParameters()
    {
        return [
            'IBLOCK_ID',
        ];
    }
}