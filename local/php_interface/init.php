<?php


define('STATUS_PY', 'PY');
define('PAY_SYSTEM_ID_SBERBANK', 4);

/**
* Include bootstrap file with custom constants, functions, agents, handlers and other
*/
if (file_exists(__DIR__.'/bootstrap.php')) {
	require_once __DIR__.'/bootstrap.php';
}