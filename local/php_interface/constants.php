<?php

// Identifications
const CATALOG_MODELS_ID = 1;
const CATALOG_OFFERS_ID = 2;
const DOCUMENTS_IBLOCK_ID = 6;
const COLORS_HIGHLOADBLOCK_ID = 4;
const CERTS_HIGHLOADBLOCK_ID = 5;
const PLUSES_HIGHLOADBLOCK_ID = 6;

// Stock
const STOCK_IN_STOCK = 'stock_in_stock';
const STOCK_OUT_STOCK = 'stock_out_of_stock';

define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/log_042687431.txt");
