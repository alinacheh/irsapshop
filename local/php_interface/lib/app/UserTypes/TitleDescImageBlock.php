<?php

namespace App\UserTypes;

use Bitrix\Main\Localization\Loc,
    Bitrix\Iblock;

class TitleDescImageBlock
{
    const USER_TYPE = 'TitleDescImage';

    public static function GetUserTypeDescription()
    {
        return array(
            "PROPERTY_TYPE" => Iblock\PropertyTable::TYPE_STRING,
            "USER_TYPE" => self::USER_TYPE,
            "DESCRIPTION" => 'Блок с текстом и картинкой',
            "GetPropertyFieldHtml" => array(__CLASS__, "GetPropertyFieldHtml"),
            "GetPropertyFieldHtmlMulty" => array(__CLASS__, "GetPropertyFieldHtmlMulty"),
            "ConvertToDB" => array(__CLASS__, "ConvertToDB"),
            "ConvertFromDB" => array(__CLASS__, "ConvertFromDB"),
            "GetSettingsHTML" => array(__CLASS__, "GetSettingsHTML"),
        );
    }

    public static function GetPropertyFieldHtmlMulty($arProperty, $arValues, $strHTMLControlName)
    {
        $table_id = md5($strHTMLControlName["VALUE"]);
        $return = '<table id="tb'.$table_id.'_inner" border=0 cellpadding=0 cellspacing=0>';
        foreach ($arValues as $intPropertyValueID => $arOneValue)
        {
            $return .= '<tr><td>';
            $return .= '<table>';
            $return .= '<tr><td><select name="'.htmlspecialcharsbx($strHTMLControlName["VALUE"]."[$intPropertyValueID][POSITION]").'">';
            $return .= '<option value="LEFT" '.($arOneValue["VALUE"]["POSITION"] == 'LEFT' ? 'selected="selected"' : '').'>Текст слева</option>';
            $return .= '<option value="RIGHT" '.($arOneValue["VALUE"]["POSITION"] == 'RIGHT' ? 'selected="selected"' : '').'>Текст справа</option>';
            $return .= '</select></td></tr>';
            $return .= '<tr><td><select name="'.htmlspecialcharsbx($strHTMLControlName["VALUE"]."[$intPropertyValueID][COLOR]").'">';
            $return .= '<option value="DARK" '.($arOneValue["VALUE"]["COLOR"] == 'DARK' ? 'selected="selected"' : '').'>Темный текст</option>';
            $return .= '<option value="LIGHT" '.($arOneValue["VALUE"]["COLOR"] == 'LIGHT' ? 'selected="selected"' : '').'>Светлый текст</option>';
            $return .= '</select></td></tr>';
            $return .= '<tr><td><input type="text" name="'.htmlspecialcharsbx($strHTMLControlName["VALUE"]."[$intPropertyValueID][TITLE]").'" value="'.htmlspecialcharsEx($arOneValue["VALUE"]["TITLE"]).'"></td></tr>';
            $return .= '<tr><td><textarea name="'.htmlspecialcharsbx($strHTMLControlName["VALUE"]."[$intPropertyValueID][DESCRIPTION]").'" id="" cols="50" rows="10" placeholder="Описание блока">'.htmlspecialcharsEx($arOneValue["VALUE"]["DESCRIPTION"]).'</textarea></td></tr>';
            $return .= '<tr><td><input type="text" name="'.htmlspecialcharsbx($strHTMLControlName["VALUE"]."[$intPropertyValueID][IMAGE]").'" size="50" value="'.htmlspecialcharsEx($arOneValue["VALUE"]["IMAGE"]).'" placeholder="Путь к фоновой картинке"><br/><br/></td></tr>';
            $return .= '</table>';
            $return .= '</td></tr>';
        }

        $return .= '<tr><td>';

        $return .= '<table>';
        $return .= '<tr><td><select name="'.htmlspecialcharsbx($strHTMLControlName["VALUE"]."[n0][POSITION]").'">';
        $return .= '<option value="LEFT">Текст слева</option>';
        $return .= '<option value="RIGHT">Текст справа</option>';
        $return .= '</select></td></tr>';
        $return .= '<tr><td><select name="'.htmlspecialcharsbx($strHTMLControlName["VALUE"]."[n0][COLOR]").'">';
        $return .= '<option value="DARK">Темный текст</option>';
        $return .= '<option value="LIGHT">Светлый текст</option>';
        $return .= '</select></td></tr>';
        $return .= '<tr><td><input type="text" name="'.htmlspecialcharsbx($strHTMLControlName["VALUE"]."[n0][TITLE]").'" size="50" value="" placeholder="Заголовок"></td></tr>';
        $return .= '<tr><td><textarea name="'.htmlspecialcharsbx($strHTMLControlName["VALUE"]."[n0][DESCRIPTION]").'" id="" cols="50" rows="10" placeholder="Описание блока"></textarea></td></tr>';
        $return .= '<tr><td><input type="text" name="'.htmlspecialcharsbx($strHTMLControlName["VALUE"]."[n0][IMAGE]").'" size="50" value="" placeholder="Путь к фоновой картинке"><br/><br/></td></tr>';
        $return .= '</table>';

        $return .= '</td></tr>';

        $return .= '<tr><td><input type="button" value="'.Loc::getMessage("IBLOCK_PROP_FILEMAN_ADD").'" onClick="addNewRow(\'tb'.$table_id.'_inner\', -2)"></td></tr>';
        return $return.'</table>';
    }

    public static function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
    {
        global $APPLICATION;

        if (strLen(trim($strHTMLControlName["FORM_NAME"])) <= 0)
            $strHTMLControlName["FORM_NAME"] = "form_element";
        $name = preg_replace("/[^a-zA-Z0-9_]/i", "x", htmlspecialcharsbx($strHTMLControlName["VALUE"]));

        if(is_array($value["VALUE"]))
        {
            $value["VALUE"] = $value["VALUE"]["VALUE"];
            $value["DESCRIPTION"] = $value["DESCRIPTION"]["VALUE"];
        }

        $return = '<input type="text" name="'.htmlspecialcharsbx($strHTMLControlName["VALUE"]).'" id="'.$name.'" size="'.$arProperty["COL_COUNT"].'" value="'.htmlspecialcharsEx($value["VALUE"]).'">';

        if (($arProperty["WITH_DESCRIPTION"]=="Y") && ('' != trim($strHTMLControlName["DESCRIPTION"])))
        {
            $return .= ' <span title="'.Loc::getMessage("IBLOCK_PROP_FILEMAN_DESCRIPTION_TITLE").'">'.Loc::getMessage("IBLOCK_PROP_FILEMAN_DESCRIPTION_LABEL").':<input name="'.htmlspecialcharsEx($strHTMLControlName["DESCRIPTION"]).'" value="'.htmlspecialcharsEx($value["DESCRIPTION"]).'" size="18" type="text"></span>';
        }

        return $return;
    }

    public static function ConvertToDB($arProperty, $value)
    {
        $result = array();
        $return = array();
        if(is_array($value["VALUE"]))
        {
            $result["TITLE"] = trim($value["VALUE"]["TITLE"]);
            $result["DESCRIPTION"] = trim($value["VALUE"]["DESCRIPTION"]);
            $result["IMAGE"] = trim($value["VALUE"]["IMAGE"]);
            $result["IMAGE_ALT"] = trim($value["VALUE"]["IMAGE_ALT"]);
            $result["POSITION"] = trim($value["VALUE"]["POSITION"]);
            $result["COLOR"] = trim($value["VALUE"]["COLOR"]);
        }
        else
        {
            $result["TITLE"] = trim($value["TITLE"]);
            $result["DESCRIPTION"] = trim($value["DESCRIPTION"]);
            $result["IMAGE"] = trim($value["IMAGE"]);
            $result["IMAGE_ALT"] = trim($value["IMAGE_ALT"]);
            $result["POSITION"] = trim($value["POSITION"]);
            $result["COLOR"] = trim($value["COLOR"]);
        }

        if (empty($result["TITLE"]) && empty($result["DESCRIPTION"]) && empty($result["IMAGE"])) {
            return array();
        }

        $return["VALUE"] = serialize($result);
        $return["DESCRIPTION"] = '';
        return $return;
    }

    public static function ConvertFromDB($arProperty, $value)
    {
        $result = @unserialize($value['VALUE']);
        $return = array();
        if (strLen(trim($result["TITLE"])) > 0)
            $return["VALUE"]["TITLE"] = $result["TITLE"];
        if (strLen(trim($result["DESCRIPTION"])) > 0)
            $return["VALUE"]["DESCRIPTION"] = $result["DESCRIPTION"];
        if (strLen(trim($result["IMAGE"])) > 0)
            $return["VALUE"]["IMAGE"] = $result["IMAGE"];
        if (strLen(trim($result["IMAGE_ALT"])) > 0)
            $return["VALUE"]["IMAGE_ALT"] = $result["IMAGE_ALT"];
        if (strLen(trim($result["POSITION"])) > 0)
            $return["VALUE"]["POSITION"] = $result["POSITION"];
        if (strLen(trim($result["COLOR"])) > 0)
            $return["VALUE"]["COLOR"] = $result["COLOR"];
        return $return;
    }

    public static function GetSettingsHTML($arProperty, $strHTMLControlName, &$arPropertyFields)
    {
        $arPropertyFields = array(
            "HIDE" => array("MULTIPLE_CNT"),
        );

        return '';
    }
}