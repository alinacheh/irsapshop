<?php

namespace App\UserTypes;

class Html
{
    public function GetUserTypeDescription()
    {
        return [
            'CLASS_NAME' => static::class,
            'BASE_TYPE' => 'string',
            'USER_TYPE_ID' => 'editor',
            'DESCRIPTION' => 'Визуальный редактор'
        ];
    }

    public function GetDBColumnType()
    {
        return 'text';
    }

    public function GetEditFormHTML($arUserField, $arHtmlControl)
    {
        ob_start();
        \CFileMan::AddHTMLEditorFrame($arHtmlControl['NAME'], $arHtmlControl['VALUE'], false, 'html', ['height' => 450, 'width' => '100%']);
        $result = ob_get_contents();
        ob_end_clean();

        return $result;
    }
}