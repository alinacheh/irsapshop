<?php

namespace App\Support;

use App\Catalog\CatalogProductProvider;
use Bitrix\Main\HttpRequest;
use Bitrix\Main\Loader;

class AjaxActionsController
{
    /**
     * Add product to basket
     *
     * @param HttpRequest $request {'id' : <element_id>, 'quantity' : 1, 'colorId' : 1}
     * @return array
     */
    public function addToBasket(HttpRequest $request)
    {
        try {
            $productId = $request->getPost('id');
            $quantity = $request->getPost('quantity') ?: 1;
            $colorId = (int) $request->getPost('colorId');

            if (!Loader::includeModule('catalog')) {
                throw new \Exception('Module "catalog" not installed');
            }

            $product = array(
                'PRODUCT_ID' => $productId,
                'QUANTITY' => $quantity,
            );

            $basketOptions = [];

            if ($colorId) {
                $colors = \AppMainHelper::getHlBlockRows(COLORS_HIGHLOADBLOCK_ID, [$colorId]);
                if (!empty($colors)) {
                    $color = $colors[0];
                    $product['PROPS'][] = [
                        "NAME" => "Цвет",
                        "CODE" => "COLOR",
                        "VALUE" => $color['UF_LIST_NAME'],
                        "SORT" => "100",
                    ];

                    if ($color['UF_MARKUP'] && (float) $color['UF_MARKUP'] > 0) {
                        $arProductPrice = \CCatalogProduct::GetOptimalPrice($productId, $quantity);
                        $price = $arProductPrice['RESULT_PRICE']['DISCOUNT_PRICE'] * (float) $color['UF_MARKUP'];
                        $basketOptions = [
                            'PRICE' => $price,
                            'CUSTOM_PRICE' => 'Y',
                            'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider',
                            'IGNORE_CALLBACK_FUNC' => 'Y',
                        ] + $basketOptions;
                    }
                }
            }

            /**
             * @var Result $result
             */
            $result = \Bitrix\Catalog\Product\Basket::addProduct($product, $basketOptions);
            if ($result->isSuccess()) {
                return $this->renderJsonResponse();
            } else {
                $this->log($result->getErrorMessages());
                return $this->renderJsonResponse(null, [
                    [
                        'code' => 510,
                        'message' => 'Error on add product to basket'
                    ]
                ]);
            }

        } catch (\Exception $e) {
            return $this->exception($e);
        }
    }

    protected function log($subject)
    {
        \AddMessage2Log($subject);
    }

    protected function exception(\Exception $e)
    {
        $this->log($e->getMessage());

        $error = [
            'code' => $e instanceof ClientException ? 400 : 500,
            'message' => $e instanceof ClientException ? $e->getMessage() : 'Error during action processing!'
        ];

        return $this->renderJsonResponse(null, [
            $error
        ]);
    }

    protected function renderJsonResponse($data = null, $errors = null, $additional = [])
    {
        return $additional + [
            'data' => $data,
            'successful' => empty($errors),
            'errors' => $errors
        ];
    }
}