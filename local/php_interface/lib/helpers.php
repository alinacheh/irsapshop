<?php

use Bitrix\Main\Loader;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Localization\Loc;
use Bitrix\Highloadblock\HighloadBlockTable as HL;

Loc::loadMessages(__FILE__);

class AppMainHelper
{
	public static function capitalize(string $string)
    {
        $first = mb_strtoupper(mb_substr($string, 0, 1));
        return $first . mb_substr($string, 1);
    }

    public static function getPublicCurrency(string $currencyCode)
    {
        $currencyMap = [
            'ru' => [
                'RUB' => 'руб'
            ]
        ];

        if (empty($currencyMap[LANGUAGE_ID][$currencyCode])) {
            return '';
        }

        return $currencyMap[LANGUAGE_ID][$currencyCode];
    }

    public static function formatPhoneForLink($phone = '')
    {
        return preg_replace('/[^0-9+]/i', '', $phone);
    }

    public static function getFullURL($withParams = true)
    {
        $context = \Bitrix\Main\Application::getInstance()->getContext();
        $request = $context->getRequest();
        $server = $context->getServer();

        $url = "";
        // protocol
        $url .= $request->isHttps() ? "https://" : "http://";
        // host name
        $url .= $server->getHttpHost();
        // page
        $url .= $withParams === true ? $request->getRequestUri() : $request->getRequestedPage();

        return $url;
    }

    public static function clearSectionsCache()
    {
        if(!CModule::IncludeModule('iblock'))
        {
            return false;
        }

        global $CACHE_MANAGER;

        $arIblocks = array();

        foreach($arIblocks as $iblockId)
        {
            $CACHE_MANAGER->ClearByTag('iblock_id_'.$iblockId);
        }
        return true;
    }

    /**
     * It's analog of CIBlockPriceTools::getSliderForItem with some useful options (description, custom sizes)
     * @param $item (ex. $arResult in detail card)
     * @param $propertyCode (ex. "MORE_PHOTO")
     * @param $addDetailToSlider (ex. true)
     * @param array $arAddSizes (ex. array("SMALL" => array("WIDTH" => 50, "HEIGHT" => 50), "MIDDLE => array(...), ...")))
     * @return array
     */
    public static function getGalleryForItem(&$item, $propertyCode, $addDetailToSlider, $arAddSizes = array())
    {
        $result = array();

        if (!empty($item) && is_array($item))
        {
            if (
                '' != $propertyCode &&
                isset($item['PROPERTIES'][$propertyCode]) &&
                'F' == $item['PROPERTIES'][$propertyCode]['PROPERTY_TYPE']
            )
            {
                if ('MORE_PHOTO' == $propertyCode && isset($item['MORE_PHOTO']) && !empty($item['MORE_PHOTO']))
                {
                    foreach ($item['MORE_PHOTO'] as &$onePhoto)
                    {
                        $result[] = array(
                            'ID' => (int)$onePhoto['ID'],
                            'SRC' => $onePhoto['SRC'],
                            'WIDTH' => (int)$onePhoto['WIDTH'],
                            'HEIGHT' => (int)$onePhoto['HEIGHT'],
                            'DESCRIPTION' => $onePhoto['DESCRIPTION']
                        );
                    }
                    unset($onePhoto);
                }
                else
                {
                    if (
                        isset($item['DISPLAY_PROPERTIES'][$propertyCode]['FILE_VALUE']) &&
                        !empty($item['DISPLAY_PROPERTIES'][$propertyCode]['FILE_VALUE'])
                    )
                    {
                        $fileValues = (
                        isset($item['DISPLAY_PROPERTIES'][$propertyCode]['FILE_VALUE']['ID']) ?
                            array(0 => $item['DISPLAY_PROPERTIES'][$propertyCode]['FILE_VALUE']) :
                            $item['DISPLAY_PROPERTIES'][$propertyCode]['FILE_VALUE']
                        );
                        foreach ($fileValues as &$oneFileValue)
                        {
                            $result[] = array(
                                'ID' => (int)$oneFileValue['ID'],
                                'SRC' => $oneFileValue['SRC'],
                                'WIDTH' => (int)$oneFileValue['WIDTH'],
                                'HEIGHT' => (int)$oneFileValue['HEIGHT'],
                                'DESCRIPTION' => $oneFileValue['DESCRIPTION']
                            );
                        }
                        if (isset($oneFileValue))
                            unset($oneFileValue);
                    }
                    else
                    {
                        $propValues = $item['PROPERTIES'][$propertyCode]['VALUE'];
                        if (!is_array($propValues))
                            $propValues = array($propValues);

                        foreach ($propValues as &$oneValue)
                        {
                            $oneFileValue = CFile::GetFileArray($oneValue);
                            if (isset($oneFileValue['ID']))
                            {
                                $result[] = array(
                                    'ID' => (int)$oneFileValue['ID'],
                                    'SRC' => $oneFileValue['SRC'],
                                    'WIDTH' => (int)$oneFileValue['WIDTH'],
                                    'HEIGHT' => (int)$oneFileValue['HEIGHT'],
                                    'DESCRIPTION' => $oneFileValue['DESCRIPTION']
                                );
                            }
                        }
                        if (isset($oneValue))
                            unset($oneValue);
                    }
                }
            }
            if ($addDetailToSlider || empty($result))
            {
                if (!empty($item['DETAIL_PICTURE']))
                {
                    if (!is_array($item['DETAIL_PICTURE']))
                        $item['DETAIL_PICTURE'] = CFile::GetFileArray($item['DETAIL_PICTURE']);
                    if (isset($item['DETAIL_PICTURE']['ID']))
                    {
                        array_unshift(
                            $result,
                            array(
                                'ID' => (int)$item['DETAIL_PICTURE']['ID'],
                                'SRC' => $item['DETAIL_PICTURE']['SRC'],
                                'WIDTH' => (int)$item['DETAIL_PICTURE']['WIDTH'],
                                'HEIGHT' => (int)$item['DETAIL_PICTURE']['HEIGHT'],
                                'DESCRIPTION' => $item['DETAIL_PICTURE']['DESCRIPTION']
                            )
                        );
                    }
                }
            }
            if(!empty($arAddSizes))
            {
                foreach($result as &$photo)
                {
                    if(!$photo["ID"]) continue;

                    foreach($arAddSizes as $key => $arSize)
                    {
                        if(!is_string($key) || !isset($arSize["WIDTH"]) || !isset($arSize["HEIGHT"])) continue;

                        $mode = $arSize['MODE'] === BX_RESIZE_IMAGE_EXACT ? BX_RESIZE_IMAGE_EXACT : BX_RESIZE_IMAGE_PROPORTIONAL;
                        if(intval($arSize["WIDTH"]) > 0 && intval($arSize["HEIGHT"]) > 0)
                        {
                            $tmpPhoto = CFile::ResizeImageGet($photo["ID"], array('width'=>$arSize["WIDTH"], 'height'=>$arSize["HEIGHT"]), $mode, true);
                            if(!$tmpPhoto["src"]) continue;

                            $photo[$key] = array(
                                "SRC" => $tmpPhoto["src"],
                                "WIDTH" => $tmpPhoto["width"],
                                "HEIGHT" => $tmpPhoto["height"],
                                "DESCRIPTION" => $photo["DESCRIPTION"],
                            );
                        }
                    }
                }
            }
        }
        return $result;
    }

    /**
     * Get full higloadblock row with enum values
     *
     * @param int $hlBlockId
     * @param array $ids
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\SystemException
     */
    public static function getHlBlockRows(int $hlBlockId, array $ids, bool $isReference = false)
    {
        global $USER_FIELD_MANAGER;

        Loader::includeModule('highloadblock');

        $arHLBlock = HL::getById($hlBlockId)->fetch();
        $obEntity = HL::compileEntity($arHLBlock);
        $strEntityDataClass = $obEntity->getDataClass();

        $filter = $isReference ? ['=UF_XML_ID' => $ids] : ['=ID' => $ids];
        $order = $isReference ? ['UF_SORT' => 'ASC'] : ['ID' => 'DESC'];

        $rows = $strEntityDataClass::getList([
            'order' => $order,
            'filter' => $filter,
        ])->fetchAll();

        $fields = $USER_FIELD_MANAGER->GetUserFields('HLBLOCK_' . $hlBlockId);

        // collect enum fields
        $enumFields = [];
        foreach ($fields as $field) {
            if ($field['USER_TYPE']['BASE_TYPE'] != 'enum') {
                continue;
            }
            $enumFields[] = $field['FIELD_NAME'];
        }

        // collect enum ids
        $enumIds = [];
        foreach ($rows as $row) {
            foreach ($enumFields as $enumField) {
                $enumIds[] = $row[$enumField];
            }
        }

        $enums = [];
        $dbEnums = CUserFieldEnum::GetList(array(), array("ID" => $enumIds));
        while ($arEnum = $dbEnums->Fetch()) {
            $enums[$arEnum['ID']] = $arEnum;
        }

        foreach ($rows as $index => $row) {
            foreach ($enumFields as $enumField) {
                $rows[$index][$enumField] = $enums[$row[$enumField]]['VALUE'];
                $rows[$index][$enumField . "_ENUM_ID"] = $enums[$row[$enumField]]['ID'];
                $rows[$index][$enumField . "_XML_ID"] = $enums[$row[$enumField]]['XML_ID'];
            }
        }

        return $rows;
    }
}

/**
* Class AppSettingsHelper
*
* Is a helper for 'app.configurator' module.
*/
class AppSettingsHelper
{
	/**
	* Return setting from 'app.configurator' module options.
	*/
	public static function get($name, $default = '')
	{
		if (Loader::includeModule('app.configurator')) {
			return unserialize(Option::get('app.configurator', 'app.'.$name, serialize($default)));
		}
		return $default;
	}
}