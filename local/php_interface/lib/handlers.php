<?php

use Bitrix\Main\EventManager;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/* TODO: TEMPORARY | SHOULD REMOVE */
/*
AddEventHandler("main", "OnEndBufferContent", "removeLicense");
function removeLicense(&$content) {
    if (stripos($_SERVER['REQUEST_URI'], 'bitrix/admin') === false && stripos($content, 'html>') !== false)
        $content = substr($content, 324);
}
*/

$eventManager = EventManager::getInstance();

$eventManager->addEventHandler('main', 'OnEndBufferContent', array('AppHandlers', 'OnEndBufferContentHandler'));
$eventManager->addEventHandler("main", "OnBeforeEventAdd", Array("AppHandlers", "OnBeforeEventAddHandler"));
$eventManager->addEventHandler("main", "OnUserTypeBuildList", Array("App\\UserTypes\\Html", "GetUserTypeDescription"));
$eventManager->addEventHandler("main", "OnUserTypeBuildList", Array("App\\UserTypes\\MultiStringBlock", "GetUserTypeDescription"));
$eventManager->addEventHandler("iblock", "OnIBlockPropertyBuildList", array("App\\UserTypes\\TitleDescImageBlock", "GetUserTypeDescription"));
$eventManager->addEventHandler("sale", "OnSaleComponentOrderOneStepProcess", Array("AppHandlers", "OnSaleComponentOrderOneStepProcessHandler"));

/**
*	AddEventHandler('main', 'OnPageStart', array('AppHandlers', 'OnPageStartHandler')); 
*	or
*	$eventManager->addEventHandler('main', 'OnPageStart', array('AppHandlers', 'OnPageStartHandler'));
*/

class AppHandlers
{
    public function OnEndBufferContentHandler(&$content)
    {
        if (defined("ADMIN_SECTION")) {
            return;
        }

        $content = preg_replace_callback('/{{([A-Za-z0-9_]+)}}/i', function($matches) {
            $value = AppSettingsHelper::get($matches[1], '');

            if ($value && is_scalar($value)) {
                return $value;
            }

            return '';
        }, $content);
    }

    function OnBeforeEventAddHandler(&$event, &$lid, &$arFields, &$messageId, &$files)
    {
        if (!isset($arFields['RS_FORM_ID'])
            || !($resultId = $arFields['RS_RESULT_ID'])
            || !Bitrix\Main\Loader::includeModule('form')
        ) {
            return;
        }

        $arResult = $arAnswer2 = array();
        $arAnswer = CFormResult::GetDataByID(
            $resultId, array(),
            $arResult, $arAnswer2
        );
        unset($arResult, $arAnswer2);

        if (!is_array($arAnswer) || empty($arAnswer)) {
            return;
        }

        foreach ($arAnswer as $fieldCode => $arAnswers) {
            foreach ($arAnswers as $answer) {
                if ($answer['FIELD_TYPE'] !== 'file') {
                    break;
                }

                if (isset($answer['USER_FILE_ID']) && intval($answer['USER_FILE_ID']) > 0) {
                    $files[] = intval($answer['USER_FILE_ID']);
                }
            }
        }
        return;
    }

    public static function OnSaleComponentOrderOneStepProcessHandler(&$arResult, &$arUserResult)
    {
        global $APPLICATION;

        if ($arUserResult["CONFIRM_ORDER"] == "Y") {
            /* Проверка капчи */
//            if (!$APPLICATION->CaptchaCheckCode($_POST['captcha_word'], $_POST['captcha_sid'])) {
//                $arResult["ERROR"][] = 'Необходимо установить галочку "Я не робот"';
//            }
        }
    }
}