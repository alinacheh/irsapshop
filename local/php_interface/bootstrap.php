<?php

/* Composer */
if (file_exists(__DIR__.'/../vendor/autoload.php')) {
	require_once __DIR__.'/../vendor/autoload.php';
}

/* BX autoload */
if (file_exists(__DIR__.'/lib/autoload.php')) {
    require_once __DIR__.'/lib/autoload.php';
}

/* Constants */
if (file_exists(__DIR__.'/constants.php')) {
	require_once __DIR__.'/constants.php';
}

/* Helpers */
if (file_exists(__DIR__.'/lib/helpers.php')) {
	require_once __DIR__.'/lib/helpers.php';
}

/* Agents */
if (file_exists(__DIR__.'/lib/agents.php')) {
	require_once __DIR__.'/lib/agents.php';
}

/* Handlers */
if (file_exists(__DIR__.'/lib/handlers.php')) {
	require_once __DIR__.'/lib/handlers.php';
}