<?php

use Bitrix\Main\Application;
use Bitrix\Main\Localization\Loc;

global $APPLICATION;

$request = Application::getInstance()
	->getContext()
	->getRequest();

$module_id = 'app.configurator';
$moduleAccessLevel = $APPLICATION->GetGroupRight($module_id);

if($moduleAccessLevel < 'U')
{
	$APPLICATION->AuthForm('Access denied!');
}

require_once __DIR__.'/classes/app_configurator_settings.php';

$settings = include(__DIR__.'/settings.php');

\Bitrix\Main\Loader::includeModule($module_id);

Loc::loadMessages($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/options.php");
Loc::loadMessages(__FILE__);

$aTabs = array(
	array(
		"DIV" => "edit0",
		"TAB" => Loc::getMessage("APP_CONFIGURATOR_TABS_SETTINGS_TITLE"),
		"ICON" => "",
		"TITLE" => Loc::getMessage("APP_CONFIGURATOR_TABS_SETTINGS_TITLE")
	),
	array(
		"DIV" => "edit1",
		"TAB" => Loc::getMessage("APP_CONFIGURATOR_TABS_RIGHTS_TITLE"),
		"ICON" => "",
		"TITLE" => Loc::getMessage("APP_CONFIGURATOR_TABS_RIGHTS_TITLE")
	)
);

$tabControl = new CAdminTabControl("appConfiguratorTabControl", $aTabs, true, true);

if($request->isPost() && $moduleAccessLevel == "W" && check_bitrix_sessid())
{
	if (isset($request['Update']) && $request['Update'] === 'Y')
	{
		AppConfiguratorSettings::saveServiceSettings();

		ob_start();
		require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/admin/group_rights.php');
		ob_end_clean();

		LocalRedirect($APPLICATION->GetCurPage().'?lang='.LANGUAGE_ID.'&mid='.$module_id.'&'.$tabControl->ActiveTabParam());
	}
}

$tabControl->Begin(); ?>

<form id="s_settings" enctype="multipart/form-data" method="POST" action="<?echo $APPLICATION->GetCurPage()?>?lang=<?echo LANGUAGE_ID?>&mid=<?=$module_id?>" name="innova_smssender_settings">
	<? echo bitrix_sessid_post();

	$tabControl->BeginNextTab();
	?>

	<?
	if(count($settings) > 0):
		AppConfiguratorSettings::render(array('CONTROLS' => $settings));
	else:
		ShowNote(Loc::getMessage('APP_CONFIGURATOR_EMPTY_SETTINGS'));
	endif;
	?>

	<? $tabControl->BeginNextTab();

	require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/admin/group_rights.php");

	$tabControl->Buttons(); ?>

	<input type="submit" <?if ($moduleAccessLevel < "W") echo "disabled" ?> name="Update" class="adm-btn-save" value="<?echo Loc::getMessage("MAIN_SAVE")?>">
	<input type="hidden" name="Update" value="Y">
	<input type="reset" name="reset" value="<?echo Loc::getMessage("MAIN_RESET")?>">

</form>

<? $tabControl->End(); ?>