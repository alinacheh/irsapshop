<?php

/**
 * Return array of controls arrays.
 *
 * CONTROL FIELDS:
 *
 * 		NAME - control(setting) name
 * 		TYPE - type of control(text, password, textarea, checkbox, radio, select, section, note, file, custom)
 * 		LABEL - control title
 * 		HINT - small hint under label
 * 		DEFAULT_VALUE - default value
 * 		MULTIPLE - allow multiple values(for checkbox, select)
 * 		OPTIONS - values for control(checkbox, select, radio)
 * 		ATTRIBUTES - array with additional attributes. array('attr_name' => 'attr_value')
 *      FILESIZE - max filesize in bytes(for file)
 *      FILETYPE - allowed filetypes (allowed: F - files, I - images, A - any)(for file)
 *      FILEEXT - allowed file extensions, works with FILETYPE = F (ex. "*.zip,*.rar,*.doc")(for file)
 * 		HANDLER - callback for CUSTOM type(Callable). Parameters : CONTROL - this control array, VALUE - DB value
 *
 * @return mixed
 */

return [
    [
        'TYPE' => 'SECTION',
        'LABEL' => 'Общие настройки'
    ],
    [
        'TYPE' => 'TEXT',
        'NAME' => 'EMAIL',
        'LABEL' => 'E-mail',
        'ATTRIBUTES' => ['size' => 40]
    ],
    [
        'TYPE' => 'TEXT',
        'NAME' => 'FAX',
        'LABEL' => 'Факс',
        'ATTRIBUTES' => ['size' => 40]
    ],
    [
        'TYPE' => 'TEXT',
        'NAME' => 'WHATSAPP',
        'LABEL' => 'WhatsApp',
        'ATTRIBUTES' => ['size' => 40]
    ],
    [
        'TYPE' => 'TEXT',
        'NAME' => 'PHONE',
        'LABEL' => 'Телефон',
        'ATTRIBUTES' => ['size' => 40]
    ],
    [
        'TYPE' => 'TEXT',
        'NAME' => 'PHONE_OTDEL_PRODAZH',
        'LABEL' => 'Телефон отдела продаж',
        'ATTRIBUTES' => ['size' => 40]
    ],
    [
        'TYPE' => 'TEXT',
        'NAME' => 'ADDRESS',
        'LABEL' => 'Адрес',
        'ATTRIBUTES' => ['size' => 50]
    ],
    [
        'TYPE' => 'TEXT',
        'NAME' => 'SCHEDULE',
        'LABEL' => 'График работы',
        'ATTRIBUTES' => ['size' => 50]
    ],
    [
        'TYPE' => 'TEXT',
        'NAME' => 'COPYRIGHT',
        'LABEL' => 'Копирайт',
        'ATTRIBUTES' => ['size' => 50]
    ],
    [
        'TYPE' => 'TEXT',
        'NAME' => 'SLOGAN',
        'LABEL' => 'Название сайта (шапка)',
        'ATTRIBUTES' => ['size' => 50]
    ],
    [
        'TYPE' => 'TEXT',
        'NAME' => 'SITE_NAME',
        'LABEL' => 'Название сайта (футер)',
        'ATTRIBUTES' => ['size' => 50]
    ],
    [
        'TYPE' => 'SECTION',
        'LABEL' => 'Логотип'
    ],
    [
        'TYPE' => 'FILE',
        'NAME' => 'LOGO_PATH',
        'LABEL' => 'Картинка логотипа',
        'FILETYPE' => 'I',
    ],
    [
        'TYPE' => 'TEXT',
        'NAME' => 'LOGO_TITLE',
        'LABEL' => 'Title картинки логотипа',
        'ATTRIBUTES' => ['size' => 40]
    ],
    [
        'TYPE' => 'TEXT',
        'NAME' => 'LOGO_ALT',
        'LABEL' => 'Alt картинки логотипа',
        'ATTRIBUTES' => ['size' => 40]
    ],
    [
        'TYPE' => 'SECTION',
        'LABEL' => 'Страница контактов'
    ],
    [
        'TYPE' => 'TEXT',
        'NAME' => 'MAP_SCRIPT',
        'LABEL' => 'Код карты',
        'ATTRIBUTES' => ['size' => 40]
    ],
];
