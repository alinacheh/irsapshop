<?php

use Bitrix\Main;
use Bitrix\Main\Config\Option;

class AppConfiguratorSettings
{
	const MODULE_ID = 'app.configurator';
	const OPTION_PREFIX = 'app';

	public static function saveServiceSettings()
	{
		$request = Main\Application::getInstance()->getContext()->getRequest();

		if (isset($request['CONFIG']) && is_array($request['CONFIG'])) {
			$config = $request['CONFIG'];
			foreach ($config as $name => $item) {
				if (is_array($item) && array_key_exists('tmp_name', $item)) {
				    if ($oldItem = @unserialize(Option::get(self::MODULE_ID, self::OPTION_PREFIX.'.'.$name))) {
                        CFile::Delete($oldItem);
                    }
				    $item['tmp_name'] = $_SERVER["DOCUMENT_ROOT"]. '/upload/tmp' .$item['tmp_name'];
                    $item = CFile::SaveFile(
                        array_merge($item, ['MODULE_ID' => self::MODULE_ID]),
                        self::MODULE_ID
                    );
                } elseif (is_numeric($item) && isset($request['CONFIG_del']) && isset($request['CONFIG_del'][$name])) {
				    CFile::Delete($item);
				    $item = null;
                }
			    Option::set(self::MODULE_ID, self::OPTION_PREFIX.'.'.$name, serialize($item));
			}
		}
	}

	public static function render($parameters = array())
	{
		$html = '';
		$controls = $parameters['CONTROLS'];

		foreach($controls as $control)
		{
			if($control['TYPE'] == 'SECTION' && strlen($control['LABEL']) > 0)
			{
				$html .= '<tr class="heading s_settings"><td colspan="2">'.$control['LABEL'].'</td></tr>';
				continue;
			}
			elseif($control['TYPE'] == 'NOTE' && strlen($control['LABEL']) > 0)
			{
				$note = BeginNote().$control['LABEL'].EndNote();
				$html .= '<tr class="s_settings"><td colspan="2" align="center">'.$note.'</td></tr>';
				continue;
			}

			$hint = $control['HINT'] ? '<br><small>'.$control['HINT'].'</small>' : '';
			$label = strlen($control['LABEL']) > 0 ? $control['LABEL'].':'.$hint : '';
			$html .= '<tr class="s_settings"><td width="40%">'.$label.'</td><td width="60%">';
			$name = $control['NAME'];

			$attributes = '';
			if(is_array($control['ATTRIBUTES']) && count($control['ATTRIBUTES']) > 0)
			{
				foreach($control['ATTRIBUTES'] as $attrName => $attrValue)
				{
					$attributes .= ' '.$attrName.'='.$attrValue;
				}
			}

			$dbValue = unserialize(Option::get(self::MODULE_ID, self::OPTION_PREFIX.'.'.$name, serialize('')));

			$ctrlName = 'CONFIG['.$name.']';
			$ctrlValue = htmlspecialcharsbx($dbValue ?: ($control['DEFAULT_VALUE'] ?: null));

			switch(strtoupper($control['TYPE']))
			{
				case 'TEXT':
					$html .= '<input type="text" name="'.$ctrlName.'" value="'.$ctrlValue.'" '.$attributes.'>';
					break;
				case 'PASSWORD':
					$html .= '<input type="password" name="'.$ctrlName.'" value="'.$ctrlValue.'" '.$attributes.'>';
					break;
				case 'TEXTAREA':
					$html .= '<textarea name="'.$ctrlName.'" '.$attributes.'>'.$ctrlValue.'</textarea>';
					break;
				case 'SELECT':
					if($control['MULTIPLE'] == 'Y')
					{
						$ctrlName .= '[]';
					}
					$html .= '<select name="'.$ctrlName.'" '.($control['MULTIPLE'] == 'Y' ? 'multiple' : '').' '.$attributes.'>';
					foreach($control['OPTIONS'] as $option):
						if($ctrlValue !== null && !is_array($ctrlValue))
						{
							$ctrlValue = array($ctrlValue);
						}
						$selected = (in_array($option['VALUE'], $ctrlValue) ? 'selected="selected"' : '');
						$html .= '<option value="'.$option['VALUE'].'" '.$selected.'>'.$option['LABEL'].'</option>';
					endforeach;
					$html .= '</select>';
					break;
				case 'CHECKBOX':
					if($control['MULTIPLE'] == 'Y')
					{
						$ctrlName .= '[]';
					}
					foreach($control['OPTIONS'] as $i => $option):
						if($ctrlValue !== null && !is_array($ctrlValue))
						{
							$ctrlValue = array($ctrlValue);
						}
						if($i > 0)
						{
							$html .= '<br />';
						}
						$id = strtolower($name).'_'.$i;
						$checked = (in_array($option['VALUE'], $ctrlValue) ? 'checked="checked"' : '');
						$html .= '<input type="checkbox" name="'.$ctrlName.'" id="'.$id.'" value="'.$option['VALUE'].'" '.$checked.' '.$attributes.' />';
						$html .= '<label for="'.$id.'">'.$option['LABEL'].'</label>';
					endforeach;
					break;
				case 'RADIO':
					foreach($control['OPTIONS'] as $i => $option):
						if($ctrlValue !== null && !is_array($ctrlValue))
						{
							$ctrlValue = array($ctrlValue);
						}
						if($i > 0)
						{
							$html .= '<br />';
						}
						$id = strtolower($name).'_'.$i;
						$checked = (in_array($option['VALUE'], $ctrlValue) ? 'checked="checked"' : '');
						$html .= '<input type="radio" name="'.$ctrlName.'" id="'.$id.'" value="'.$option['VALUE'].'" '.$checked.' '.$attributes.' />';
						$html .= '<label for="'.$id.'">'.$option['LABEL'].'</label>';
					endforeach;
					break;
                case 'FILE':
                    global $APPLICATION;
                    ob_start();
                    echo \Bitrix\Main\UI\FileInput::createInstance(array(
                        "name" => $ctrlName,
                        "description" => false,
                        "upload" => true,
                        "allowUpload" => !empty($control['FILETYPE']) ? $control['FILETYPE'] : 'A',
                        "allowUploadExt" => !empty($control['FILEEXT']) ? $control['FILEEXT'] : '',
                        "medialib" => false,
                        "fileDialog" => true,
                        "cloud" => false,
                        "delete" => true,
                        "maxCount" => (!empty($control['MULTIPLE']) && $control['MULTIPLE'] == 'Y') ? 10 : 1,
                        "maxSize" => !empty($control['FILESIZE']) ? $control['FILESIZE'] : '',
                    ))->show($ctrlValue);
                    $html .= ob_get_clean();
                    break;
				case 'CUSTOM':
					if(strlen($control['HTML']) > 0)
					{
						$html .= $control['HTML'];
					}
					elseif(is_callable($control['HANDLER']))
					{
						$res = call_user_func($control['HANDLER'], array(
							'CONTROL' => $control,
							'VALUE' => $ctrlValue
						));
						if(is_string($res) && strlen($res) > 0)
						{
							$html .= $res;
						}
					}
					break;
			}
			$html .= '</td></tr>';
		}
		echo $html;
	}
}