<?php
$MESS['APP_CONFIGURATOR_TABS_SETTINGS_TITLE'] = 'Настройки';
$MESS['APP_CONFIGURATOR_TABS_RIGHTS_TITLE'] = 'Доступ';
$MESS['APP_CONFIGURATOR_EMPTY_SETTINGS'] = 'Настройки не найдены. Добавьте их при необходимости в файле settings.php в папке модуля.';