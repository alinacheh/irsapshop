<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();?>
        </main>
        <footer class="footer hidden-des">
            <div class="container">
                <div class="footer-logo">
                    <img src="<?= SITE_TEMPLATE_PATH.'/images/footer-logo.png';?>" class="footer-logo__img" alt="" />
                </div>
                <div>
                    <?$APPLICATION->IncludeComponent("bitrix:highloadblock.list", "social_links_footer_mobile", Array(
                        "BLOCK_ID" => "3",	// ID инфоблока
                        "CHECK_PERMISSIONS" => "N",	// Проверять права доступа
                        "COMPONENT_TEMPLATE" => ".default",
                        "DETAIL_URL" => "",	// Путь к странице просмотра записи
                        "ROWS_PER_PAGE" => "",	// Разбить по страницам количеством
                        "PAGEN_ID" => "page",	// Идентификатор страницы
                        "FILTER_NAME" => "",	// Идентификатор фильтра
                        "SORT_FIELD" => "UF_SORT",	// Поле сортировки
                        "SORT_ORDER" => "ASC",	// Направление сортировки
                    ),
                        false
                    );?>
                </div>
                <div class="contact-footer">
                    <div class="contact-footer__item">
                        <span class="contact-footer__label">Тел:</span>
                        <a href="tel:<?=AppMainHelper::formatPhoneForLink(AppSettingsHelper::get("PHONE"));?>" class="contact-footer__value">{{PHONE}}</a>
                    </div>
                    <div class="contact-footer__item">
                        <span class="contact-footer__label">WhatsApp:</span>
                        <a href="https://api.whatsapp.com/send?phone=<?=AppMainHelper::formatPhoneForLink(AppSettingsHelper::get("WHATSAPP"));?>" target="_blank" class="contact-footer__value">{{WHATSAPP}}</a>
                    </div>
                    <div class="contact-footer__item">
                        <span class="contact-footer__label">E-mail:</span>
                        <a href="mailto:{{EMAIL}}" class="contact-footer__value">{{EMAIL}}</a>
                    </div>
                </div>
                <div class="bottom-footer">
                    <div>{{SITE_NAME}}<br/>Адрес: {{ADDRESS}}</div>
                    <div class="copyright">{{COPYRIGHT}}</div>
                </div>
            </div>
        </footer>
        <footer class="footer hidden-mob">
            <div class="container">
                <div class="footer-logo">
                    <div class="footer-logo__wrap"><img src="<?= SITE_TEMPLATE_PATH.'/images/footer-logo.png';?>" class="footer-logo__img" alt="" /></div>
                </div>
                <div>
                    <?$APPLICATION->IncludeComponent("bitrix:highloadblock.list", "social_links_footer", Array(
                            "BLOCK_ID" => "3",	// ID инфоблока
                            "CHECK_PERMISSIONS" => "N",	// Проверять права доступа
                            "COMPONENT_TEMPLATE" => ".default",
                            "DETAIL_URL" => "",	// Путь к странице просмотра записи
                            "ROWS_PER_PAGE" => "",	// Разбить по страницам количеством
                            "PAGEN_ID" => "page",	// Идентификатор страницы
                            "FILTER_NAME" => "",	// Идентификатор фильтра
                            "SORT_FIELD" => "UF_SORT",	// Поле сортировки
                            "SORT_ORDER" => "ASC",	// Направление сортировки
                        ),
                        false
                    );?>
                </div>
                <div class="contact-footer">
                    <div class="contact-footer__left">
                        <div class="contact-footer__item">{{SITE_NAME}}</div>
                        <div class="contact-footer__item">Адрес: {{ADDRESS}}</div>
                    </div>
                    <div class="contact-footer__middle">
                        <div class="contact-footer__item">
                            <span class="contact-footer__label">Тел:</span>
                            <a href="tel:<?=AppMainHelper::formatPhoneForLink(AppSettingsHelper::get("PHONE"));?>" class="contact-footer__value">{{PHONE}}</a>
                        </div>
                        <div class="contact-footer__item">
                            <span class="contact-footer__label">E-mail:</span>
                            <a href="mailto:{{EMAIL}}" class="contact-footer__value">{{EMAIL}}</a>
                        </div>
                        <div class="contact-footer__item">
                            <span class="contact-footer__label">WhatsApp:</span>
                            <a href="https://api.whatsapp.com/send?phone=<?=AppMainHelper::formatPhoneForLink(AppSettingsHelper::get("WHATSAPP"));?>" target="_blank" class="contact-footer__value">{{WHATSAPP}}</a>
                        </div>
                    </div>
                    <div class="contact-footer__right">
                        <div class="contact-footer__item">
                            <span class="contact-footer__label">Тел отдела продаж:</span>
                            <a href="tel:<?=AppMainHelper::formatPhoneForLink(AppSettingsHelper::get("PHONE_OTDEL_PRODAZH"));?>" class="contact-footer__value">{{PHONE_OTDEL_PRODAZH}}</a>
                        </div>
                        <div class="contact-footer__item">
                            <span class="contact-footer__label">Факс:</span>
                            <a href="mailto:{{FAX}}" class="contact-footer__value">{{FAX}}</a>
                        </div>
                    </div>
                </div>
                <div class="bottom-footer flex_center_between">
                    <div class="copyright">{{COPYRIGHT}}</div>
                    <div>
                        <?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"bottom_menu", 
	array(
		"ROOT_MENU_TYPE" => "bottom",
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "",
		"USE_EXT" => "N",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "Y",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"COMPONENT_TEMPLATE" => "bottom_menu"
	),
	false
);?>
                    </div>
                </div>
            </div>
        </footer>
        <div class="overlay"></div>
    </body>
</html>