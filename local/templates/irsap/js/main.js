$(document).ready(function() {
    function n() {
        $(".menu").toggleClass("active")
    }

    $(".slider-product").each(function(){

    	var length = $(this).find('.slider-product__item').length;

    	if (length > 3) {
    		 $(this).slick({
		        centerMode: !0,
		        centerPadding: "0",
		        slidesToShow: 4,
		        arrows: !0,
		        responsive: [{
		            breakpoint: 800,
		            settings: {
		                slidesToShow: 2
		            }
		        }, {
		            breakpoint: 480,
		            settings: {
		                slidesToShow: 1,
		                centerMode: !0,
		                centerPadding: "20px"
		            }
		        }, {
		            breakpoint: 330,
		            settings: {
		                slidesToShow: 1,
		                centerMode: !0,
		                centerPadding: "5px"
		            }
		        }]
		    })
    	}else{
    		$(this).addClass("noslider");
    	}


    });

    $(".tab-style").click(function(e){
    	e.preventDefault();
    	var bl = $(this).parent().find(".li-cont");
    	$(".li-cont").slideUp();
    	if (bl.css("display") == "none") {bl.slideDown()}else{bl.slideUp()}
    	
    });

    $(".mainmenu__link").click(function(e) {
        e.preventDefault(), n(), $("body").toggleClass("hidd")
    }), $(".hide-menu__link").click(function(e) {
        e.preventDefault(), n()
    }), $(".view-block").click(function(e) {
        e.preventDefault(), $(this).parent().find(".hide-text").show(), $(this).hide()
    });
	
	$('.lazy').Lazy({
        // your configuration goes here
        scrollDirection: 'vertical',
        effect: 'fadeIn',
        visibleOnly: true
    });
});