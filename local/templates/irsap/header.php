<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Page\Asset;

global $APPLICATION;

$dir = $APPLICATION->GetCurPage();
$assetInstance = Asset::getInstance();
?><!DOCTYPE html>
<html>
<head>
    <title><?$APPLICATION->ShowTitle();?></title><?php
    /* Set Strings */
    $assetInstance->addString('<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">');
    $assetInstance->addString('<meta charset="utf-8">');
    $assetInstance->addString('<link rel="apple-touch-icon" sizes="57x57" href="'.SITE_TEMPLATE_PATH.'/images/favicon/apple-icon-57x57.png">');
    $assetInstance->addString('<link rel="apple-touch-icon" sizes="60x60" href="'.SITE_TEMPLATE_PATH.'/images/favicon/apple-icon-60x60.png">');
    $assetInstance->addString('<link rel="apple-touch-icon" sizes="72x72" href="'.SITE_TEMPLATE_PATH.'/images/favicon/apple-icon-72x72.png">');
    $assetInstance->addString('<link rel="apple-touch-icon" sizes="76x76" href="'.SITE_TEMPLATE_PATH.'/images/favicon/apple-icon-76x76.png">');
    $assetInstance->addString('<link rel="apple-touch-icon" sizes="114x114" href="'.SITE_TEMPLATE_PATH.'/images/favicon/apple-icon-114x114.png">');
    $assetInstance->addString('<link rel="apple-touch-icon" sizes="120x120" href="'.SITE_TEMPLATE_PATH.'/images/favicon/apple-icon-120x120.png">');
    $assetInstance->addString('<link rel="apple-touch-icon" sizes="144x144" href="'.SITE_TEMPLATE_PATH.'/images/favicon/apple-icon-144x144.png">');
    $assetInstance->addString('<link rel="apple-touch-icon" sizes="152x152" href="'.SITE_TEMPLATE_PATH.'/images/favicon/apple-icon-152x152.png">');
    $assetInstance->addString('<link rel="apple-touch-icon" sizes="180x180" href="'.SITE_TEMPLATE_PATH.'/images/favicon/apple-icon-180x180.png">');
    $assetInstance->addString('<link rel="icon" type="image/png" sizes="192x192"  href="'.SITE_TEMPLATE_PATH.'/images/favicon/android-icon-192x192.png">');
    $assetInstance->addString('<link rel="icon" type="image/png" sizes="32x32" href="'.SITE_TEMPLATE_PATH.'/images/favicon/favicon-32x32.png">');
    $assetInstance->addString('<link rel="icon" type="image/png" sizes="96x96" href="'.SITE_TEMPLATE_PATH.'/images/favicon/favicon-96x96.png">');
    $assetInstance->addString('<link rel="icon" type="image/png" sizes="16x16" href="'.SITE_TEMPLATE_PATH.'/images/favicon/favicon-16x16.png">');
    $assetInstance->addString('<link rel="manifest" href="'.SITE_TEMPLATE_PATH.'/images/favicon/manifest.json">');
    $assetInstance->addString('<meta name="msapplication-TileColor" content="#ffffff">');
    $assetInstance->addString('<meta name="msapplication-TileImage" content="'.SITE_TEMPLATE_PATH.'/images/favicon/ms-icon-144x144.png">');
    $assetInstance->addString('<meta name="theme-color" content="#ffffff">');
    /* Set Head */
    $APPLICATION->ShowHead();
    /* Set CSS */
    $assetInstance->addCss(SITE_TEMPLATE_PATH."/css/font-awesome.min.css");
    $assetInstance->addCss("https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/base/jquery-ui.css");
    $assetInstance->addCss(SITE_TEMPLATE_PATH."/css/iThing.css");
    $assetInstance->addCss(SITE_TEMPLATE_PATH."/fancybox/jquery.fancybox.min.css");
    $assetInstance->addCss(SITE_TEMPLATE_PATH."/css/nouislider.css");
    $assetInstance->addCss(SITE_TEMPLATE_PATH."/css/style.css");
    /* Set JS */
    $assetInstance->addJs(SITE_TEMPLATE_PATH.'/js/polyfills.js');
    $assetInstance->addJs("https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js");
    $assetInstance->addJs("https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js");
//$assetInstance->addJs("https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js");
	$assetInstance->addJs(SITE_TEMPLATE_PATH.'/js/jquery.bxslider.js');
    $assetInstance->addJs(SITE_TEMPLATE_PATH.'/js/nouislider.min.js');
    $assetInstance->addJs(SITE_TEMPLATE_PATH.'/js/jquery.maskedinput.js');
    $assetInstance->addJs(SITE_TEMPLATE_PATH.'/js/slick.js');
    $assetInstance->addJs(SITE_TEMPLATE_PATH.'/fancybox/jquery.fancybox.min.js');
    $assetInstance->addJs(SITE_TEMPLATE_PATH.'/js/jquery.lazy.min.js');
    $assetInstance->addJs(SITE_TEMPLATE_PATH.'/js/main.js');
    $assetInstance->addJs(SITE_TEMPLATE_PATH.'/js/js.js');
    CJSCore::Init([]);?>

<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(62433241, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true,
        ecommerce:"dataLayer"
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/62433241" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-179167545-2"></script>
<script>
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());

 gtag('config', 'UA-179167545-2');
</script>

</head>
<body>
    <div id="panel"><?php $APPLICATION->ShowPanel();?></div>
    <header class="header hidden-mob">
        <div class="header-top">
            <div class="container">
                <a href="mailto:{{EMAIL}}">{{EMAIL}}</a> <span>{{SCHEDULE}}</span>
            </div>
        </div>
        <div class="header-bottom bg_white">
            <div class="container">
                <div class="header-bottom__content flex_center_between">
                    <div class="header-bottom__left flex_stretch_start">
                        <div class="logo">
                            <a href="/" class="logo__link flex_center">
                                <img src="<?=CFile::GetPath(AppSettingsHelper::get('LOGO_PATH'))?>" alt="{{LOGO_ALT}}" title="{{LOGO_TITLE}}" />
                            </a>
                        </div>
                        <div class="all-radiators">
                            <a href="/catalog/" class="all-radiators__link">
                                <div class="all-radiators__title top-headerline">Каталог</div>
                                <div class="all-radiators__desc">все радиаторы</div>
                            </a>
                        </div>
                        <div class="header-soc">
                            <?$APPLICATION->IncludeComponent("bitrix:highloadblock.list", "social_links_header", Array(
                                    "BLOCK_ID" => "2",	// ID инфоблока
                                    "CHECK_PERMISSIONS" => "N",	// Проверять права доступа
                                    "COMPONENT_TEMPLATE" => ".default",
                                    "DETAIL_URL" => "",	// Путь к странице просмотра записи
                                    "ROWS_PER_PAGE" => "",	// Разбить по страницам количеством
                                    "PAGEN_ID" => "page",	// Идентификатор страницы
                                    "FILTER_NAME" => "",	// Идентификатор фильтра
                                    "SORT_FIELD" => "UF_SORT",	// Поле сортировки
                                    "SORT_ORDER" => "ASC",	// Направление сортировки
                                ),
                                false
                            );?>
                            <div class="header-soc__slogan">{{SLOGAN}}</div>
                        </div>
                    </div>
                    <div class="header-bottom__right">
                        <div class="right-top flex_end_center top-headerline">
                            <a href="tel:<?=AppMainHelper::formatPhoneForLink(AppSettingsHelper::get("PHONE"));?>">{{PHONE}}</a>
                            <div class="right-top__info">
                                <div id="cart-header" class="cart-header-block">
                                    <?$APPLICATION->IncludeFile('/local/templates/irsap/ajax/basket_line.php');?>
                                </div>
                                <a href="/personal/" class=""><img src="<?= SITE_TEMPLATE_PATH.'/images/icon-user.png';?>"> кабинет</a>
                            </div>
                        </div>
                        <div>
                            <?$APPLICATION->IncludeComponent(
								"bitrix:menu", 
								"top_menu", 
								array(
									"ROOT_MENU_TYPE" => "top",
									"MAX_LEVEL" => "1",
									"CHILD_MENU_TYPE" => "",
									"USE_EXT" => "N",
									"DELAY" => "N",
									"ALLOW_MULTI_SELECT" => "Y",
									"MENU_CACHE_TYPE" => "A",
									"MENU_CACHE_TIME" => "3600",
									"MENU_CACHE_USE_GROUPS" => "N",
									"MENU_CACHE_GET_VARS" => array(
									),
									"COMPONENT_TEMPLATE" => "top_menu"
								),
								false
							);?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <header class="headermob hidden-des">
        <div class="headermob-top">
            <div class="headermob-top__left mainmenu">
                <a href="#" class="mainmenu__link">
                    <span></span>
                    <span></span>
                    <span></span>
                </a>
            </div>
            <div class="headermob-top__center">
                <a href="/" class="logo">
                    <img src="<?=CFile::GetPath(AppSettingsHelper::get('LOGO_PATH'))?>" alt="{{LOGO_ALT}}" title="{{LOGO_TITLE}}" />
                </a>
                <div class="time-top">{{SCHEDULE}}</div>
            </div>
            <div class="icon-top">
                <a href="tel:<?=AppMainHelper::formatPhoneForLink(AppSettingsHelper::get("PHONE"));?>" class="icon-top__link">
                    <img src="<?= SITE_TEMPLATE_PATH.'/images/icon-phone.png';?>" alt="" />
                </a>
                <div id="cart-header-mobile">
					<?$APPLICATION->IncludeFile('/local/templates/irsap/ajax/basket_line_mobile.php');?>
				</div>
                <a href="/personal/" class="icon-top__link">
                    <img src="<?= SITE_TEMPLATE_PATH.'/images/icon-user.png';?>" alt="" />
                </a>
            </div>
        </div>
        <div class="headermob-menu">
            <div class="menu">
                <div>
                    <?$APPLICATION->IncludeComponent("bitrix:menu", "top_menu_mobile",Array(
                            "ROOT_MENU_TYPE" => "top",
                            "MAX_LEVEL" => "1",
                            "CHILD_MENU_TYPE" => "",
                            "USE_EXT" => "N",
                            "DELAY" => "N",
                            "ALLOW_MULTI_SELECT" => "Y",
                            "MENU_CACHE_TYPE" => "N",
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "MENU_CACHE_GET_VARS" => ""
                        )
                    );?>
                </div>
                <div>
                    <?$APPLICATION->IncludeComponent("bitrix:catalog.section", "product_list_menu_mobile", Array(
                            "ACTION_VARIABLE" => "action",	// Название переменной, в которой передается действие
                            "ADD_PICT_PROP" => "-",	// Дополнительная картинка основного товара
                            "ADD_PROPERTIES_TO_BASKET" => "N",	// Добавлять в корзину свойства товаров и предложений
                            "ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
                            "ADD_TO_BASKET_ACTION" => "ADD",	// Показывать кнопку добавления в корзину или покупки
                            "AJAX_MODE" => "N",	// Включить режим AJAX
                            "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
                            "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
                            "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
                            "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
                            "BACKGROUND_IMAGE" => "UF_BACKGROUND_IMAGE",	// Установить фоновую картинку для шаблона из свойства
                            "BASKET_URL" => "/personal/basket.php",	// URL, ведущий на страницу с корзиной покупателя
                            "BRAND_PROPERTY" => "BRAND_REF",
                            "BROWSER_TITLE" => "-",	// Установить заголовок окна браузера из свойства
                            "CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
                            "CACHE_GROUPS" => "N",	// Учитывать права доступа
                            //"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
                            "CACHE_TIME" => "864000",	// Время кеширования (сек.)
                            //"CACHE_TYPE" => "A",	// Тип кеширования
                            "CACHE_TYPE" => "Y",	// Тип кеширования
                            "COMPATIBLE_MODE" => "Y",	// Включить режим совместимости
                            "CONVERT_CURRENCY" => "Y",	// Показывать цены в одной валюте
                            "CURRENCY_ID" => "RUB",	// Валюта, в которую будут сконвертированы цены
                            "CUSTOM_FILTER" => "{\"CLASS_ID\":\"CondGroup\",\"DATA\":{\"All\":\"AND\",\"True\":\"True\"},\"CHILDREN\":[]}",
                            "DATA_LAYER_NAME" => "dataLayer",
                            "DETAIL_URL" => "",	// URL, ведущий на страницу с содержимым элемента раздела
                            "DISABLE_INIT_JS_IN_COMPONENT" => "N",	// Не подключать js-библиотеки в компоненте
                            "DISCOUNT_PERCENT_POSITION" => "bottom-right",
                            "DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
                            "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
                            "ELEMENT_SORT_FIELD" => "sort",	// По какому полю сортируем элементы
                            "ELEMENT_SORT_FIELD2" => "id",	// Поле для второй сортировки элементов
                            "ELEMENT_SORT_ORDER" => "asc",	// Порядок сортировки элементов
                            "ELEMENT_SORT_ORDER2" => "desc",	// Порядок второй сортировки элементов
                            "ENLARGE_PRODUCT" => "PROP",	// Выделять товары в списке
                            "ENLARGE_PROP" => "-",	// Выделять по выбранному свойству
                            "FILTER_NAME" => "arMenuFilter",	// Имя массива со значениями фильтра для фильтрации элементов
                            "HIDE_NOT_AVAILABLE" => "Y",	// Недоступные товары
                            "HIDE_NOT_AVAILABLE_OFFERS" => "N",	// Недоступные торговые предложения
                            "IBLOCK_ID" => "1",	// Инфоблок
                            "IBLOCK_TYPE" => "catalog",	// Тип инфоблока
                            "INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
                            "LABEL_PROP" => "",	// Свойства меток товара
                            "LABEL_PROP_MOBILE" => "",
                            "LABEL_PROP_POSITION" => "top-left",
                            "LAZY_LOAD" => "N",	// Показать кнопку ленивой загрузки Lazy Load
                            "LINE_ELEMENT_COUNT" => "3",	// Количество элементов выводимых в одной строке таблицы
                            "LOAD_ON_SCROLL" => "N",	// Подгружать товары при прокрутке до конца
                            "MESSAGE_404" => "",
                            "MESS_BTN_ADD_TO_BASKET" => "В корзину",	// Текст кнопки "Добавить в корзину"
                            "MESS_BTN_BUY" => "Купить",	// Текст кнопки "Купить"
                            "MESS_BTN_DETAIL" => "Подробнее",	// Текст кнопки "Подробнее"
                            "MESS_BTN_LAZY_LOAD" => "Показать ещё",	// Текст кнопки "Показать ещё"
                            "MESS_BTN_SUBSCRIBE" => "Подписаться",	// Текст кнопки "Уведомить о поступлении"
                            "MESS_NOT_AVAILABLE" => "Нет в наличии",	// Сообщение об отсутствии товара
                            "META_DESCRIPTION" => "-",	// Установить описание страницы из свойства
                            "META_KEYWORDS" => "-",	// Установить ключевые слова страницы из свойства
                            "OFFERS_CART_PROPERTIES" => array(
                                0 => "ARTNUMBER",
                                1 => "COLOR_REF",
                                2 => "SIZES_SHOES",
                                3 => "SIZES_CLOTHES",
                            ),
                            "OFFERS_FIELD_CODE" => array(	// Поля предложений
                                0 => "",
                                1 => "",
                            ),
                            "OFFERS_LIMIT" => "5",	// Максимальное количество предложений для показа (0 - все)
                            "OFFERS_PROPERTY_CODE" => array(
                                0 => "COLOR_REF",
                                1 => "SIZES_SHOES",
                                2 => "SIZES_CLOTHES",
                                3 => "",
                            ),
                            "OFFERS_SORT_FIELD" => "sort",	// По какому полю сортируем предложения товара
                            "OFFERS_SORT_FIELD2" => "id",	// Поле для второй сортировки предложений товара
                            "OFFERS_SORT_ORDER" => "asc",	// Порядок сортировки предложений товара
                            "OFFERS_SORT_ORDER2" => "desc",	// Порядок второй сортировки предложений товара
                            "OFFER_ADD_PICT_PROP" => "MORE_PHOTO",
                            "OFFER_TREE_PROPS" => array(
                                0 => "COLOR_REF",
                                1 => "SIZES_SHOES",
                                2 => "SIZES_CLOTHES",
                            ),
                            "PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
                            "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
                            "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
                            "PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
                            "PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
                            "PAGER_TITLE" => "Товары",	// Название категорий
                            "PAGE_ELEMENT_COUNT" => "50",	// Количество элементов на странице
                            "PARTIAL_PRODUCT_PROPERTIES" => "N",	// Разрешить добавлять в корзину товары, у которых заполнены не все характеристики
                            "PRICE_CODE" => array(	// Тип цены
                                0 => "BASE",
                            ),
                            "PRICE_VAT_INCLUDE" => "Y",	// Включать НДС в цену
                            "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons,compare",	// Порядок отображения блоков товара
                            "PRODUCT_DISPLAY_MODE" => "N",	// Схема отображения
                            "PRODUCT_ID_VARIABLE" => "id",	// Название переменной, в которой передается код товара для покупки
                            "PRODUCT_PROPERTIES" => array(
                                0 => "NEWPRODUCT",
                                1 => "MATERIAL",
                            ),
                            "PRODUCT_PROPS_VARIABLE" => "prop",	// Название переменной, в которой передаются характеристики товара
                            "PRODUCT_QUANTITY_VARIABLE" => "",	// Название переменной, в которой передается количество товара
                            "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':true}]",	// Вариант отображения товаров
                            "PRODUCT_SUBSCRIPTION" => "N",	// Разрешить оповещения для отсутствующих товаров
                            "PROPERTY_CODE" => array(
                                0 => "LABEL",
                                1 => "",
                            ),
                            "PROPERTY_CODE_MOBILE" => "",
                            "RCM_PROD_ID" => "",	// Параметр ID продукта (для товарных рекомендаций)
                            "RCM_TYPE" => "personal",	// Тип рекомендации
                            "SECTION_CODE" => "",	// Код раздела
                            "SECTION_ID" => "",	// ID раздела
                            "SECTION_ID_VARIABLE" => "",	// Название переменной, в которой передается код группы
                            "SECTION_URL" => "",	// URL, ведущий на страницу с содержимым раздела
                            "SECTION_USER_FIELDS" => array(	// Свойства раздела
                                0 => "",
                                1 => "",
                            ),
                            "SEF_MODE" => "N",	// Включить поддержку ЧПУ
                            "SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
                            "SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
                            "SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
                            "SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
                            "SET_STATUS_404" => "N",	// Устанавливать статус 404
                            "SET_TITLE" => "N",	// Устанавливать заголовок страницы
                            "SHOW_404" => "N",	// Показ специальной страницы
                            "SHOW_ALL_WO_SECTION" => "Y",	// Показывать все элементы, если не указан раздел
                            "SHOW_CLOSE_POPUP" => "N",	// Показывать кнопку продолжения покупок во всплывающих окнах
                            "SHOW_DISCOUNT_PERCENT" => "N",	// Показывать процент скидки
                            "SHOW_FROM_SECTION" => "N",	// Показывать товары из раздела
                            "SHOW_MAX_QUANTITY" => "N",	// Показывать остаток товара
                            "SHOW_OLD_PRICE" => "N",	// Показывать старую цену
                            "SHOW_PRICE_COUNT" => "1",	// Выводить цены для количества
                            "SHOW_SLIDER" => "N",	// Показывать слайдер для товаров
                            "SLIDER_INTERVAL" => "3000",
                            "SLIDER_PROGRESS" => "N",
                            "TEMPLATE_THEME" => "blue",	// Цветовая тема
                            "USE_ENHANCED_ECOMMERCE" => "N",	// Отправлять данные электронной торговли в Google и Яндекс
                            "USE_MAIN_ELEMENT_SECTION" => "N",	// Использовать основной раздел для показа элемента
                            "USE_PRICE_COUNT" => "N",	// Использовать вывод цен с диапазонами
                            "USE_PRODUCT_QUANTITY" => "N",	// Разрешить указание количества товара
                            "COMPONENT_TEMPLATE" => ".default",
                            "DISPLAY_COMPARE" => "N",	// Разрешить сравнение товаров
                            "FILE_404" => "",	// Страница для показа (по умолчанию /404.php)
                            "BLOCK_TITLE" => "Каталог",
                        ),
                        false
                    );?>
                </div>
                <a href="#" class="hide-menu__link">свернуть меню</a>
            </div>
        </div>
    </header>
    <main class="main">