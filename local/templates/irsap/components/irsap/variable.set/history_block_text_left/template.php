<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

/** @var array $arParams */
?>
<section class="sec-padding__null" style="background-image: url(<?=$arParams['bg'];?>)">
    <div class="info info-back__4">
        <div class="container">
            <div class="flex_start_center">
                <div class="info__desp flex50">
                    <h2><?=$arParams['~header'];?></h2>
                    <p class="info__text"><?=nl2br($arParams['~text']);?></p>
                </div>
            </div>
        </div>
    </div>
</section>