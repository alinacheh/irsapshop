<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

/** @var array $arParams */
?>
<div class="contact-store__info">
    <p class="contact-store__text text-bold"><?=$arParams['~address'];?></p>
    <ul>
        <?foreach ($arParams['~stores'] as $store):?>
			<?if(!empty($store)){?>
				<li class="list-li__red"><?=$store;?></li>
			<?}?>
        <?endforeach;?>
    </ul>
</div>