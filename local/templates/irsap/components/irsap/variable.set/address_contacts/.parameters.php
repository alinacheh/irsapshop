<?php

$set = array(
    'address' => 'Адрес',
    'stores' => [
        'NAME' => 'Склады',
        'TYPE' => 'STRING',
        'MULTIPLE' => 'Y'
    ],
);

$arTemplateParameters = array();
foreach ($set as $k => $val) {
    if (is_array($val)) {
        $arTemplateParameters[$k] = $val;
    } else {
        $arTemplateParameters[$k] = array(
            'NAME' => $val,
            'COLS' => 35,
            'ROWS' => 3
        );
    }
}
