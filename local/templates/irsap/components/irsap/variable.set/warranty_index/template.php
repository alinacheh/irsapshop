<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

/** @var array $arParams */
?>
<section class="garant-index" style="background-image: url(<?=$arParams['bg'];?>)">
    <div class="container">
        <div class="garant-index__content">
            <div class="flex50">
                <img src="<?=$arParams['pic'];?>" alt="<?=$arParams['pic_alt'];?>" title="<?=$arParams['pic_title'];?>" />
            </div>
            <div class="flex50">
                <h2><?=$arParams['~header'];?></h2>
                <p><?=nl2br($arParams['~text']);?></p>
            </div>
        </div>
    </div>
</section>