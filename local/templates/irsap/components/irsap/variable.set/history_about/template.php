<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

/** @var array $arParams */
?>
<section class="about-history">
    <div class="container">
        <div class="about-info">
            <h2 class="about-info__title"><?=$arParams['~header'];?></h2>
            <div class="about-info__box">
                <p class="about-info__text"><?=nl2br($arParams['~text']);?></p>
            </div>
            <a href="#" class="view-block style_btn2 hidden-des">Читать далее</a>
        </div>
    </div>
</section>