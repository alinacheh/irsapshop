<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

/** @var array $arParams */
?>
<div class="contact-store__info contact-info flex_start">
    <div class="info-tel">
        <p class="contact-info__title text-bold">Телефон</p>
        <div class="info-tel__box">
            <?foreach ($arParams['~phones'] as $phone):?>
                <div class="info-tel__number"><?=$phone;?></div>
            <?endforeach;?>
        </div>
    </div>
    <div class="info-mail">
        <p class="contact-info__title text-bold">E-Mail</p>
        <?foreach ($arParams['~emails'] as $email):?>
            <div class="info-mail__address text-bold">
                <a href="mailto:<?=$email;?>" class="info-mail__address-link"><?=$email;?></a>
            </div>
        <?endforeach;?>
    </div>
    <div class="info-mode">
        <p class="contact-info__title text-bold">Режим работы</p>
        <?foreach ($arParams['~schedules'] as $schedule):?>
            <div class="info-mode__address"><?=$schedule;?></div>
        <?endforeach;?>
    </div>
</div>