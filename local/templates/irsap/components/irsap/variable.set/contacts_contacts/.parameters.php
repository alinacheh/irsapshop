<?php

$set = array(
    'phones' => [
        'NAME' => 'Телефон',
        'TYPE' => 'STRING',
        'MULTIPLE' => 'Y'
    ],
    'emails' => [
        'NAME' => 'E-mail',
        'TYPE' => 'STRING',
        'MULTIPLE' => 'Y'
    ],
    'schedules' => [
        'NAME' => 'Режим работы',
        'TYPE' => 'STRING',
        'MULTIPLE' => 'Y'
    ],
);

$arTemplateParameters = array();
foreach ($set as $k => $val) {
    if (is_array($val)) {
        $arTemplateParameters[$k] = $val;
    } else {
        $arTemplateParameters[$k] = array(
            'NAME' => $val,
            'COLS' => 35,
            'ROWS' => 3
        );
    }
}
