<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

/** @var array $arParams */
?>
<section class="history-index" style="background-image: url(<?=$arParams['bg'];?>)">
    <div class="container">
        <h2><?=$arParams['~header'];?></h2>
        <p><?=nl2br($arParams['~text']);?></p>
    </div>
</section>