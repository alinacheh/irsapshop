<?php

$set = array(
    'header' => 'Заголовок',
    'text' => 'Текст',
    'bg' => [
        'NAME' => 'Фоновая картинка',
        'TYPE' => 'FILE',
        "FD_TARGET" => "F",
        "FD_EXT" => 'jpg,jpeg,png',
        "FD_UPLOAD" => true,
        "FD_USE_MEDIALIB" => true,
    ],
);

$arTemplateParameters = array();
foreach ($set as $k => $val) {
    if (is_array($val)) {
        $arTemplateParameters[$k] = $val;
    } else {
        $arTemplateParameters[$k] = array(
            'NAME' => $val,
            'COLS' => 35,
            'ROWS' => 3
        );
    }
}
