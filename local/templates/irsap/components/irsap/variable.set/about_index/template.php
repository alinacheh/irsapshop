<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

/** @var array $arParams */
?>
<section class="about-index" style="background-image: url(<?=$arParams['bg'];?>)">
    <div class="container">
        <div class="about-index__content flex_center_start">
            <div class="flex50"></div>
            <div class="flex50">
                <h1 class="h2" style="text-align: left; color: #565656; text-shadow: none;"><?=$arParams['~header'];?></h1>
                <p><?=$arParams['~text'];?></p>
                <a href="#" class="style_btn2 hidden-des view-block">Читать далее</a>
            </div>
        </div>
    </div>
</section>