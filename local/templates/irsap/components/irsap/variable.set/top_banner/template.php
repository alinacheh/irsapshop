<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

/** @var array $arParams */
?>
<div class="common-banner">
    <h1 class="h1 page-title"><?=$arParams['~title'];?></h1>
    <img
        src="<?=$arParams['image'];?>"
        alt="<?=$arParams['image_alt_attr'];?>"
        title="<?=$arParams['image_title_attr'];?>"
        class="banner-img hidden-banner__des"
    >
    <img
        src="<?=$arParams['mobile_image'];?>"
        alt="<?=$arParams['image_alt_attr'];?>"
        title="<?=$arParams['image_title_attr'];?>"
        class="banner-img hidden-banner__mob"
    >
</div>