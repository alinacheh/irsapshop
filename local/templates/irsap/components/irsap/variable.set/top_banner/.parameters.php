<?php

$set = array(
    'title' => 'Заголовок',
    'image' => [
        'NAME' => 'Фоновая картинка',
        'TYPE' => 'FILE',
        "FD_TARGET" => "F",
        "FD_EXT" => 'jpg,jpeg,png',
        "FD_UPLOAD" => true,
        "FD_USE_MEDIALIB" => true,
    ],
    'image_alt_attr' => 'ALT фоновой картинки',
    'image_title_attr' => 'TITLE фоновой картинки',
    'mobile_image' => [
        'NAME' => 'Фоновая картинка (mobile)',
        'TYPE' => 'FILE',
        "FD_TARGET" => "F",
        "FD_EXT" => 'jpg,jpeg,png',
        "FD_UPLOAD" => true,
        "FD_USE_MEDIALIB" => true,
    ],
);

$arTemplateParameters = array();
foreach ($set as $k => $val) {
    if (is_array($val)) {
        $arTemplateParameters[$k] = $val;
    } else {
        $arTemplateParameters[$k] = array(
            'NAME' => $val,
            'COLS' => 35,
            'ROWS' => 3
        );
    }
}
