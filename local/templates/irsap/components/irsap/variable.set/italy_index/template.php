<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

/** @var array $arParams */
?>
<section class="italy-index" style="background-image: url(<?=$arParams['bg'];?>)">
    <div class="container">
        <div class="flex_start_center">
            <div class="flex50">
                <h2><?=$arParams['~header'];?></h2>
                <p><?=nl2br($arParams['~text']);?></p>
            </div>
        </div>
    </div>
</section>