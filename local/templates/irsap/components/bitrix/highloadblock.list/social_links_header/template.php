<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!empty($arResult['rows'])):?>
    <div class="header-soc__ul top-headerline"><?
        foreach($arResult['rows'] as $arItem):
            ?>
            <a href="<?=$arItem['UF_LINK'];?>" target="_blank" class="header-soc__link">
                <?=$arItem['UF_IMAGE'];?>
            </a><?
        endforeach;?>
    </div>
<?endif;?>