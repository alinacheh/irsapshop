<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!empty($arResult['rows'])):?>
    <nav class="soc-footer"><?
        foreach($arResult['rows'] as $arItem):
            ?>
            <a href="<?=$arItem['UF_LINK'];?>" target="_blank" class="soc-footer__link">
                <i class="fa <?=$arItem['UF_ICON'];?>" aria-hidden="true"></i>
            </a><?
        endforeach;?>
    </nav>
<?endif;?>