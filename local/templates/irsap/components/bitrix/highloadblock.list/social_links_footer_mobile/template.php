<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!empty($arResult['rows'])):?>
    <nav class="soc-footer"><?
        foreach($arResult['rows'] as $arItem):
            ?>
            <a href="<?=$arItem['UF_LINK'];?>" target="_blank" class="soc-footer__link">
                <?=$arItem['UF_IMAGE_MOB'];?>
            </a><?
        endforeach;?>
    </nav>
<?endif;?>