<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/**
 * @global string $componentPath
 * @global string $templateName
 * @var CBitrixComponentTemplate $this
 */

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

?>
<a href="<?=$arParams['PATH_TO_BASKET'];?>" class="icon-top__link">
	<img src="<?= SITE_TEMPLATE_PATH.'/images/icon-basket.png';?>" alt="" />
	<?if ((int) $arResult['NUM_PRODUCTS']):?>
		<div class="mob-basket">
			<span class="mob-basket__cont"><?=$arResult['NUM_PRODUCTS'];?></span>
		</div>
	<?endif;?>
</a>


<script>
    new JCIrsapSmallBasketMobileComponent;
</script>
