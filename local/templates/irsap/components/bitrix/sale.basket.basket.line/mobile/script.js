// JCIrsapSmallBasketMobileComponent - компонент для работы малой корзины

(function() {
    'use strict';

    if (!!window.JCIrsapSmallBasketMobileComponent)
        return;

    if (!window.BX)
        return;

    window.JCIrsapSmallBasketMobileComponent = function(params) {
        this.blockId = 'cart-header-mobile';
        this.blockIdSelector = '#' + this.blockId;
        this.actionUrl = '/local/templates/irsap/ajax/basket_line_mobile.php';

        this.initHandlers();
    };

    window.JCIrsapSmallBasketMobileComponent.prototype = {
        initHandlers : function () {
            BX.addCustomEvent(window, 'OnBasketChange', BX.delegate(this.refreshCart, this));
        },

        refreshCart : function () {
            var basketWrapper = $(this.blockIdSelector);
            $.get(
                this.actionUrl,
                function (out) {
                    basketWrapper.html(out);
                }
            );
        }
    };

})();