<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/**
 * @global string $componentPath
 * @global string $templateName
 * @var CBitrixComponentTemplate $this
 */

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

?>
<a href="<?=$arParams['PATH_TO_BASKET'];?>">
    <img src="<?=SITE_TEMPLATE_PATH.'/images/icon-basket.png';?>"> заказ: <?=$arResult['NUM_PRODUCTS'];?>
</a>

<script>
    new JCIrsapSmallBasketComponent;
</script>
