// JCIrsapSmallBasketComponent - компонент для работы малой корзины

(function() {
    'use strict';

    if (!!window.JCIrsapSmallBasketComponent)
        return;

    if (!window.BX)
        return;

    window.JCIrsapSmallBasketComponent = function(params) {
        this.blockId = 'cart-header';
        this.blockIdSelector = '#' + this.blockId;
        this.actionUrl = '/local/templates/irsap/ajax/basket_line.php';

        this.initHandlers();
    };

    window.JCIrsapSmallBasketComponent.prototype = {
        initHandlers : function () {
            BX.addCustomEvent(window, 'OnBasketChange', BX.delegate(this.refreshCart, this));
        },

        refreshCart : function () {
            var basketWrapper = $(this.blockIdSelector);
            $.get(
                this.actionUrl,
                function (out) {
                    basketWrapper.html(out);
                }
            );
        }
    };

})();