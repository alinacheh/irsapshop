<?
$MESS ['CHP_AUTH_CHANGE_PASSWORD'] = "Смена пароля";
$MESS ['CHP_LOGIN'] = "Логин:";
$MESS ['CHP_CHECKWORD'] = "Контрольная строка:";
$MESS ['CHP_NEW_PASSWORD'] = "Новый пароль (мин. 6 символов):";
$MESS ['CHP_NEW_PASSWORD_CONFIRM'] = "Подтверждение пароля:";
$MESS ['CHP_CHANGE'] = "Изменить пароль";
$MESS ['CHP_REQ'] = "Обязательные поля";
$MESS ['CHP_AUTH'] = "Авторизация";
$MESS ['CHP_NEW_PASSWORD_REQ'] = "Новый пароль (мин. 6 символов):";
$MESS["CHP_SECURE_NOTE"]="Перед отправкой формы пароль будет зашифрован в браузере. Это позволит избежать передачи пароля в открытом виде.";
$MESS["CHP_NONSECURE_NOTE"]="Пароль будет отправлен в открытом виде. Включите JavaScript в браузере, чтобы зашифровать пароль перед отправкой.";
?>