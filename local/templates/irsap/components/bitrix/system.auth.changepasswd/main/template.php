<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
?>
<div class="form-cmn flex_stretch_between pb">
    <h2><?=Loc::getMessage("CHP_AUTH_CHANGE_PASSWORD") ?></h2>
</div>
<?php if ($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($arParams['~AUTH_RESULT'])):?>
    <div class="form-cmn flex_stretch_between">
        <?ShowMessage($arParams["~AUTH_RESULT"]);?>
    </div>
<?php endif;?>
<div class="form-cmn flex_stretch_between">
    <div class="flex50">
        <form method="post" id="change_password_form" action="<?=SITE_DIR?>auth/change-password/" name="bform">
            <?if (strlen($arResult["BACKURL"]) > 0): ?>
                <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
            <? endif ?>
            <input type="hidden" name="AUTH_FORM" value="Y">
            <input type="hidden" name="TYPE" value="CHANGE_PWD">

            <div class="form-cmn-row flex_center_start">
                <label for="" class="form-cmn-label"><?=Loc::getMessage('CHP_LOGIN')?></label>
                <input type="text" name="USER_LOGIN" class="input_style" value="<?=$arResult["LAST_LOGIN"];?>">
            </div>
            <div class="form-cmn-row flex_center_start">
                <label for="" class="form-cmn-label"><?=Loc::getMessage('CHP_CHECKWORD')?></label>
                <input type="text" name="USER_CHECKWORD" class="input_style" value="<?=$arResult["USER_CHECKWORD"];?>">
            </div>
            <div class="form-cmn-row flex_center_start">
                <label for="" class="form-cmn-label"><?=Loc::getMessage('CHP_NEW_PASSWORD_REQ')?></label>
                <input type="password" name="USER_PASSWORD" class="input_style" value="<?=$arResult["USER_PASSWORD"];?>">
            </div>
            <div class="form-cmn-row flex_center_start">
                <label for="" class="form-cmn-label"><?=Loc::getMessage('CHP_NEW_PASSWORD_CONFIRM')?></label>
                <input type="password" name="USER_CONFIRM_PASSWORD" class="input_style" value="<?=$arResult["USER_CONFIRM_PASSWORD"];?>">
            </div>
            <div class="form-cmn-btn flex_center_start">
                <button class="style_btn3" type="submit" name="change_pwd"><?=Loc::getMessage("CHP_CHANGE")?></button>
            </div>
        </form>
    </div>
</div>

