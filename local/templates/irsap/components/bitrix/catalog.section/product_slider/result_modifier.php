<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogSectionComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

/** Irsap Customs */

foreach ($arResult['ITEMS'] as &$item) {
    if (!empty($item['PREVIEW_PICTURE']) && !empty($item['IPROPERTY_VALUES'])) {
        if (!empty($item['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_ALT'])) {
            $item['PREVIEW_PICTURE']['ALT'] = $item['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_ALT'];
        }
        if (!empty($item['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'])) {
            $item['PREVIEW_PICTURE']['TITLE'] = $item['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'];
        }
    }
}
unset($item);