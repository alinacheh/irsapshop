<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 */

$this->setFrameMode(true);

$uniqueId = $this->randString();
$elementEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT');
$elementDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE');
$elementDeleteParams = array('CONFIRM' => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));

if (empty($arResult['ITEMS'])) {
    return;
}?>
<div id="product-slider-<?=$uniqueId;?>" class="container">
    <h2 class="section-product">
        <span><?=$arParams['BLOCK_TITLE'];?></span>
    </h2>
    <div class="slider-product">
        <?foreach ($arResult['ITEMS'] as $item):?>
            <div class="slider-product__item" data-entity="product-item" data-product-id="<?=$item['ID'];?>">
                <div class="slider-product__top">
                    <?if (!empty($item['PROPERTIES']['LABEL']) && !empty($item['PROPERTIES']['LABEL']['VALUE'])):?>
                        <div class="label-product hidden-des"><?=$item['PROPERTIES']['LABEL']['VALUE'];?></div>
                    <?endif;?>
                    <img
                        src="<?=$item['PREVIEW_PICTURE']['SRC'];?>"
                        alt="<?=$item['PREVIEW_PICTURE']['ALT'];?>"
                        title="<?=$item['PREVIEW_PICTURE']['TITLE'];?>"
                    >
                </div>
                <div class="slider-product__bottom">
                    <a href="javascript:void(0)" class="slider-product__name"><?=$item['NAME'];?></a>
                    <div class="slider-product__desc"><?=$item['PREVIEW_TEXT'];?></div>
                    <div class="price-buy">
                        <div class="slider-product__price">
                            <div class="label_price">Цена:</div>
                            <?
                            $price = round($item['MIN_PRICE']['DISCOUNT_VALUE'], 2);
                            $currency = AppMainHelper::getPublicCurrency($item['MIN_PRICE']['CURRENCY']);
                            ?>
                            <span class="price"><?=$price;?>&nbsp;<span class="hidden-des"><?=$currency;?></span></span>
                        </div>
                        <div class="slider-product__buy">
                            <a href="javascript:void(0)" data-entity="cart-btn" class="style_btn3">Добавить в заказ</a>
                            <?if (!empty($item['PROPERTIES']['LABEL']) && !empty($item['PROPERTIES']['LABEL']['VALUE'])):?>
                                <span class="label-product hidden-mob"><?=$item['PROPERTIES']['LABEL']['VALUE'];?></span>
                            <?endif;?>
                        </div>
                    </div>
                </div>
            </div>
        <?endforeach;?>
    </div>
</div>
<?php

$jsParams = [
    'BLOCK_SELECTOR' => '#product-slider-' . $uniqueId,
];

?>
<script>
    // JCIrsapProductSliderComponent - компонент для обработки логики слайдера товаров

    (function() {
        'use strict';

        if (!!window.JCIrsapProductSliderComponent)
            return;

        if (!window.BX)
            return;

        window.JCIrsapProductSliderComponent = function (params) {
            this.actionsUrl = '/local/templates/irsap/ajax/actions.php';
            this.blockSelector = params.BLOCK_SELECTOR;
            this.initHandlers();
        };

        window.JCIrsapProductSliderComponent.prototype = {

            entity : function(entity, selector) {
                if (!entity) {
                    return null;
                }
                return $(selector || this.blockSelector).find('[data-entity="' + entity + '"]');
            },

            initHandlers : function() {
                var self = this;

                $(this.blockSelector).on('click', '[data-entity="cart-btn"]', function(e) {
                    e.stopPropagation();
                    var product = $(e.currentTarget).closest('[data-entity="product-item"]');
                    if (!product) {
                        return;
                    }
                    self.addToBasket(product.data('product-id'));
                });
            },

            addToBasket : function(productId) {
                var self = this;
                var quantity = 1;
                var btn = self.entity('cart-btn', '[data-product-id="' + productId + '"]');

                $.post(
                    this.actionsUrl,
                    {
                        action : 'addToBasket',
                        id: productId,
                        quantity: quantity
                    },
                    function (data) {
                        if (data.successful) {
                            BX.onCustomEvent('OnBasketChange');

                            if (!btn) {
                                return;
                            }

                            btn.text('Добавлено');
                            btn.addClass('cart-btn-added');
                            setTimeout(function () {
                                btn.text('Добавить в заказ');
                                btn.removeClass('cart-btn-added');
                            }, 1000);
                        }
                    },
                    'json'
                );
            },

        };

    })();

    new JCIrsapProductSliderComponent(<?=CUtil::PhpToJSObject($jsParams, false, true);?>);
</script>