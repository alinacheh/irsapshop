<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 */

$this->setFrameMode(true);

$elementEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT');
$elementDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE');
$elementDeleteParams = array('CONFIRM' => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));

if (empty($arResult['ITEMS'])) {
    return;
}?>
<div class="menu-catalog__title"><a href="/catalog"><?=$arParams['BLOCK_TITLE'];?></a></div>
<ul class="menu-catalog">
    <?foreach ($arResult['ITEMS'] as $item):
        $uniqueId = $item['ID'].'_'.md5($this->randString().$component->getAction());
        $this->AddEditAction($uniqueId, $item['EDIT_LINK'], $elementEdit);
        $this->AddDeleteAction($uniqueId, $item['DELETE_LINK'], $elementDelete, $elementDeleteParams);
    ?>
        <li class="menu-catalog__item">
            <a href="<?=$item['DETAIL_PAGE_URL'];?>" class="menu-catalog__link"><?=$item['NAME'];?></a>
            <span class="menu-catalog__span">от <?=$item['MIN_PRICE']['PRINT_DISCOUNT_VALUE'];?></span>
        </li>
    <?endforeach;?>
</ul>