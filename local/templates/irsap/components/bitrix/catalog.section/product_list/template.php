<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 */

$this->setFrameMode(true);

$elementEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT');
$elementDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE');
$elementDeleteParams = array('CONFIRM' => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));

?>
<div id="catalog-list" class="product-list__gallery">
    <div class="container">
		<?if (empty($arResult['ITEMS'])):?>
			<p>Подходящей коллекции не найдено. Попробуйте изменить параметры поиска</p>
		<?else:?>
			<div class="gallery">
				<?foreach ($arResult['ITEMS'] as $item):
					$uniqueId = $item['ID'].'_'.md5($this->randString().$component->getAction());
					$this->AddEditAction($uniqueId, $item['EDIT_LINK'], $elementEdit);
					$this->AddDeleteAction($uniqueId, $item['DELETE_LINK'], $elementDelete, $elementDeleteParams);
				?>
				<a href="<?=$item['DETAIL_PAGE_URL'];?>" class="gallery-flex">
					<img 
						data-src="<?=$item['PREVIEW_PICTURE']['SRC'];?>"
						alt="<?=$item['PREVIEW_PICTURE']['ALT'];?>"
						title="<?=$item['PREVIEW_PICTURE']['TITLE'];?>"
						class="gallery-photo__item lazy"
					>
					<?if (!empty($item['PROPERTIES']['LABEL']) && !empty($item['PROPERTIES']['LABEL']['VALUE'])):?>
						<span class="label-product"><?=$item['PROPERTIES']['LABEL']['VALUE'];?></span>
					<?endif;?>
					<div class="galley-label__box">
						<div class="gallery-img__label"><?=$item['NAME'];?></div>
						<div class="gallery-img__price">от&nbsp;<?=$item['MIN_PRICE']['PRINT_DISCOUNT_VALUE'];?></div>
					</div>
				</a>
				<?endforeach;?>
			</div>
		<?endif;?>
    </div>
</div>