<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 */

$this->setFrameMode(true);

$elementEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT');
$elementDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE');
$elementDeleteParams = array('CONFIRM' => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));

if (empty($arResult['ITEMS'])) {
    return;
}?>
<div class="container">
    <h2 class="section-product">
        <span><?=$arParams['BLOCK_TITLE'];?></span>
    </h2>
    <div class="slider-product">
        <?foreach ($arResult['ITEMS'] as $item):
            $model = $arResult['MODELS'][$item['MODEL_ID']];
            $nameParts = [
                $model['NAME'],
                $item['PROPERTIES']['TIP_PODCKLUCHENIA']['VALUE'],
                $item['PROPERTIES']['VISOTA']['VALUE'],
                $item['PROPERTIES']['KOLICHESTVO_SEKCIY']['VALUE'],
            ];
            ?>
            <div class="slider-product__item">
                <div class="slider-product__top">
                    <?if (!empty($item['LABEL'])):?>
                        <div class="label-product hidden-des"><?=$item['LABEL'];?></div>
                    <?endif;?>
                    <?if ($model['PREVIEW_PICTURE']):?>
                        <img
                            src="<?=CFile::GetPath($model['PREVIEW_PICTURE']);?>"
                            alt="<?=$model['PROPERTIES']['PREVIEW_PICTURE_ALT']['VALUE'];?>"
                            title="<?=$model['PROPERTIES']['PREVIEW_PICTURE_TITLE']['VALUE'];?>"
                        >
                    <?endif;?>
                </div>
                <div class="slider-product__bottom">
                    <a href="<?=$model['DETAIL_PAGE_URL'];?>?OFFER_ID=<?=$item['ID'];?>" class="slider-product__name"><?=implode(' | ', $nameParts);?></a>
                    <div class="slider-product__desc"><?=$item['PREVIEW_TEXT'];?></div>
                    <div class="price-buy">
                        <div class="slider-product__price">
                            <div class="label_price">Цена от:</div>
                            <?
                            $price = round($item['MIN_PRICE']['DISCOUNT_VALUE'], 2);
                            $currency = AppMainHelper::getPublicCurrency($item['MIN_PRICE']['CURRENCY']);
                            ?>
                            <span class="price"><?=$price;?>&nbsp;<span class="hidden-des"><?=$currency;?></span></span>
                        </div>
                        <div class="slider-product__buy">
                            <a href="<?=$model['DETAIL_PAGE_URL'];?>?OFFER_ID=<?=$item['ID'];?>" class="style_btn3">Размеры и цвета</a>
                            <?if (!empty($item['LABEL'])):?>
							<span class="label-product hidden-mob"><span><?=$item['LABEL'];?></span></span>
                            <?endif;?>
                        </div>
                    </div>
                </div>
            </div>
        <?endforeach;?>
    </div>
</div>