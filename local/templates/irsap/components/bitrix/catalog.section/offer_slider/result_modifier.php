<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogSectionComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

/** Irsap Customs */

$modelIds = [];

foreach ($arResult['ITEMS'] as &$item) {

    if (!empty($item['PREVIEW_PICTURE']) && !empty($item['IPROPERTY_VALUES'])) {
        if (!empty($item['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_ALT'])) {
            $item['PREVIEW_PICTURE']['ALT'] = $item['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_ALT'];
        }
        if (!empty($item['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'])) {
            $item['PREVIEW_PICTURE']['TITLE'] = $item['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'];
        }
    }

    $item['LABEL'] = null;
    if ($arParams['LABEL_XML_ID']) {
        foreach ($item['PROPERTIES']['LABEL']['VALUE_XML_ID'] as $index => $xmlId) {
            if ($xmlId !== $arParams['LABEL_XML_ID']) {
                continue;
            }
            $item['LABEL'] = $item['PROPERTIES']['LABEL']['VALUE'][$index];
        }
    }

    $modelIds[] = $item['PROPERTIES']['CML2_LINK']['VALUE'];
    $item['MODEL_ID'] = (int) $item['PROPERTIES']['CML2_LINK']['VALUE'];
}
unset($item);

/** Collect models */
$arResult['MODELS'] = [];
$modelIds = array_unique($modelIds);
$dbModels = CIBlockElement::GetList(
    ['ID' => 'ASC'],
    ['ID' => $modelIds, 'IBLOCK_ID' => CATALOG_MODELS_ID],
    false,
    false,
    ['ID', 'CODE', 'IBLOCK_ID', 'NAME', 'PREVIEW_PICTURE', 'DETAIL_PAGE_URL']
);
while ($obModel = $dbModels->GetNextElement()) {
    $model = $obModel->GetFields();
    $model['PROPERTIES'] = $obModel->GetProperties();
    $arResult['MODELS'][$model['ID']] = $model;
}