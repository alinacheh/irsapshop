<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

?>
<div>
    <?if ($arResult["FORM_NOTE"] != ""):?>
        <div><?=$arParams["SUCCESS_TEXT"] ?: $arResult["FORM_NOTE"]?></div>
    <?elseif ($arResult["isFormNote"] != "Y"):?>
        <?if ($arResult["isFormErrors"] == "Y"):?><div style="margin-bottom: 20px"><?=$arResult["FORM_ERRORS_TEXT"];?></div><?endif;?>
        <?=$arResult["FORM_HEADER"];?>
            <div class="help-form">
                <?foreach($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion):
                if (!in_array($arQuestion['STRUCTURE'][0]['FIELD_TYPE'], ['text', 'email', 'password', 'hidden', 'textarea'])) {
                    continue;
                }?>
                    <?=$arQuestion["HTML_CODE"]?>
                <?endforeach;?>
            </div>
            <div class="help-middle">
                <?if ($arResult["isUseCaptcha"] == "Y"):?>
                    <div class="form-column__captcha">
                        <img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" alt="" class="captcha" style="display: block;">
                        <input type="hidden" name="captcha_sid" value="<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" />
                        <input type="text" name="captcha_word">
                    </div>
                <?endif;?>
                <div class="help-middle__btn-send flex_center">
                    <input class="btn-send" type="submit" name="web_form_submit" value="<?=htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]);?>">
                </div>
            </div>
        <?=$arResult["FORM_FOOTER"];?>
        <div class="help-bottom">
            Нажимая кнопку "Отправить" Вы соглашаетесь на <a href="/policy/" target="_blank">обработку персональных данных</a>
        </div>
    <?endif;?>
</div>

<script>
    $(document).ready(function () {
        Recaptchafree.reset();
        $('input[data-field-name="PHONE"]').mask('+7 (999) 999-99-99');
    });
</script>