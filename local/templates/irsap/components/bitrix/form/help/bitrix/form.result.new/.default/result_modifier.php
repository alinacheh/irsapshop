<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
    if (!empty($arParams["~TARGET_URL"]))
    {
        $targetUrl = $arParams["~TARGET_URL"];
        foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion)
        {
            if ($FIELD_SID == "TARGET_URL")
            {
                $arAnswer = array("ID"=>$arQuestion["STRUCTURE"][0]["ID"], "VALUE"=>htmlspecialcharsBack($targetUrl));
                $value = CForm::GetHiddenValue($arQuestion["STRUCTURE"][0]["ID"], $arAnswer, $arResult["arrVALUES"]);
                $res = CForm::GetHiddenField($arQuestion["STRUCTURE"][0]["ID"], $value);
                $arResult["QUESTIONS"][$FIELD_SID]["HTML_CODE"] = $res;
            }
        }
    }

    foreach ($arResult['QUESTIONS'] as $fname => &$arQuestion)
	{
		$strRequired = $arQuestion['REQUIRED'] == 'Y' ? 'required' : '';
		$arStructure = $arQuestion['STRUCTURE'];
		$fType = $arStructure[0]['FIELD_TYPE'];
		$params = $arStructure[0]['FIELD_PARAM'];
		
		switch($fType)
		{
			case 'text' : 
			case 'email' : 
			case 'password' : 
				
				$name = 'form_'.$fType.'_'.$arStructure[0]['ID'];
				$arQuestion['HTML_CODE'] = '
				    <div class="help-form__line">
				        <label for="'.$name.'" class="help-form__label">'.$arQuestion['CAPTION'].'<span>*</span></label>
                        <input 
                            type="text" 
                            id="'.$name.'" 
                            name="'.$name.'" 
                            class="input_style" 
                            data-field-name="'.$fname.'" 
                            value="'.$arResult['arrVALUES'][$name].'" '.$strRequired.' '.$params.'
                        >
                    </div>
                ';
				break;
		}
		
	}
?>