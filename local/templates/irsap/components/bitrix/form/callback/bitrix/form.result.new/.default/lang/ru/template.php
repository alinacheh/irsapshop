<?
$MESS ['FORM_REQUIRED_FIELDS'] = "Поля, обязательные для заполнения";
$MESS ['FORM_APPLY'] = "Применить";
$MESS ['FORM_ADD'] = "Добавить";
$MESS ['FORM_ACCESS_DENIED'] = "Не хватает прав доступа к веб-форме.";
$MESS ['FORM_DATA_SAVED1'] = "Спасибо!<br><br>Ваша заявка";
$MESS ['FORM_DATA_SAVED2'] = " принята к рассмотрению.";
$MESS ['FORM_MODULE_NOT_INSTALLED'] = "Модуль веб-форм не установлен.";
$MESS ['FORM_NOT_FOUND'] = "Веб-форма не найдена.";
$MESS ['FORM_CAPTION'] = "Отправить сообщение";
$MESS ['FORM_CUSTOM_TITLE'] = "Для заказа бесплатного изготовления проектного решения, заполните прилагаемую ниже форму:";
?>
