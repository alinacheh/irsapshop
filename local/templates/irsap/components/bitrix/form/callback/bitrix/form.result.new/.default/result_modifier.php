<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
    foreach ($arResult['QUESTIONS'] as $fname => &$arQuestion)
	{
		$strRequired = $arQuestion['REQUIRED'] == 'Y' ? 'required' : '';
		$arStructure = $arQuestion['STRUCTURE'];
		$fType = $arStructure[0]['FIELD_TYPE'];
		$params = $arStructure[0]['FIELD_PARAM'];
		
		switch($fType)
		{
			case 'text' : 
			case 'email' : 
			case 'password' : 
				
				$name = 'form_'.$fType.'_'.$arStructure[0]['ID'];
				$arQuestion['HTML_CODE'] = '
                    <input 
                        type="text" 
                        id="'.$name.'" 
                        class="column-data" 
                        name="'.$name.'" 
                        data-field-name="'.$fname.'" 
                        value="'.$arResult['arrVALUES'][$name].'" '.$strRequired.' '.$params.'
                    >
                ';
				break;

            case 'textarea' :

                $name = 'form_'.$fType.'_'.$arStructure[0]['ID'];
                $arQuestion['HTML_CODE'] = '
                    <textarea 
                        id="'.$name.'" 
                        class="form-textarea" 
                        name="'.$name.'" 
                        data-field-name="'.$fname.'" '.$strRequired.' '.$params.'
                    >'.$arResult['arrVALUES'][$name].'</textarea>
                ';
                break;
		}
		
	}
?>