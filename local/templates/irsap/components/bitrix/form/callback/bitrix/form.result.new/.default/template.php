<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

?>
<?if ($arResult["FORM_NOTE"] != ""):?>
    <div><?=$arParams["SUCCESS_TEXT"] ?: $arResult["FORM_NOTE"]?></div>
<?elseif ($arResult["isFormNote"] != "Y"):?>
<div class="form-wrap form-contact">
    <h2 class="contact-requisite__title"><?=$arResult['FORM_TITLE'];?></h2>
    <?if ($arResult["isFormErrors"] == "Y"):?><div style="margin-bottom: 20px"><?=$arResult["FORM_ERRORS_TEXT"];?></div><?endif;?>
    <?=$arResult["FORM_HEADER"];?>
        <?foreach($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion):
            if (!in_array($arQuestion['STRUCTURE'][0]['FIELD_TYPE'], ['text', 'email', 'password', 'hidden', 'textarea'])) {
                continue;
            }
            switch ($arQuestion['STRUCTURE'][0]['FIELD_TYPE']) {
                case 'text':
                case 'email':
                case 'password':
                    ?>
                    <div class="form-column__wrap">
                        <div class="form-column">
                            <label for="form-name" class="form-column__label"><?=$arQuestion['CAPTION'];?></label>
                            <?=$arQuestion["HTML_CODE"]?>
                        </div>
                    </div>
                    <?
                    break;
                case 'textarea':
                    ?>
                    <div class="form-column column-textarea">
                        <label for="form-mess" class="form-column__label"><?=$arQuestion['CAPTION'];?></label>
                        <?=$arQuestion["HTML_CODE"]?>
                    </div>
                    <?
                    break;
            }
        endforeach;?>
        <div class="form-column__btn-box">
            <div class="empty"></div>
            <div class="form-column-btn">
                <?if ($arResult["isUseCaptcha"] == "Y"):?>
                    <div class="form-column__captcha">
                        <img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" class="captcha" style="display: block;" alt="" />
                        <input type="hidden" name="captcha_sid" value="<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" />
                        <input type="text" name="captcha_word">
                    </div>
                <?endif;?>
                <div class="form-column__btn-send flex_center">
                    <input class="btn-send" type="submit" name="web_form_submit" value="<?=htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]);?>">
                </div>
            </div>
        </div>
        <div class="form-personal">
            <p class="form-personal__text">Нажимая кнопку "Отправить" Вы соглашаетесь на
                <a href="/policy/" target="_blank" class="form-personal__"><span>обработку персональных данных</span></a>
            </p>
        </div>
    <?=$arResult["FORM_FOOTER"];?>
</div>
<?endif;?>

<script>
    $(document).ready(function () {
        Recaptchafree.reset();
        $('input[data-field-name="PHONE"]').mask('+7 (999) 999-99-99');
    });
</script>