<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if (!empty($arResult)):?>
    <ul class="menu-info">
        <?foreach ($arResult as $arItem):?>
            <li class="menu-info__item"><a href="<?= $arItem['LINK']; ?>" class="menu-info__link"><?= $arItem['TEXT']; ?></a></li>
        <?endforeach;?>
    </ul>
<?endif;?>