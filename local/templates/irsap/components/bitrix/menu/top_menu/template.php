<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if (!empty($arResult)):?>
    <ul class="main-menu">
        <?foreach ($arResult as $arItem):?>
            <li><a href="<?= $arItem['LINK']; ?>"><?= $arItem['TEXT']; ?></a></li>
        <?endforeach;?>
    </ul>
<?endif;?>