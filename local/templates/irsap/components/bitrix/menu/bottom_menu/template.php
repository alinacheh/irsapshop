<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if (!empty($arResult)):?>
    <div class="bottom-footer__menu flex_end_start">
        <?foreach ($arResult as $arItem):?>
            <a href="<?= $arItem['LINK']; ?>" class="footer-menu__link"><?= $arItem['TEXT']; ?></a>
        <?endforeach;?>
    </div>
<?endif;?>