function JCSmartFilter(ajaxURL)
{
	this.ajaxURL = ajaxURL;
	this.resultAjaxURL = ajaxURL;
	this.resetAjaxURL = ajaxURL;
	this.searchAjaxURL = '/catalog/search.php';
	this.form = null;
	this.timer = null;
	this.searchTimer = null;
	this.cacheKey = '';
	this.cache = [];
	this.initHtml = $('#filter-props-wrapper').html()
	this.filterReseted = true;
}

JCSmartFilter.prototype.keyup = function(input)
{
	if(!!this.timer)
	{
		clearTimeout(this.timer);
	}
	this.timer = setTimeout(BX.delegate(function(){
		this.reload(input);
	}, this), 500);
};

JCSmartFilter.prototype.click = function(checkbox)
{
	if(!!this.timer)
	{
		clearTimeout(this.timer);
	}

	this.timer = setTimeout(BX.delegate(function(){
		this.reload(checkbox);
	}, this), 500);
};

JCSmartFilter.prototype.reload = function(input)
{
	BX('products-search-input').value = '';

	if (this.cacheKey !== '')
	{
		//Postprone backend query
		if(!!this.timer)
		{
			clearTimeout(this.timer);
		}
		this.timer = setTimeout(BX.delegate(function(){
			this.reload(input);
		}, this), 1000);
		return;
	}
	this.cacheKey = '|';

	this.position = BX.pos(input, true);
	this.form = BX.findParent(input, {'tag':'form'});
	if (this.form)
	{
		var values = [];
		values[0] = {name: 'ajax', value: 'y'};
		this.gatherInputsValues(values, BX.findChildren(this.form, {'tag': new RegExp('^(input|select)$', 'i')}, true));

		for (var i = 0; i < values.length; i++)
			this.cacheKey += values[i].name + ':' + values[i].value + '|';

		if (this.cache[this.cacheKey])
		{
			this.curFilterinput = input;
			this.postHandler(this.cache[this.cacheKey], true);
		}
		else
		{
			this.curFilterinput = input;
			BX.ajax.loadJSON(
				this.ajaxURL,
				this.values2post(values),
				BX.delegate(this.postHandler, this)
			);
		}
	}
};

JCSmartFilter.prototype.updateItem = function (PID, arItem)
{
	if (arItem.VALUES)
	{
		for (var i in arItem.VALUES)
		{
			if (arItem.VALUES.hasOwnProperty(i))
			{
				var value = arItem.VALUES[i];
				var control = BX(value.CONTROL_ID);

				if (!!control)
				{
					var label = document.querySelector('[data-role="label_'+value.CONTROL_ID+'"]');
					if (value.DISABLED)
					{
						if (label)
							BX.addClass(label, 'disabled');
						else
							BX.addClass(control.parentNode, 'disabled');
					}
					else
					{
						if (label)
							BX.removeClass(label, 'disabled');
						else
							BX.removeClass(control.parentNode, 'disabled');
					}

					if (value.hasOwnProperty('ELEMENT_COUNT'))
					{
						label = document.querySelector('[data-role="count_'+value.CONTROL_ID+'"]');
						if (label)
							label.innerHTML = value.ELEMENT_COUNT;
					}
				}
			}
		}
	}
};

JCSmartFilter.prototype.postHandler = function (result, fromCache)
{
	var url, curProp;

	if (!!result && !!result.ITEMS)
	{
		for(var PID in result.ITEMS)
		{
			if (result.ITEMS.hasOwnProperty(PID))
			{
				this.updateItem(PID, result.ITEMS[PID]);
			}
		}

		if (result.FILTER_AJAX_URL)
		{
			this.resultAjaxURL = BX.util.htmlspecialcharsback(result.FILTER_AJAX_URL);
		}

		if (result.SEF_DEL_FILTER_URL)
		{
			this.resetAjaxURL = BX.util.htmlspecialcharsback(result.SEF_DEL_FILTER_URL);
		}
	}

	if (!fromCache && this.cacheKey !== '')
	{
		this.cache[this.cacheKey] = result;
	}
	this.cacheKey = '';

	if (result.FILTER_AJAX_URL)
	{
		this.filterReseted = false;
		this.applyFilter();
	}
};

JCSmartFilter.prototype.applyFilter = function ()
{
	var self = this;
	this.switchLoading(true);
	$.get(
		BX.util.htmlspecialcharsback(this.resultAjaxURL),
		function (data) {
			self.switchLoading(false);
			$('#catalog-list').html($(data).find('#catalog-list').html());
			console.log("applyFilter");
			$('.lazy').Lazy({
				scrollDirection: 'vertical',
				effect: 'fadeIn',
				visibleOnly: true
			});
		}
	);
}

JCSmartFilter.prototype.resetFilter = function ()
{
	this.resultAjaxURL = this.resetAjaxURL;
	var self = this;
	this.switchLoading(true);
	$.get(
		BX.util.htmlspecialcharsback(this.resetAjaxURL),
		function (data) {
			this.filterReseted = true;
			self.switchLoading(false);
			$('#filter-props-wrapper').html(self.initHtml);
			$('#catalog-list').html($(data).find('#catalog-list').html());
			console.log("resetFilter");
			$('.lazy').Lazy({
				scrollDirection: 'vertical',
				effect: 'fadeIn',
				visibleOnly: true
			});
		}
	);
}

JCSmartFilter.prototype.gatherInputsValues = function (values, elements)
{
	if(elements)
	{
		for(var i = 0; i < elements.length; i++)
		{
			var el = elements[i];
			if (el.disabled || !el.type)
				continue;

			switch(el.type.toLowerCase())
			{
				case 'text':
				case 'textarea':
				case 'password':
				case 'hidden':
				case 'select-one':
					if(el.value.length)
						values[values.length] = {name : el.name, value : el.value};
					break;
				case 'radio':
				case 'checkbox':
					if(el.checked)
						values[values.length] = {name : el.name, value : el.value};
					break;
				case 'select-multiple':
					for (var j = 0; j < el.options.length; j++)
					{
						if (el.options[j].selected)
							values[values.length] = {name : el.name, value : el.options[j].value};
					}
					break;
				default:
					break;
			}
		}
	}
};

JCSmartFilter.prototype.values2post = function (values)
{
	var post = [];
	var current = post;
	var i = 0;

	while(i < values.length)
	{
		var p = values[i].name.indexOf('[');
		if(p == -1)
		{
			current[values[i].name] = values[i].value;
			current = post;
			i++;
		}
		else
		{
			var name = values[i].name.substring(0, p);
			var rest = values[i].name.substring(p+1);
			if(!current[name])
				current[name] = [];

			var pp = rest.indexOf(']');
			if(pp == -1)
			{
				//Error - not balanced brackets
				current = post;
				i++;
			}
			else if(pp == 0)
			{
				//No index specified - so take the next integer
				current = current[name];
				values[i].name = '' + current.length;
			}
			else
			{
				//Now index name becomes and name and we go deeper into the array
				current = current[name];
				values[i].name = rest.substring(0, pp) + rest.substring(pp+1);
			}
		}
	}
	return post;
};

JCSmartFilter.prototype.expandPropValues = function(element)
{
	var obj = element.parentNode;
	var labels = BX.findChild(obj, {tag: "label"}, true, true);

	for (var i = 0; i < labels.length; i++) {
		labels[i].style.display = 'flex';
	}

	BX(element).style.display = 'none';
}

JCSmartFilter.prototype.search = function(element)
{
	if (!!this.searchTimer) {
		clearTimeout(this.searchTimer);
	}
	this.searchTimer = setTimeout(BX.delegate(function(){
		this.searchRequest(element.value);
	}, this), 1000);
	return;
}

JCSmartFilter.prototype.searchRequest = function(value)
{
	var self = this;
	this.switchLoading(true);
	$.get(
		BX.util.htmlspecialcharsback(this.searchAjaxURL + '?q=' + value),
		function (data) {
			self.switchLoading(false);
			$('#filter-props-wrapper').html(self.initHtml);
			var content = $(data).find('#catalog-list').html();
			if (content) {
				$('#catalog-list').html(content);
			} else {
				$('#catalog-list').html('');
			}
			console.log("searchRequest");
			$('.lazy').Lazy({
				scrollDirection: 'vertical',
				effect: 'fadeIn',
				visibleOnly: true
			});
		}
	);
}

JCSmartFilter.prototype.switchLoading = function(loading)
{
	if (loading) {
		$('#filter-wrapper').addClass('overlay-loading');
	} else {
		$('#filter-wrapper').removeClass('overlay-loading');
	}
}