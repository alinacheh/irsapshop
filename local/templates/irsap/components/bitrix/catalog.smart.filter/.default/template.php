<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

?>
<div id="filter-wrapper" class="filter-search">
    <div class="filter-box">
        <form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get">
            <?foreach ($arResult["HIDDEN"] as $arItem):?>
                <input type="hidden" name="<?echo $arItem["CONTROL_NAME"]?>" id="<?echo $arItem["CONTROL_ID"]?>" value="<?echo $arItem["HTML_VALUE"]?>" />
            <?endforeach;?>
            <div id="filter-props-wrapper">
                <?foreach ($arResult["ITEMS"] as $key => $arItem):
                    if (empty($arItem["VALUES"]) || isset($arItem["PRICE"])) {
                        continue;
                    }?>
                    <div class="filter-collection">
                        <div class="filter-collection__title"><?=$arItem["NAME"]?></div>
                        <?
                        $arCur = current($arItem["VALUES"]);
                        switch ($arItem["DISPLAY_TYPE"]) {
                            case "P"://DROPDOWN
                            default://CHECKBOXES
                                ?>
                                <div data-block-id="<?=$arCur["CONTROL_ID"]?>" class="filter-collection__radio flex_center_start">
                                    <?foreach ($arItem["VALUES"] as $val => $ar):?>
                                        <label for="<? echo $ar["CONTROL_ID"] ?>">
                                            <input
                                                    type="checkbox"
                                                    class="filter-radio"
                                                    onclick="smartFilter.click(this)"
                                                    id="<? echo $ar["CONTROL_ID"] ?>"
                                                    name="<? echo $ar["CONTROL_NAME"] ?>"
                                                    value="<? echo $ar["HTML_VALUE"] ?>"
                                                <? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
                                            >
                                            <span class="bold"><?=$ar["VALUE"];?></span>
                                        </label>
                                    <?endforeach;?>
                                </div>
                                <? break;
                        }?>
                    </div>
                <?endforeach;?>
            </div>
            <div class="filter-collection__btn">
                <a href="javascript:void(0)" onclick="smartFilter.resetFilter()" class="filter-collection__link reset">сбросить фильтр</a>
                <a href="javascript:void(0)" onclick="smartFilter.applyFilter()" class="filter-collection__link submit">показать все</a>
            </div>
        </form>
    </div>
    <div class="search">
        <div class="filter-function__search">
            <input id="products-search-input" oninput="smartFilter.search(this)" type="text" placeholder="Поиск по названию модели...">
            <a href="#" class="search__icon"><i class="fa fa-search" aria-hidden="true"></i></a>
        </div>
    </div>
</div>
<script>
	var smartFilter = new JCSmartFilter('<?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>');
</script>