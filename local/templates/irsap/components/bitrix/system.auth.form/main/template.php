<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
?>

<div class="form-cmn flex_stretch_between pb">
    <h2><? echo Loc::getMessage("auth_form_comp_auth") ?></h2>
</div>
<?
if (!empty($_POST["AUTH_FORM"]) && $arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR']):?>
    <div class="form-cmn flex_stretch_between">
        <? ShowMessage($arResult['ERROR_MESSAGE']); ?>
    </div>
<?php endif;?>
<div class="form-cmn flex_stretch_between">
    <div class="flex50">
        <form name="system_auth_form<?=$arResult["RND"]?>" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
            <?if($arResult["BACKURL"] <> ''):?>
                <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
            <?endif?>
            <?foreach ($arResult["POST"] as $key => $value):?>
                <input type="hidden" name="<?=$key?>" value="<?=$value?>" />
            <?endforeach?>
            <input type="hidden" name="AUTH_FORM" value="Y" />
            <input type="hidden" name="TYPE" value="AUTH" />

            <div class="form-cmn-row flex_center_start">
                <label for="" class="form-cmn-label"><?=Loc::getMessage('AUTH_LOGIN')?></label>
                <input type="text" name="USER_LOGIN" class="input_style" id="authEmail" value="<?=$arResult["USER_LOGIN"];?>">
                <script>
                    BX.ready(function() {
                        var loginCookie = BX.getCookie("<?=CUtil::JSEscape($arResult["~LOGIN_COOKIE_NAME"])?>");
                        if (loginCookie) {
                            var form = document.forms["system_auth_form<?=$arResult["RND"]?>"];
                            var loginInput = form.elements["USER_LOGIN"];
                            loginInput.value = loginCookie;
                        }
                    });
                </script>
            </div>
            <div class="form-cmn-row flex_center_start">
                <label for="" class="form-cmn-label"><?=Loc::getMessage('AUTH_PASSWORD')?></label>
                <input type="password" name="USER_PASSWORD" class="input_style" value="<?=$arResult["USER_PASSWORD"];?>" autocomplete="off">
            </div>
            <div class="form-cmn-btn flex_center_start">
                <button class="style_btn3" type="submit" name="Login"><?=GetMessage("AUTH_LOGIN_BUTTON")?></button>
            </div>
        </form>
    </div>
</div>