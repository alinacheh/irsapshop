// JCIrsapProductComponent - компонент для обработки логики товара

(function() {
    'use strict';

    if (!!window.JCIrsapProductComponent)
        return;

    if (!window.BX)
        return;

    window.JCIrsapProductComponent = function(params) {
        this.actionsUrl = '/local/templates/irsap/ajax/actions.php';
        this.blockSelector = params.BLOCK_SELECTOR;
        this.constants = params.CONSTANTS;
        this.offerId = params.CURRENT_OFFER.ID;
        this.currentOffer = params.CURRENT_OFFER;
        this.offers = params.OFFERS;
        this.colorId = params.COLOR_ID;
        this.colors = params.COLORS;
        this.currentFilter = params.CURRENT_FILTER;
        this.filterProperties = params.FILTER_PROPERTIES;
        this.initUI();
        this.initHandlers();
    };

    window.JCIrsapProductComponent.prototype = {

        entity : function(entity, selector) {
            if (!entity) {
                return null;
            }
            return $(selector || this.blockSelector).find('[data-entity="' + entity + '"]');
        },

        initHandlers : function() {
            var self = this;

            $(this.blockSelector).on('click', '[data-entity="cart-btn"]', function(e) {
                e.preventDefault();
                e.stopPropagation();
                self.addToBasket();
            });

            $(this.blockSelector).on('click', '[data-entity="open-color-window-btn"]', function(e) {
                e.preventDefault();
                $("#ps__id").show();
                $(".overlay").show();
            });

            $(this.blockSelector).on('click', '[data-entity="color-window-close"]', function(e) {
                e.preventDefault();
                $("#ps__id").hide();
                $(".overlay").hide();
            });
        },

        initUI: function() {
            var self = this;

            $(".range-filter").each(function(){

                let val = $(this).attr("data-val") * 1;
                let min = $(this).attr("data-min") * 1;
                let max = $(this).attr("data-max") * 1;
                
                let propCode = $(this).data("prop-code");
                let values = self.filterProperties[propCode].VALUES.map(function(item) {
                    return +item;
                });
                let valIndex = values.findIndex(function(item) {
                    return item === val;
                });

                let rangeblock = $(this).parents(".range-block");

                rangeblock.find(".range__start").text(min);
                rangeblock.find(".range__finish").text(max);


			
			
                noUiSlider.create(this, {
                    start: [valIndex],
                    step: 1,
                    tooltips: true,
                    format: {
                        from: function(value) {
                            return parseInt(value);
                        },
                        to: function(value) {
                            return parseInt(values[value]);
                        }
                    },
					 //range: {
					//	 min: [0],
					//	'20%': 400,
					//	'50%': 600,	
					//	 max: [values.length - 1]
					//}
	
                    range: {
                        min: [0],
						//'50%': [values.length - 5],	
                        max: [values.length - 1]
                    }
                });
				
				console.log('values11');
				console.log(values[10]);
				
				console.log('values');
			
				console.log(values);

                //this.noUiSlider.on('update', function (values) {
                this.noUiSlider.on('update', function (values, handle, unencoded) {
						
				console.log('values - '+values);
				console.log('valueshandle');
				console.log(values[handle]);
				
				console.log('handle');
				console.log(handle);
				
				console.log('unencoded');
				console.log();
				console.log(values[parseInt(unencoded)]);

                   self.changeFilterValue(propCode, values[0]);
                });

            });


            $(function() {
                $(".tabs-info" ).tabs();
            });

            $(".filter-radio").change(function(){
                var checked = $(this).is(':checked');
                $(".filter-radio").parent().removeClass("bold");
                $(this).parent().addClass("bold");
            })
        },

        addToBasket : function() {
            var self = this;
            var btn = self.entity('cart-btn');

            var quantity = parseInt(this.entity('item-quantity').val());
            if (!quantity) {
                return;
            }

            $.post(
                this.actionsUrl,
                {
                    action : 'addToBasket',
                    id: this.offerId,
                    quantity: quantity,
                    colorId: this.colorId
                },
                function (data) {
                    if (data.successful) {
                        BX.onCustomEvent('OnBasketChange');
                        if (!btn) {
                            return;
                        }
                        btn.text('Добавлено');
                        btn.addClass('cart-btn-added');
                        setTimeout(function () {
                            btn.text('Добавить в заказ');
                            btn.removeClass('cart-btn-added');
                        }, 1000);
                    }
                },
                'json'
            );
        },

        changeFilterValue : function(code, value) {
			
			console.log('changeFilterValue');
			console.log(code);
			console.log(value);
			
            var self = this;
            this.currentFilter[code] = value;

            var offer = this.offers.find(function (offer) {
                for (var prop in self.currentFilter) {
                    if (offer.PROPERTIES[prop].VALUE != self.currentFilter[prop]) {
                        return false;
                    }
                }
                return true;
            });
			console.log('currentFilter');
			//console.log(currentFilter);
			console.log('------------------------------');

            if (!offer) {
                this.entity('current-offer-block').hide();
                this.entity('no-offer-block').show();
                return;
            } else {
                this.entity('no-offer-block').hide();
                this.entity('current-offer-block').show();
            }

            this.offerId = offer.ID;
            this.currentOffer = offer;

            this.redrawCurrentOffer();
        },

        redrawCurrentOffer : function() {
            var currentOfferBlock = this.entity('current-offer-block');

            this.entity('artnumber', currentOfferBlock).text(this.currentOffer.PROPERTIES.ARTNUMBER.VALUE);

            /** Replace price */
            this.updatePrice();

            /** Replace stock */
            this.entity('in-stock').hide();
            this.entity('out-stock').hide();

            var stock = this.currentOffer.PROPERTIES.STOCK.VALUE_XML_ID;
            if (stock === this.constants['IN_STOCK']) {
                this.entity('in-stock').show();
            } else {
                this.entity('out-stock').show();
            }

            /** Replace properties */
            var propertiesBlock = this.entity('values-block');
            propertiesBlock.html('');

            for (var code in this.filterProperties) {
                if (!this.currentOffer.PROPERTIES[code] || !this.currentOffer.PROPERTIES[code].VALUE) {
                    continue;
                }
                var isLast = Object.keys(this.filterProperties).findIndex(function(entry) {
                    return entry === code;
                }) === (Object.keys(this.filterProperties).length - 1);
                var suffix = !isLast ? ', ' : '';
                propertiesBlock.append('<span class="value-name">' + this.currentOffer.PROPERTIES[code].NAME + ': </span>');
                propertiesBlock.append('<span class="value-text">' + this.currentOffer.PROPERTIES[code].VALUE + suffix + '</span>');
            }

            /** Replace labels */
            var labelsBlock = this.entity('labels-block');
            labelsBlock.html('');
            if (this.currentOffer.PROPERTIES.LABEL && this.currentOffer.PROPERTIES.LABEL.VALUE) {
                this.currentOffer.PROPERTIES.LABEL.VALUE.forEach(function(value) {
                    labelsBlock.append('<div class="total__tags">' + value + '</div>');
                });
            }

        },

        changeColor : function(colorId) {
            var previousColorId = this.colorId;
            this.colorId = parseInt(colorId);
            var color = this.colors[this.colorId];

            var previousColorBtn = this.entity('color-window').find('[data-color-id="' + previousColorId + '"]');
            previousColorBtn.removeClass('active');
            previousColorBtn.closest('[data-entity="color-group-block"]').find('[data-entity="selected-color-description"]').html('');

            var selectedColorBtn = this.entity('color-window').find('[data-color-id="' + this.colorId + '"]');
            selectedColorBtn.addClass('active');
            selectedColorBtn.closest('[data-entity="color-group-block"]').find('[data-entity="selected-color-description"]')
                .html('<div class="ps-top__text">Выбрано: ' + color.UF_LIST_NAME + '  |  RAL: ' + color.UF_RAL + '  |  Код: ' + color.UF_CODE + '  |  Серия: ' + color.UF_SERIES + '  |  Покрытие: ' + color.UF_COATING + '</div>');

            this.entity('current-color-name').text(color.UF_LIST_NAME);
            this.entity('current-color-btn').css('background-image', 'url(' + color.UF_FILE + ')');

            this.entity('color-name', this.entity('current-offer-block')).text(color.UF_LIST_NAME);
            this.entity('color-ral', this.entity('current-offer-block')).text(color.UF_RAL);
            this.entity('color-code', this.entity('current-offer-block')).text(color.UF_CODE);
            this.entity('color-series', this.entity('current-offer-block')).text(color.UF_SERIES);
            this.entity('color-coating', this.entity('current-offer-block')).text(color.UF_COATING);

            this.updatePrice();
        },

        updatePrice : function() {
            if (!this.currentOffer) {
                return;
            }
            var currentOfferBlock = this.entity('current-offer-block');
            var price = this.currentOffer.MIN_PRICE.DISCOUNT_VALUE;
            if (this.colorId && this.colors[this.colorId] && this.colors[this.colorId]['UF_MARKUP'].length) {
                var markup = parseFloat(this.colors[this.colorId]['UF_MARKUP']);
                if (markup > 0) {
                    price = parseFloat(price) * markup;
                }
            }
            var quantity = parseInt(this.entity('item-quantity', currentOfferBlock).val());
            this.entity('price', currentOfferBlock).text(this.roundNumber(price * quantity, 2));
        },

        roundNumber : function(value, precision) {
            return +parseFloat(value).toFixed(precision);
        },

    }

})();