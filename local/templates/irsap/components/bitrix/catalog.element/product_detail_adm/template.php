<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);

$mainId = $this->GetEditAreaId($arResult['ID']);

$name = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'])
	? $arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
	: $arResult['NAME'];
$title = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_TITLE'])
	? $arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_TITLE']
	: $arResult['NAME'];
$alt = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_ALT'])
	? $arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_ALT']
	: $arResult['NAME'];

$haveOffers = !empty($arResult['OFFERS']);
if ($haveOffers)
{
	$actualItem = isset($arResult['OFFERS'][$arResult['OFFERS_SELECTED']])
		? $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]
		: reset($arResult['OFFERS'])
    ;
}
else
{
	$actualItem = $arResult;
}

$skuProps = array();
$price = $actualItem['ITEM_PRICES'][$actualItem['ITEM_PRICE_SELECTED']];
$measureRatio = $actualItem['ITEM_MEASURE_RATIOS'][$actualItem['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'];

$showDescription = !empty($arResult['PREVIEW_TEXT']) || !empty($arResult['DETAIL_TEXT']);
?>
<div id="product-wrap">
    <section class="sec-banner sec-padding__null">
        <div class="card-product__banner">
            <div class="banner">
                <div class="banner-label">
                    <div class="container">
						<div class="banner-label__text"><?=$arResult['PROPERTIES']['DETAIL_PICTURE_DESC_TOP']['VALUE'];?></div>
                        <h1 class="h1 banner-label__title"><?=$name;?></h1>
                        <div class="banner-label__text">На фото: <?=$arResult['PROPERTIES']['DETAIL_PICTURE_DESC']['VALUE'];?></div>
                    </div>
                </div>
                <img
                    class="card-product__banner-img hidden-banner__des"
                    src="<?=$arResult['DETAIL_PICTURE']['SRC']?>"
                    alt="<?=$alt?>"
                    title="<?=$title?>"
                >
                <img
                    class="card-product__banner-img hidden-banner__mob"
                    src="<?=$arResult['DETAIL_PICTURE']['SRC']?>"
                    alt="<?=$alt?>"
                    title="<?=$title?>"
                >
            </div>
        </div>
    </section>

    <section class="sec-padding__null">
        <div class="calc-payment cp">
            <div class="calc-payment__nav">
                <div class="container">
                    <div class="cp-nav__list">
                        <a href="" class="cp-nav__link border-link border-color">ВАРИАНТЫ ИСПОЛНЕНИЯ  И ЦЕНЫ</a>
                        <a href="#desc_pr" class="cp-nav__link yacor">ОПИСАНИЕ</a>
                        <a href="#char_pr" class="cp-nav__link yacor">ХАРАКТЕРИСТИКИ</a>
                        <a href="#acs_pr" class="cp-nav__link yacor">АКСЕССУАРЫ</a>
                        <a href="#tech_pr" class="cp-nav__link yacor">ТЕХНИЧЕСКИЕ ДАННЫЕ</a>
                    </div>
                </div>
            </div>
            <div class="calc-payment__filter-wrap">
                <div class="container">
                    <div class="calc-payment__filter flex_start_between cpf">
                        <div class="cpf-box cpf-select">
                            <?foreach ($arResult['FILTER_PROPERTIES'] as $code => $property):
                                switch ($property['TYPE']) {
                                    case 'L':
                                        ?>
                                        <div class="cpf-select__type select-box">
                                            <h4 class="bold uppercase type-title"><?=$property['NAME'];?> <?if ($property['HINT']):?><span class="text-normal lowercase"><?=$property['HINT'];?></span><?endif;?></h4>
                                            <div class="cpf-select__radio">
                                                <?foreach ($property['VALUES'] as $key => $value):
                                                    $checked = (isset($actualItem['DISPLAY_PROPERTIES'][$code]) && $actualItem['DISPLAY_PROPERTIES'][$code]['VALUE'] === $value);
                                                    ?>
                                                    <label for="<?=$property['CODE'] . "_" . $key?>" name="l-type" <?if ($checked):?>class="bold"<?endif;?>>
                                                        <input
                                                            class="filter-radio type"
                                                            <?if ($checked):?>checked="checked"<?endif;?>
                                                            id="<?=$property['CODE'] . "_" . $key?>"
                                                            type="radio"
                                                            name="<?=$property['CODE'];?>"
                                                            onchange="productComponent.changeFilterValue('<?=$property['CODE'];?>', this.value)"
                                                            value="<?=$value;?>"
                                                        >
                                                        <?=$value;?>
                                                    </label>
                                                <?endforeach;?>
                                            </div>
                                        </div>
                                        <?
                                        break;
                                    case 'N':
                                        ?>
                                        <div class="cpf-select__range select-box" data-property-code="<?=$property['CODE'];?>">
                                            <div class="range-height">
                                                <h4 class="bold uppercase type-title"><?=$property['NAME'];?> <?if ($property['HINT']):?><span class="text-normal lowercase"><?=$property['HINT'];?></span><?endif;?></h4>
                                                <div class="range-block">
                                                    <div class="range__border">
                                                        <div
                                                            id="<?=$property['CODE'];?>"
                                                            class="range-filter"
                                                            data-prop-code="<?=$property['CODE'];?>"
                                                            data-max="<?=$property['VALUES'][count($property['VALUES']) - 1]?>"
                                                            data-min="<?=$property['VALUES'][0]?>"
                                                            data-val="<?=$actualItem['DISPLAY_PROPERTIES'][$code]['VALUE']?>"
                                                        ></div>
                                                    </div>
                                                    <div class="flex_start_between range">
                                                        <div class="range__start"></div>
                                                        <div class="range__finish"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?
                                        break;
                                }
                            endforeach;
                            ?>
                            <div class="cpf-select__type select-box">
                                <div>
                                    <div class="popup-link">
                                        <a href="" class="color-help__link" data-entity="open-help-window-btn">
                                            <img src="<?=SITE_TEMPLATE_PATH;?>/images/icon_ask.png" alt="">Помочь рассчитать
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <?if ($arResult['DEFAULT_COLOR']):?>
                                <div class="cpf-select__color select-box">
                                    <h4 class="bold uppercase type-title">Цвет</h4>
                                    <div class="select-color flex_center_start">
                                        <? $colorImgUrl = $arResult['DEFAULT_COLOR']['UF_FILE']; ?>
                                        <a
                                            data-entity="current-color-btn"
                                            class="select-color__link"
                                            style="background-image: url(<?=$colorImgUrl;?>);background-repeat:no-repeat;background-position:center;background-size:contain;"
                                        ></a>
                                        <span data-entity="current-color-name"><?=$arResult['DEFAULT_COLOR']['UF_LIST_NAME'];?></span>
                                    </div>
                                    <?if (!empty($arResult['ALL_COLORS'])):?>
                                        <div class="cpf-color__help">
                                            <div class="popup-link">
                                                <a class="color-help__link view-colorBlock" data-entity="open-color-window-btn">
                                                    <img src="<?=SITE_TEMPLATE_PATH;?>/images/color-select__icon.png" alt="" class="color-help__img">Другие цвета под заказ
                                                </a>
                                            </div>
                                        </div>
                                    <?endif;?>
                                </div>
                            <?endif;?>
                        </div>
                        <div data-entity="current-offer-block" class="cpf-center">
                            <div class="calc-box__item calc-box__model">
                                <div class="">
                                    <h4 class="bold uppercase type-title">Ваш заказ</h4>
                                    <div class="calc-total__order">
                                        <div class="calc-total__desp">
                                            <div class="total-value__text">
											 <?//if (!empty($actualItem["PREVIEW_PICTURE"]["SRC"])){?>
												  <img
													class="card-product_actual-img"
													data-entity="actual_img"
													src="<?=$actualItem["PREVIEW_PICTURE"]["SRC"]?>"
													alt="<?=$actualItem["PREVIEW_PICTURE"]["ALT"]?>"
													title="<?=$actualItem["PREVIEW_PICTURE"]["TITLE"]?>"
												>																						   
											<?//}?>											   
                                            </div>
											<div class="total-value__text">
                                                <span class="value-name bold"><?=$arResult['PROPERTIES']['DETAIL_PICTURE_DESC_TOP']['VALUE'];?></span>                                            
                                            </div>
											<div class="total-value__text">
                                                <span class="value-name">Артикул:</span>
                                                <span class="value-text" data-entity="artnumber"><?=$actualItem['PROPERTIES']['ARTNUMBER']['VALUE'];?></span>
                                            </div>
                                            <div class="total-value__text" data-entity="values-block">
                                                <?foreach (array_keys($arResult['FILTER_PROPERTIES']) as $index => $code):
                                                    if (empty($actualItem['PROPERTIES'][$code]) || empty($actualItem['PROPERTIES'][$code]['VALUE'])) {
                                                        continue;
                                                    }?>
                                                    <span class="value-name"><?=$actualItem['PROPERTIES'][$code]['NAME'];?>:</span>
                                                    <span class="value-text"><?=$actualItem['PROPERTIES'][$code]['VALUE'];?><?if ($index < (count($arResult['FILTER_PROPERTIES']) - 1)):?>,<?endif;?></span>
                                                <?endforeach;?>
                                            </div>
                                            <?if ($arResult['DEFAULT_COLOR']):?>
                                                <div class="total-value__text">
                                                    <span class="value-name">Цвет: </span><span class="value-text vert-line" data-entity="color-name"><?=$arResult['DEFAULT_COLOR']['UF_LIST_NAME'];?></span>
                                                    <span class="value-name">RAL: </span><span class="value-text vert-line" data-entity="color-ral"><?=$arResult['DEFAULT_COLOR']['UF_RAL'];?></span>
                                                    <span class="value-name">Код: </span><span class="value-text vert-line" data-entity="color-code"><?=$arResult['DEFAULT_COLOR']['UF_CODE'];?></span>
                                                    <span class="value-name">Серия: </span><span class="value-text vert-line" data-entity="color-series"><?=$arResult['DEFAULT_COLOR']['UF_SERIES'];?></span>
                                                    <span class="value-name">Покрытие: </span><span class="value-text" data-entity="color-coating"><?=$arResult['DEFAULT_COLOR']['UF_COATING'];?></span>
                                                </div>
                                            <?endif;?>
                                        </div>
                                        <?if (!empty($actualItem['PROPERTIES']['LABEL'])
                                            && !empty($actualItem['PROPERTIES']['LABEL']['VALUE'])
                                            && is_array($actualItem['PROPERTIES']['LABEL']['VALUE'])
                                        ):?>
                                            <div class="calc-total__tags" data-entity="labels-block">
                                                <?foreach ($actualItem['PROPERTIES']['LABEL']['VALUE'] as $value):?>
                                                    <div class="total__tags"><?=$value;?></div>
                                                <?endforeach;?>
                                            </div>
                                        <?endif;?>
                                    </div>

                                    <div class="popup-link total-result__stock">
                                        <? $inStock = $actualItem['PROPERTIES']['STOCK']['VALUE_XML_ID'] == STOCK_IN_STOCK; ?>
                                        <a href="javascript:void(0)" data-entity="in-stock" class="color-help__link color-default" <?if (!$inStock):?>style="display: none"<?endif;?>>
                                            <span><img src="<?=SITE_TEMPLATE_PATH;?>/images/nal.png" alt=""></span>В наличии
                                        </a>
                                        <a href="javascript:void(0)" data-entity="out-stock" class="color-help__link color-default" <?if ($inStock):?>style="display: none"<?endif;?>>
                                            Под заказ
                                        </a>
                                    </div>

                                    <div class="calc-total__result">
                                        <div class="flex_center_between">
                                            <div class="count flex_center_start">
                                                <div class="kolst">Кол-во шт.:</div>
                                                <select name="" id="" class="select_style" data-entity="item-quantity" onchange="productComponent.updatePrice()">
                                                    <? $maxCount = 10;
                                                    for ($i = 1; $i <= $maxCount; $i++):?>
                                                        <option value="<?=$i;?>"><?=$i;?></option>
                                                    <?endfor;?>
                                                </select>
                                            </div>
                                            <div class="total-result__price">
                                                <span class="price-text">Цена:</span>
                                                <?
                                                $price = $actualItem['MIN_PRICE']['DISCOUNT_VALUE'];
                                                if ($arResult['DEFAULT_COLOR'] && $arResult['DEFAULT_COLOR']['UF_MARKUP']) {
                                                    $price = $price * (float) $arResult['DEFAULT_COLOR']['UF_MARKUP'];
                                                }
                                                $currency = AppMainHelper::getPublicCurrency($actualItem['MIN_PRICE']['CURRENCY']);
                                                ?>
                                                <span class="price">
                                                    <span data-entity="price"><?=round($price,2);?></span>
                                                    <span class="currency"><?=$currency;?></span>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="flex_center_between">
                                            <div class="total-result__btn">
                                                <a href="javascript:void(0)" data-entity="cart-btn" class="style_btn3">Добавить в заказ</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div data-entity="no-offer-block" class="cpf-center" style="display: none">
                            <div class="calc-box__item calc-box__model">
                                <h4 class="bold uppercase type-title">Ничего не найдено</h4>
                            </div>
                        </div>
                        <?if (!empty($arResult['PROPERTIES']['STANDARD_COMPLECT'])
                            && !empty($arResult['PROPERTIES']['STANDARD_COMPLECT']['VALUE'])
                            && is_array($arResult['PROPERTIES']['STANDARD_COMPLECT']['VALUE'])
                        ):?>
                            <div class="cpf-box cpf-desp">
                                <h4 class="bold uppercase type-title">Стандартная комплектация</h4>
                                <ul class="extra-char__list list">
                                    <?foreach ($arResult['PROPERTIES']['STANDARD_COMPLECT']['VALUE'] as $value):?>
                                        <li class="extra-char__item"><?=$value;?></li>
                                    <?endforeach;?>
                                </ul>
                                <div class="cpf-desp__btn">
                                    <a href="#acs_pr" class="cpf-desp__link flex_center border-link yacor">Дополнительные аксессуары</a>
                                </div>
                            </div>
                        <?endif;?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?if (!empty($arResult['PROPERTIES']['DESC_BLOCKS']['VALUE'])):?>
        <section id="desc_pr">
            <?foreach ($arResult['PROPERTIES']['DESC_BLOCKS']['VALUE'] as $value):
                $class = $value['COLOR'] == 'DARK' ? 'info-back__dark' : 'info-back__light';
            ?>
                <div class="info <?=$class;?>" style="background-image: url(<?=$value['IMAGE'];?>)">
                    <div class="container">
                        <div class="flex_start_center">
                            <?if ($value['POSITION'] == 'RIGHT'):?>
                                <div class="info__desp flex50"></div>
                            <?endif;?>
                            <div class="info__desp flex50">
                                <?if (mb_strlen($value['TITLE']) > 0):?>
                                    <h2><?=$value['TITLE'];?></h2>
                                <?endif;?>
                                <div>
                                    <p class="info__text"><?=nl2br($value['DESCRIPTION']);?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?endforeach;?>
        </section>
    <?endif;?>

    <?if (!empty($arResult['PROPERTIES']['INTERIOR_PHOTO']['VALUE'])):?>
        <section>
            <div class="container">
                <h2 class="h2 section-product galleryh2"><span>Фото <?=$arResult['NAME'];?> в интерьере</span></h2>
                <div class="gallery imggallery">
                    <?foreach ($arResult['PROPERTIES']['INTERIOR_PHOTO']['VALUE'] as $index => $value):?>
                        <a data-fancybox="gallery" href="<?=CFile::GetPath($value);?>" class="gallery-flex">
                            <img src="<?=CFile::GetPath($value);?>" alt="" class="gallery-photo__item">
                            <div class="galley-label__box">
                                <div class="gallery-img__label gallery-img__label-m"><?=html_entity_decode($arResult['PROPERTIES']['INTERIOR_PHOTO']['DESCRIPTION'][$index]);?></div>
                            </div>
                        </a>
                    <?endforeach;?>
                </div>
            </div>
        </section>
    <?endif;?>

    <section class="extra-wrap" id="char_pr">
        <div class="container">
            <div class="extra">
                <?if (!empty($arResult['PROPERTIES']['CERTS']['VALUE'])):
                    $certs = AppMainHelper::getHlBlockRows(CERTS_HIGHLOADBLOCK_ID, $arResult['PROPERTIES']['CERTS']['VALUE'], true);
                    ?>
                    <div class="extra-sert">
                        <div class="extra__title">Сертификаты</div>
                        <div class="extra-sert__logo">
                            <?foreach ($certs as $cert):?>
                                <div class="logo-wrap">
                                    <img src="<?=CFile::GetPath($cert['UF_FILE']);?>" alt="<?=$cert['UF_DESCRIPTION'];?>" class="logo-wrap__icon">
                                </div>
                            <?endforeach;?>
                        </div>
                    </div>
                <?endif;?>
                <?if (!empty($arResult['PROPERTIES']['PLUSES']['VALUE'])):
                    $pluses = AppMainHelper::getHlBlockRows(PLUSES_HIGHLOADBLOCK_ID, $arResult['PROPERTIES']['PLUSES']['VALUE'], true);
                    ?>
                    <div class="extra-advantage">
                        <div class="extra__title">ПЛЮСЫ <?=$arResult['NAME'];?></div>
                        <div class="extra-advantage__plus">
                            <?foreach ($pluses as $plus):?>
                                <div class="plus-item">
                                    <img src="<?=CFile::GetPath($plus['UF_FILE']);?>" alt="<?=$plus['UF_DESCRIPTION'];?>" class="plus-item__icon">
                                </div>
                            <?endforeach;?>
                        </div>
                    </div>
                <?endif;?>
                <?if (!empty($arResult['PROPERTIES']['TECH_INFO']['VALUE'])):?>
                    <div class="extra-char">
                        <div class="extra__title">ТЕХНИЧЕСКИЕ ХАРАКТЕРИСТИКИ</div>
                        <ul class="extra-char__list list">
                            <?foreach ($arResult['PROPERTIES']['TECH_INFO']['VALUE'] as $index => $value):
                                $lastSymbol = $index === (count($arResult['PROPERTIES']['TECH_INFO']['VALUE']) - 1) ? '.' : ';';
                                ?>
                                <li class="extra-char__item"><?=$value . $lastSymbol;?></li>
                            <?endforeach;?>
                        </ul>
                    </div>
                <?endif;?>
            </div>
        </div>
    </section>

    <?if (!empty($arResult['PROPERTIES']['ACCESSUARS']['VALUE'])):?>
        <section class="popular-index" id="acs_pr">
            <?$APPLICATION->IncludeComponent(
                "bitrix:catalog.section",
                "product_slider",
                Array(
                    "ACTION_VARIABLE" => "action",
                    "ADD_PICT_PROP" => "",
                    "ADD_PROPERTIES_TO_BASKET" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "ADD_TO_BASKET_ACTION" => "ADD",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "BACKGROUND_IMAGE" => "",
                    "BASKET_URL" => "/cart/",
                    "BRAND_PROPERTY" => "BRAND_REF",
                    "BROWSER_TITLE" => "-",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "N",
                    "CACHE_TIME" => "36000000",
                    "CACHE_TYPE" => "N",
                    "COMPATIBLE_MODE" => "Y",
                    "CONVERT_CURRENCY" => "Y",
                    "CURRENCY_ID" => "RUB",
                    "CUSTOM_FILTER" => "",
                    "DATA_LAYER_NAME" => "dataLayer",
                    "DETAIL_URL" => "",
                    "DISABLE_INIT_JS_IN_COMPONENT" => "N",
                    "DISCOUNT_PERCENT_POSITION" => "bottom-right",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "ELEMENT_SORT_FIELD" => "sort",
                    "ELEMENT_SORT_FIELD2" => "id",
                    "ELEMENT_SORT_ORDER" => "asc",
                    "ELEMENT_SORT_ORDER2" => "desc",
                    "ENLARGE_PRODUCT" => "PROP",
                    "ENLARGE_PROP" => "NEWPRODUCT",
                    "FILTER_NAME" => "arrFilter",
                    "HIDE_NOT_AVAILABLE" => "Y",
                    "HIDE_NOT_AVAILABLE_OFFERS" => "Y",
                    "IBLOCK_ID" => "5",
                    "IBLOCK_TYPE" => "catalog",
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "LABEL_PROP" => array("NEWPRODUCT"),
                    "LABEL_PROP_MOBILE" => array(),
                    "LABEL_PROP_POSITION" => "top-left",
                    "LAZY_LOAD" => "N",
                    "LINE_ELEMENT_COUNT" => "",
                    "LOAD_ON_SCROLL" => "N",
                    "MESSAGE_404" => "",
                    "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                    "MESS_BTN_BUY" => "Купить",
                    "MESS_BTN_DETAIL" => "Подробнее",
                    "MESS_BTN_LAZY_LOAD" => "Показать ещё",
                    "MESS_BTN_SUBSCRIBE" => "Подписаться",
                    "MESS_NOT_AVAILABLE" => "Нет в наличии",
                    "META_DESCRIPTION" => "-",
                    "META_KEYWORDS" => "-",
                    "OFFERS_CART_PROPERTIES" => array(),
                    "OFFERS_FIELD_CODE" => array(),
                    "OFFERS_LIMIT" => "0",
                    "OFFERS_PROPERTY_CODE" => array(),
                    "OFFERS_SORT_FIELD" => "sort",
                    "OFFERS_SORT_FIELD2" => "id",
                    "OFFERS_SORT_ORDER" => "asc",
                    "OFFERS_SORT_ORDER2" => "desc",
                    "OFFER_ADD_PICT_PROP" => "MORE_PHOTO",
                    "OFFER_TREE_PROPS" => array(),
                    "PAGER_BASE_LINK_ENABLE" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => ".default",
                    "PAGER_TITLE" => "Товары",
                    "PAGE_ELEMENT_COUNT" => "50",
                    "PARTIAL_PRODUCT_PROPERTIES" => "N",
                    "PRICE_CODE" => array("BASE"),
                    "PRICE_VAT_INCLUDE" => "Y",
                    "PRODUCT_BLOCKS_ORDER" => "",
                    "PRODUCT_DISPLAY_MODE" => "Y",
                    "PRODUCT_ID_VARIABLE" => "id",
                    "PRODUCT_PROPERTIES" => array("LABEL"),
                    "PRODUCT_PROPS_VARIABLE" => "prop",
                    "PRODUCT_QUANTITY_VARIABLE" => "",
                    "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':true}]",
                    "PRODUCT_SUBSCRIPTION" => "Y",
                    "PROPERTY_CODE" => array("LABEL",""),
                    "PROPERTY_CODE_MOBILE" => array(),
                    "RCM_PROD_ID" => "",
                    "RCM_TYPE" => "personal",
                    "SECTION_CODE" => "",
                    "SECTION_ID" => "",
                    "SECTION_ID_VARIABLE" => "",
                    "SECTION_URL" => "",
                    "SECTION_USER_FIELDS" => array("",""),
                    "SEF_MODE" => "N",
                    "SET_BROWSER_TITLE" => "N",
                    "SET_LAST_MODIFIED" => "N",
                    "SET_META_DESCRIPTION" => "N",
                    "SET_META_KEYWORDS" => "N",
                    "SET_STATUS_404" => "N",
                    "SET_TITLE" => "N",
                    "SHOW_404" => "N",
                    "SHOW_ALL_WO_SECTION" => "N",
                    "SHOW_CLOSE_POPUP" => "N",
                    "SHOW_DISCOUNT_PERCENT" => "Y",
                    "SHOW_FROM_SECTION" => "N",
                    "SHOW_MAX_QUANTITY" => "N",
                    "SHOW_OLD_PRICE" => "N",
                    "SHOW_PRICE_COUNT" => "1",
                    "SHOW_SLIDER" => "N",
                    "SLIDER_INTERVAL" => "3000",
                    "SLIDER_PROGRESS" => "N",
                    "TEMPLATE_THEME" => "blue",
                    "USE_ENHANCED_ECOMMERCE" => "N",
                    "USE_MAIN_ELEMENT_SECTION" => "N",
                    "USE_PRICE_COUNT" => "N",
                    "USE_PRODUCT_QUANTITY" => "N",
                    "BLOCK_TITLE" => "Аксессуары для {$arResult['NAME']}"
                )
            );?>
        </section>
    <?endif;?>

    <section class="table-card" id="tech_pr">
        <div class="hidden-mob">
            <div class="container">
                <div class="tabs-info">
                    <ul>
                        <li><a href="#tabs-1">Технические данные</a></li>
                        <? 
                        $tabIndex = 2;
                        foreach ($arResult['SORTED_DOC_CATEGORIES'] as $category):
                            if (empty($arResult['GROUPED_DOCS'][$category]['DOCUMENTS'])) {
                                continue;
                            }
                            ?>
                            <li><a href="#tabs-<?=$tabIndex++;?>"><?=$arResult['GROUPED_DOCS'][$category]['CATEGORY_NAME'];?></a></li>
                        <?endforeach;?>
                    </ul>
                    <div id="tabs-1"><?=$arResult['PROPERTIES']['TAB_TECH_DATA']['~VALUE']['TEXT'];?></div>
                    <? 
                        $tabIndex = 2;
                        foreach ($arResult['SORTED_DOC_CATEGORIES'] as $category):
                            if (empty($arResult['GROUPED_DOCS'][$category]['DOCUMENTS'])) {
                                continue;
                            }
                            ?>
                            <div id="tabs-<?=$tabIndex++;?>">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Имя</th>
                                            <th>Тип</th>
                                            <th>Скачать</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <? foreach ($arResult['GROUPED_DOCS'][$category]['DOCUMENTS'] as $document): 
                                            $file = CFile::GetPath($document['PROPERTIES']['FILE']['VALUE']);
                                            $extension = strtoupper(GetFileExtension($file));
                                            ?>
                                            <tr>
                                                <td><?=$document['NAME'];?></td>
                                                <td><?=(empty($document['PROPERTIES']['TYPE']['VALUE']) ? '-' : $document['PROPERTIES']['TYPE']['VALUE']);?></td>
                                                <td>
                                                    <a href="<?=$file;?>" class="pdf-link" target="_blank" download>
                                                        <span>Скачать</span>
                                                        <span class="badge"><?=$extension;?></span>
                                                    </a>
                                                </td>
                                            </tr>
                                        <? endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        <?endforeach;?>
                </div>
            </div>
        </div>
        <div class="hidden-des">
            <div class="container">
                <h2 class="section-product"><span>Технические данные</span></h2>
                <ul class="tab-ul">
                    <li>
						<a href="#" class="tab-style">Технические данные</a>
						<div class="li-cont"><?=$arResult['PROPERTIES']['TAB_TECH_DATA']['~VALUE']['TEXT'];?></div>
                    </li>
                    <?foreach ($arResult['SORTED_DOC_CATEGORIES'] as $category):
                        if (empty($arResult['GROUPED_DOCS'][$category]['DOCUMENTS'])) {
                            continue;
                        }
                        ?>
                        <li>
                            <a href="#" class="tab-style"><?=$arResult['GROUPED_DOCS'][$category]['CATEGORY_NAME'];?></a>
                            <div class="li-cont">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Имя</th>
                                            <th>Тип</th>
                                            <th>Скачать</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <? foreach ($arResult['GROUPED_DOCS'][$category]['DOCUMENTS'] as $document): 
                                            $file = CFile::GetPath($document['PROPERTIES']['FILE']['VALUE']);
                                            $extension = strtoupper(GetFileExtension($file));
                                            ?>
                                            <tr>
                                                <td><?=$document['NAME'];?></td>
                                                <td><?=(empty($document['PROPERTIES']['TYPE']['VALUE']) ? '-' : $document['PROPERTIES']['TYPE']['VALUE']);?></td>
                                                <td>
                                                    <a href="<?=$file;?>" class="pdf-link" target="_blank" download>
                                                        <span>Скачать</span>
                                                        <span class="badge"><?=$extension;?></span>
                                                    </a>
                                                </td>
                                            </tr>
                                        <? endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </li>
                    <?endforeach;?>
                </ul>
            </div>
        </div>
    </section>

    <section id="ps__id" data-entity="color-window" class="popup popup-select ps">
        <div class="ps-wrap">
            <div class="ps-container">
                <div class="ps-color">
                    <h2 class="section-product h2 ps-color"><span>Выбор цвета</span></h2>
                    <?foreach ($arResult['SORTED_COLOR_GROUPS'] as $groupCode):
                        if (empty($arResult['GROUPED_COLORS'][$groupCode])) {
                            continue;
                        }
                        $group = $arResult['GROUPED_COLORS'][$groupCode];
                    ?>
                        <div data-entity="color-group-block" class="ps-color__box">
                            <div class="ps-tab">
                                <div class="ps-tab__name "><span><?=$group['CATEGORY_NAME'];?></span></div>
                                <div class="ps__bottom-line"></div>
                            </div>
                            <div data-entity="selected-color-description">
                                <?if ($arResult['DEFAULT_COLOR'] && $arResult['DEFAULT_COLOR']['UF_CATEGORY_XML_ID'] == $groupCode):?>
                                    <div class="ps-top__text">
                                        <?=sprintf(
                                            'Выбрано: %s  |  RAL: %s  |  Код: %s  |  Серия: %s  |  Покрытие: %s',
                                            $arResult['DEFAULT_COLOR']['UF_LIST_NAME'],
                                            $arResult['DEFAULT_COLOR']['UF_RAL'] ?? '-',
                                            $arResult['DEFAULT_COLOR']['UF_CODE'] ?? '-',
                                            $arResult['DEFAULT_COLOR']['UF_SERIES'] ?? '-',
                                            $arResult['DEFAULT_COLOR']['UF_COATING'] ?? '-'
                                        );?>
                                    </div>
                                <?endif;?>
                            </div>
                            <?foreach ($arResult['SORTED_COLOR_STOCKS'] as $stockId):
                                if (empty($group['STOCKS'][$stockId])) {
                                    continue;
                                }
                                $stockType = $group['STOCKS'][$stockId];
                            ?>
                                <div data-entity="color-stock-block" class="ps-color__enum">
                                    <div class="enum-title ps-title"><?=$stockType['STOCK_TYPE'];?></div>
                                    <div class="enum-color">
                                        <?foreach ($stockType['COLORS'] as $color):
                                            $colorImgUrl = $color['UF_FILE'];
                                            $selected = ($arResult['DEFAULT_COLOR'] && $arResult['DEFAULT_COLOR']['ID'] === $color['ID']);
                                            ?>
                                            <a
                                                href="javascript:void(0)"
                                                data-entity="color-btn"
                                                data-color-id="<?=$color['ID'];?>"
                                                class="ps-color__link <?if ($selected):?>active<?endif;?>"
                                                onclick="productComponent.changeColor(<?=$color['ID'];?>)"
                                                style="background-image: url(<?=$colorImgUrl;?>);background-repeat:no-repeat;background-position:center;background-size:contain;"
                                            ></a>
                                        <?endforeach;?>
                                    </div>
                                </div>
                            <?endforeach;?>
                        </div>
                    <?endforeach;?>
                    <div class="ps-color__note"></div>
                    <div id="close-id" class="close"><a class="close-link" data-entity="color-window-close"><i class="fa fa-times" aria-hidden="true"></i></a></div>
                </div>
            </div>
        </div>
    </section>
</div>
<?

$jsParams = [
    'BLOCK_SELECTOR' => '#product-wrap',
    'CURRENT_OFFER' => $actualItem,
    'OFFERS' => $arResult['OFFERS'],
    'COLOR_ID' => !empty($arResult['DEFAULT_COLOR']['ID']) ? $arResult['DEFAULT_COLOR']['ID'] : '',
    'COLORS' => $arResult['ALL_COLORS'],
    'CURRENT_FILTER' => $arResult['CURRENT_FILTER'],
    'FILTER_PROPERTIES' => $arResult['FILTER_PROPERTIES'],
    'CONSTANTS' => [
        'IN_STOCK' => STOCK_IN_STOCK,
    ]
];

?>
<script>
    var productComponent = new JCIrsapProductComponent(<?=CUtil::PhpToJSObject($jsParams, false, true);?>);
</script>
