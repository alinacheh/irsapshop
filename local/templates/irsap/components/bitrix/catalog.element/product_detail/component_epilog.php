<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();?>
<section id="help" data-entity="help-window" class="popup">
    <h2 class="section-product h2 ps-color"><span>ПОМОЧЬ РАССЧИТАТЬ</span></h2>
    <div class="close">
        <a class="close-link" data-entity="help-window-close"><i class="fa fa-times" aria-hidden="true"></i></a>
    </div>
    <div id="help-form-wrap" data-entity="help-window-wrap">
        <?$APPLICATION->IncludeComponent(
            "bitrix:form",
            "help",
            array(
                "AJAX_MODE" => "Y",
                "SEF_MODE" => "N",
                "WEB_FORM_ID" => 2,
                "RESULT_ID" => $_REQUEST["RESULT_ID"],
                "START_PAGE" => "new",
                "SHOW_LIST_PAGE" => "N",
                "SHOW_EDIT_PAGE" => "N",
                "SHOW_VIEW_PAGE" => "N",
                "SUCCESS_URL" => "",
                "SUCCESS_TEXT" => "Ваше сообщение успешно отправлено. Наши менеджеры ответят Вам в ближайшее время.",
                "SHOW_ANSWER_VALUE" => "Y",
                "SHOW_ADDITIONAL" => "Y",
                "SHOW_STATUS" => "Y",
                "EDIT_ADDITIONAL" => "Y",
                "EDIT_STATUS" => "Y",
                "NOT_SHOW_FILTER" => array(
                    0 => "",
                    1 => "",
                ),
                "NOT_SHOW_TABLE" => array(
                    0 => "",
                    1 => "",
                ),
                "CHAIN_ITEM_TEXT" => "",
                "CHAIN_ITEM_LINK" => "",
                "IGNORE_CUSTOM_TEMPLATE" => "Y",
                "USE_EXTENDED_ERRORS" => "Y",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "3600",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "SEF_FOLDER" => "/communication/web-forms/",
                "AJAX_OPTION_ADDITIONAL" => "",
                "VARIABLE_ALIASES" => array(
                    "action" => "action",
                ),
                "PRIVACY_LINK" => "/privacy/",
                "TARGET_URL" => AppMainHelper::getFullURL(),
            ),
            false
        );?>
    </div>
</section>
<script>
    $(document).ready(function () {
        $('[data-entity="open-help-window-btn"]').on('click', function(e) {
			e.preventDefault();
			$("#help").show();
			$(".overlay").show();
		});

		$('[data-entity="help-window-close"]').on('click', function(e) {
			e.preventDefault();
			$("#help").hide();
			$(".overlay").hide();
		});
    });
</script>