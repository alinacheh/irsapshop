<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */

use Bitrix\Highloadblock\HighloadBlockTable;

CModule::IncludeModule('highloadblock');

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

/** Custom Irsap */

if ($arParams['OFFER_ID']) {
    foreach ($arResult['OFFERS'] as $index => $offer) {
        if ($offer['ID'] != (int) $arParams['OFFER_ID']) {
            continue;
        }
        $arResult['OFFERS_SELECTED'] = $index;
    }
}

$arFilterProperties = [];

// Collect all properties for filter
foreach ($arResult['OFFERS'] as $offer) {
    if (empty($offer['DISPLAY_PROPERTIES'])) {
        continue;
    }
    foreach ($offer['DISPLAY_PROPERTIES'] as $code => $property) {
        if (empty($property['VALUE'])) {
            continue;
        }
        if (!isset($arFilterProperties[$code])) {
            $arFilterProperties[$code] = [
                'ID' => $property['ID'],
                'CODE' => $code,
                'NAME' => $property['NAME'],
                'TYPE' => $property['PROPERTY_TYPE'],
                'HINT' => $property['HINT'],
                'VALUES' => [],
            ];
        }
        if (!in_array($property['VALUE'], $arFilterProperties[$code]['VALUES'])) {
            $arFilterProperties[$code]['VALUES'][] = $property['VALUE'];
        }
    }
}

if (!empty($arFilterProperties)) {
    foreach ($arFilterProperties as $code => $property) {
        sort($arFilterProperties[$code]['VALUES']);
    }
}

$arResult['FILTER_PROPERTIES'] = $arFilterProperties;
unset($arFilterProperties);

// Build current filter values
$actualItem = isset($arResult['OFFERS'][$arResult['OFFERS_SELECTED']])
    ? $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]
    : reset($arResult['OFFERS'])
;

$arResult['CURRENT_FILTER'] = [];
foreach (array_keys($arResult['FILTER_PROPERTIES']) as $code) {
    $arResult['CURRENT_FILTER'][$code] = $actualItem['PROPERTIES'][$code]['VALUE'];
}

// Default color
$arResult['DEFAULT_COLOR'] = null;
if (!empty($arResult['PROPERTIES']['DEFAULT_COLOR']['VALUE'])) {
    $colorRow = AppMainHelper::getHlBlockRows(COLORS_HIGHLOADBLOCK_ID, [$arResult['PROPERTIES']['DEFAULT_COLOR']['VALUE']], true);
    $arResult['DEFAULT_COLOR'] = isset($colorRow[0]) ? $colorRow[0] : null;
    $arResult['DEFAULT_COLOR']['UF_FILE'] = CFile::GetPath($arResult['DEFAULT_COLOR']['UF_FILE']);
}

// All colors
$arResult['ALL_COLORS'] = [];
if (!empty($arResult['PROPERTIES']['COLORS'])
    && !empty($arResult['PROPERTIES']['COLORS']['VALUE'])
    && is_array($arResult['PROPERTIES']['COLORS']['VALUE'])
) {
	$colors = AppMainHelper::getHlBlockRows(COLORS_HIGHLOADBLOCK_ID, $arResult['PROPERTIES']['COLORS']['VALUE'], true);
    foreach ($colors as $color) {
        $color['UF_FILE'] = CFile::GetPath($color['UF_FILE']);
        $arResult['ALL_COLORS'][$color['ID']] = $color;
    }
}

if (!empty($arResult['DEFAULT_COLOR']) && empty($arResult['ALL_COLORS'][$arResult['DEFAULT_COLOR']['ID']])) {
    $arResult['ALL_COLORS'][$arResult['DEFAULT_COLOR']['ID']] = $arResult['DEFAULT_COLOR'];
}

// Colors grouped by category
$categoryXmlIds = $stockXmlIds = [];
$arResult['GROUPED_COLORS'] = [];
if (!empty($arResult['ALL_COLORS'])) {
    foreach ($arResult['ALL_COLORS'] as $color) {
        if (!isset($arResult['GROUPED_COLORS'][$color['UF_CATEGORY_XML_ID']])) {
            $categoryXmlIds[] = $color['UF_CATEGORY_XML_ID'];
            $arResult['GROUPED_COLORS'][$color['UF_CATEGORY_XML_ID']] = [
                'CATEGORY_NAME' => AppMainHelper::capitalize($color['UF_CATEGORY'] ?? ''),
                'STOCKS' => [],
            ];
        }
        if (!isset($arResult['GROUPED_COLORS'][$color['UF_CATEGORY_XML_ID']]['STOCKS'][$color['UF_STOCK_XML_ID']])) {
            $stockXmlIds[] = $color['UF_STOCK_XML_ID'];
            $arResult['GROUPED_COLORS'][$color['UF_CATEGORY_XML_ID']]['STOCKS'][$color['UF_STOCK_XML_ID']] = [
                'STOCK_TYPE' => AppMainHelper::capitalize($color['UF_STOCK'] ?? ''),
                'COLORS' => [],
            ];
        }
        $arResult['GROUPED_COLORS'][$color['UF_CATEGORY_XML_ID']]['STOCKS'][$color['UF_STOCK_XML_ID']]['COLORS'][] = $color;
    }
}

// Sorted ids color groups and stocks
$arResult['SORTED_COLOR_GROUPS'] = [];
if (!empty($categoryXmlIds)) {
    $dbCategotyEnums = CUserFieldEnum::GetList(['SORT' => 'ASC'], [
        'USER_FIELD_NAME' => 'UF_CATEGORY',
        'XML_ID' => $categoryXmlIds,
    ]);
    while ($arCategoryEnum = $dbCategotyEnums->GetNext()) {
        $arResult['SORTED_COLOR_GROUPS'][] = $arCategoryEnum['XML_ID'];
    }
}

$arResult['SORTED_COLOR_STOCKS'] = [];
if (!empty($stockXmlIds)) {
    $dbStockEnums = CUserFieldEnum::GetList(['SORT' => 'ASC'], [
        'USER_FIELD_NAME' => 'UF_STOCK',
        'XML_ID' => $stockXmlIds,
    ]);
    while ($arStockEnum = $dbStockEnums->GetNext()) {
        $arResult['SORTED_COLOR_STOCKS'][] = $arStockEnum['XML_ID'];
    }
}

/** Prepare documents */
$categoryXmlIds = [];
$arResult['DOCUMENTS'] = [];
$docIds = $arResult['PROPERTIES']['DOCS']['VALUE'];
if (!empty($docIds)) {
    $dbDocs = CIblockElement::GetList(
        ['SORT' => 'ASC'],
        ['ACTIVE' => 'Y', 'IBLOCK_ID' => DOCUMENTS_IBLOCK_ID, '=ID' => $docIds],
        false,
        false,
        ['ID', 'IBLOCK_ID', 'NAME']
    );
    while ($obDoc = $dbDocs->GetNextElement()) {
        $document = $obDoc->GetFields();
        $document['PROPERTIES'] = $obDoc->GetProperties();
        $arResult['DOCUMENTS'][$document['ID']] = $document;
    }
}

$arResult['SORTED_DOC_CATEGORIES'] = [];
if (!empty($arResult['DOCUMENTS'])) {
    $arResult['GROUPED_DOCS'] = [];

    foreach ($arResult['DOCUMENTS'] as $doc) {
        if (!isset($arResult['GROUPED_DOCS'][$doc['PROPERTIES']['CATEGORY']['VALUE_XML_ID']])) {
            $categoryXmlIds[] = $doc['PROPERTIES']['CATEGORY']['VALUE_XML_ID'];
            $arResult['GROUPED_DOCS'][$doc['PROPERTIES']['CATEGORY']['VALUE_XML_ID']] = [
                'CATEGORY_NAME' => AppMainHelper::capitalize($doc['PROPERTIES']['CATEGORY']['VALUE'] ?? ''),
                'DOCUMENTS' => [],
            ];
        }
		$arResult['GROUPED_DOCS'][$doc['PROPERTIES']['CATEGORY']['VALUE_XML_ID']]['DOCUMENTS'][] = $doc;
    }

    if (!empty($categoryXmlIds)) {
        $dbDocumentEnums = CIBlockPropertyEnum::GetList(['SORT' => 'ASC'], [
            'CODE' => 'CATEGORY',
            'XML_ID' => $categoryXmlIds,
            'IBLOCK_ID' => DOCUMENTS_IBLOCK_ID,
        ]);
        while ($arDocumentEnum = $dbDocumentEnums->GetNext()) {
            $arResult['SORTED_DOC_CATEGORIES'][] = $arDocumentEnum['XML_ID'];
        }
    }
}