<?

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @param array $arParams
 * @param array $arResult
 * @param CBitrixComponentTemplate $this
 */

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
?>
<div class="form-cmn flex_stretch_between pb">
    <h2><?=Loc::getMessage("AUTH_REGISTER") ?></h2>
</div>

<?if ($USER->IsAuthorized()):?>
    <div class="form-cmn flex_stretch_between">
        <?=ShowMessage(Loc::getMessage("MAIN_REGISTER_AUTH"));?>
    </div>
<?
    return;
endif;?>

<?if ($arResult["VALUES"]["USER_ID"] > 0):?>
    <?if ($arResult["USE_EMAIL_CONFIRMATION"] === "Y"):?>
        <div class="form-cmn flex_stretch_between">
            <?=ShowMessage(Loc::getMessage("REGISTER_EMAIL_WILL_BE_SENT"));?>
        </div>
    <?endif?>
<?
    return;
endif;?>

<?php if (count($arResult["ERRORS"]) > 0):
    foreach ($arResult["ERRORS"] as $key => $error)
        if (intval($key) == 0 && $key !== 0)
            $arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;" . Loc::getMessage("REGISTER_FIELD_" . $key) . "&quot;", $error);?>
    <div class="form-cmn flex_stretch_between">
        <?ShowError(implode("<br />", $arResult["ERRORS"]));?>
    </div>
<?php endif;?>
<div class="form-cmn flex_stretch_between">
    <div class="flex50">
        <form method="post" action="<?=POST_FORM_ACTION_URI?>" name="regform" enctype="multipart/form-data">
            <?if($arResult["BACKURL"] <> ''):?>
                <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
            <?endif;?>
            <div class="form-cmn-row flex_center_start">
                <label for="" class="form-cmn-label"><?=Loc::getMessage('REGISTER_FIELD_LOGIN')?>*</label>
                <input type="text" name="REGISTER[LOGIN]" class="input_style" value="<?=$arResult["VALUES"]["LOGIN"];?>">
            </div>
            <div class="form-cmn-row flex_center_start">
                <label for="" class="form-cmn-label"><?=Loc::getMessage('REGISTER_FIELD_EMAIL')?>*</label>
                <input type="text" name="REGISTER[EMAIL]" class="input_style" value="<?=$arResult["VALUES"]["EMAIL"];?>">
            </div>
            <div class="form-cmn-row flex_center_start">
                <label for="" class="form-cmn-label"><?=Loc::getMessage('REGISTER_FIELD_PASSWORD')?>*</label>
                <input type="password" name="REGISTER[PASSWORD]" class="input_style" value="<?=$arResult["VALUES"]["PASSWORD"];?>">
            </div>
            <div class="form-cmn-row flex_center_start">
                <label for="" class="form-cmn-label"><?=Loc::getMessage('REGISTER_FIELD_CONFIRM_PASSWORD')?>*</label>
                <input type="password" name="REGISTER[CONFIRM_PASSWORD]" class="input_style" value="<?=$arResult["VALUES"]["CONFIRM_PASSWORD"];?>">
            </div>
            <div class="form-cmn-btn flex_center_start">
                <button class="style_btn3" type="submit" name="register_submit_button" value="<?=Loc::getMessage("REGISTER_REGISTER_BTN")?>"><?=Loc::getMessage("REGISTER_REGISTER_BTN")?></button>
            </div>
        </form>
    </div>
</div>
