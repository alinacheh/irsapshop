<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
?>
<div class="form-cmn flex_stretch_between pb">
    <h2>Личные данные</h2>
</div>
<?php if ($arResult["strProfileError"] || $arResult['DATA_SAVED'] == 'Y'):?>
    <div class="form-cmn flex_stretch_between">
        <?ShowError($arResult["strProfileError"]);
        if ($arResult['DATA_SAVED'] == 'Y') {
            ShowNote(Loc::getMessage('PROFILE_DATA_SAVED'));
        }?>
    </div>
<?php endif;?>
<div class="form-cmn flex_stretch_between">
    <div class="flex50">
        <form method="post" name="form1" action="<?=POST_FORM_ACTION_URI?>" enctype="multipart/form-data" role="form">
            <?=$arResult["BX_SESSION_CHECK"]?>
            <input type="hidden" name="lang" value="<?=LANG?>" />
            <input type="hidden" name="ID" value="<?=$arResult["ID"]?>" />
            <input type="hidden" name="LOGIN" value="<?=$arResult["arUser"]["LOGIN"]?>" />

            <div class="form-cmn-row flex_center_start">
                <label for="" class="form-cmn-label"><?=Loc::getMessage('NAME')?></label>
                <input type="text" name="NAME" class="input_style" value="<?=$arResult["arUser"]["NAME"]?>">
            </div>
            <div class="form-cmn-row flex_center_start">
                <label for="" class="form-cmn-label"><?=Loc::getMessage('EMAIL')?></label>
                <input type="text" name="EMAIL" class="input_style" value="<?=$arResult["arUser"]["EMAIL"]?>">
            </div>
            <div class="form-cmn-row flex_center_start">
                <label for="" class="form-cmn-label"><?=Loc::getMessage('NEW_PASSWORD_REQ')?></label>
                <input type="password" name="NEW_PASSWORD" class="input_style" value="" autocomplete="off">
            </div>
            <div class="form-cmn-row flex_center_start">
                <label for="" class="form-cmn-label"><?=Loc::getMessage('NEW_PASSWORD_CONFIRM')?></label>
                <input type="password" name="NEW_PASSWORD_CONFIRM" class="input_style" value="" autocomplete="off">
            </div>
            <div class="form-cmn-btn flex_center_start">
                <button class="style_btn3" type="submit" name="save" value="<?=(($arResult["ID"]>0) ? Loc::getMessage("MAIN_SAVE") : Loc::getMessage("MAIN_ADD"))?>"><?=(($arResult["ID"]>0) ? Loc::getMessage("MAIN_SAVE") : Loc::getMessage("MAIN_ADD"))?></button>
            </div>
        </form>
    </div>
</div>