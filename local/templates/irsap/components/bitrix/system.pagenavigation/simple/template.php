<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$this->createFrame()->begin("Загрузка навигации");

if($arResult["NavPageCount"] > 1):?>
    <div class="nav-wrap">
        <div class="news-list-nav">
            <?if($arResult["NAV"]["PAGE_NUMBER"] > 1):?>
                <div class="nav-item">
                    <a href="<?=$arResult["NAV"]["URL"]["PREV_PAGE"];?>" class="num__link prev pren">
                        <i class="fa fa-angle-double-left" aria-hidden="true"></i>
                    </a>
                </div>
            <?endif;?>
            <?if($arResult["NAV"]["START_PAGE"] > 1):?>
                <div class="nav-item num">
                    <a href="<?=$arResult["NAV"]["URL"]["FIRST_PAGE"];?>" class="num__link">1</a>
                </div>
                <div class="nav-item"><span class="num__link">...</span></div>
            <?endif;?>
            <?for($PAGE_NUMBER=$arResult["NAV"]["START_PAGE"]; $PAGE_NUMBER<=$arResult["NAV"]["END_PAGE"]; $PAGE_NUMBER++):?>
                <div class="nav-item num">
                    <a href="<?=$arResult["NAV"]["URL"]["SOME_PAGE"][$PAGE_NUMBER];?>" class="num__link<?if($arResult["NAV"]["PAGE_NUMBER"] == $PAGE_NUMBER):?> active<?endif;?>"><?=$PAGE_NUMBER;?></a>
                </div>
            <?endfor;?>
            <?if($arResult["NAV"]["END_PAGE"] < $arResult["NAV"]["PAGE_COUNT"]):?>
                <div class="nav-item"><span class="num__link">...</span></div>
                <div class="nav-item num">
                    <a href="<?=$arResult["NAV"]["URL"]["LAST_PAGE"];?>" class="num__link"><?=$arResult["NAV"]["PAGE_COUNT"];?></a>
                </div>
            <?endif;?>
            <?if($arResult["NAV"]["PAGE_NUMBER"] < $arResult["NAV"]["PAGE_COUNT"]):?>
                <div class="nav-item">
                    <a href="<?=$arResult["NAV"]["URL"]["NEXT_PAGE"];?>" class="num__link next pren">
                        <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                    </a>
                </div>
            <?endif;?>
        </div>
    </div>
<?endif?>