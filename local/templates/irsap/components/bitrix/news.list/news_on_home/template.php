<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (empty($arResult["ITEMS"])) {
    return;
}
?>
<section class="popular-index news-index">
    <div class="container">
        <h2 class="section-product"><span>Новости</span></h2>
        <div class="slider-product">
            <?foreach ($arResult["ITEMS"] as $item):?>
                <div class="slider-product__item">
                    <div class="slider-product__top">
                        <img
                            src="<?=$item["PREVIEW_PICTURE"]["SRC"]?>"
                            alt="<?=$item["PREVIEW_PICTURE"]["ALT"]?>"
                            title="<?=$item["PREVIEW_PICTURE"]["TITLE"]?>"
                        >
                    </div>
                    <div class="slider-product__bottom">
                        <a href="<?=$item['DETAIL_PAGE_URL'];?>" class="slider-product__name"><?=$item['NAME'];?></a>
                        <div class="news-index__data"><?=$item["DISPLAY_ACTIVE_FROM"]?></div>
                        <div class="slider-product__desc"><?=$item['PREVIEW_TEXT'];?></div>
                        <div class="price-buy">
                            <a href="<?=$item['DETAIL_PAGE_URL'];?>" class="style_btn4">Подробнее</a>
                        </div>
                    </div>
                </div>
            <?endforeach;?>
        </div>
    </div>
</section>
