<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (empty($arResult["ITEMS"])) {
    return;
}
?><section class="main-slider">
    <?foreach($arResult["ITEMS"] as $arItem):

        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

        $headerCss = $buttonCss = [];

        if ($headerColor = $arItem['PROPERTIES']['HEADER_COLOR']['VALUE']) {
            $headerCss[] = "color:{$headerColor}";
        }

        if ($buttonColor = $arItem['PROPERTIES']['BTN_COLOR']['VALUE']) {
            $buttonCss[] = "color:{$buttonColor}";
            $buttonCss[] = "border-color:{$buttonColor}";
        }

    ?>
        <div id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="main-slider__item">
            <img
                src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
                alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
                title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
            >
            <div class="main-slider__desc">
                <div class="main-slider__text" style="<?=implode(';', $headerCss);?>"><?=nl2br($arItem["PROPERTIES"]["HEADER_TEXT"]["VALUE"]);?></div>
                <div class="main-slider__btn">
                    <a href="<?=$arItem["PROPERTIES"]["LINK"]["VALUE"];?>" class="main-slider__link style_btn" style="<?=implode(';', $buttonCss);?>"><?=$arItem["PROPERTIES"]["BTN_TITLE"]["VALUE"] ?: 'Смотреть';?></a>
                </div>
            </div>
        </div>
    <?endforeach;?>
</section>

<script>
    $(".main-slider").bxSlider();
</script>
