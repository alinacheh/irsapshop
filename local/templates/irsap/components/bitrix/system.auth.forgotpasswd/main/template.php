<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
?>
<div class="form-cmn flex_stretch_between pb">
    <h2><?=Loc::getMessage("AUTH_FORGOT_TITLE") ?></h2>
</div>
<?php if (!empty($_POST)):?>
    <div class="form-cmn flex_stretch_between">
        <?ShowMessage($arParams["~AUTH_RESULT"]);?>
    </div>
<?php endif;?>
<div class="form-cmn flex_stretch_between">
    <div class="flex50">
        <form name="bform" id="forgot_password_form" method="post" target="_top" action="<?=SITE_DIR?>auth/forgot-password/">
            <?if (strlen($arResult["BACKURL"]) > 0):?>
                <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
            <?endif;?>

            <input type="hidden" name="AUTH_FORM" value="Y">
            <input type="hidden" name="TYPE" value="SEND_PWD">

            <div class="form-cmn-row flex_center_start">
                <label for="" class="form-cmn-label"><?=Loc::getMessage('AUTH_LOGIN')?></label>
                <input type="text" name="USER_LOGIN" class="input_style" value="<?=$arResult["LAST_LOGIN"];?>">
            </div>

            <div class="form-cmn-row flex_center_start">
                <label for="" class="form-cmn-label"><?=Loc::getMessage('AUTH_OR')?> <?=Loc::getMessage('AUTH_EMAIL')?></label>
                <input type="text" name="USER_EMAIL" class="input_style" value="">
            </div>

            <div class="form-cmn-btn flex_center_start">
                <button class="style_btn3" type="submit" name="send_account_info" value="<?=Loc::getMessage("AUTH_SEND")?>"><?=Loc::getMessage("AUTH_SEND")?></button>
            </div>
        </form>
    </div>
</div>
