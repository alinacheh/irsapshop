<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/** Irsap customs */
$arResult['CUSTOM']['GALLERY'] = AppMainHelper::getGalleryForItem($arResult, 'MORE_PHOTO', false, [
    'THUMB' => [
        'WIDTH' => 200,
        'HEIGHT' => 200,
    ],
]);
