$(document).on('submit', '.b-subscribe__form', function (e) {
    e.preventDefault();
    var form = $(this);
    $.ajax({
        url: '/local/ajax/subscribe.php',
        type: 'post',
        data: form.serialize(),
        dataType: 'json',
        success: function(data) {
            if(data['status'] === true) {
                form.find('.-error').html('');
                form.find('.-error').hide();
                form.trigger('reset');
				//form.find('.-success').html(data['message']);
				//form.find('.-success').show();
				$("#subscribe_ok").show();
				$(".overlay").show();
            } else {
                form.find('.-error').html(data['errors']);
                form.find('.-error').show();
            }
        }});		
});
$(document).on('click', '[data-entity="subscribe-window-close"]', function (e) {
	e.preventDefault();
	$("#subscribe_ok").hide();
	$(".overlay").hide();
});