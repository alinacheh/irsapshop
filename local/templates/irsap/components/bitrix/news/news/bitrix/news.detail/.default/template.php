<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$this->setFrameMode(true);
?>
<div>
    <div class="news-content__title"><?=$arResult['NAME'];?></div>
    <div class="news-content__box">
        <div class="news-content__image">
            <?if ($arParams['DISPLAY_DATE'] != 'N' && $arResult['DISPLAY_ACTIVE_FROM']):?>
                <span class="news-content__date date"><?=strtolower($arResult['DISPLAY_ACTIVE_FROM']);?></span>
            <?endif;?>
            <?if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arResult["DETAIL_PICTURE"])):?>
                <img
                    class="news-image"
                    src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>"
                    alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>"
                    title="<?=$arResult["DETAIL_PICTURE"]["TITLE"]?>"
                />
            <?endif?>
        </div>
        <div class="news-content__text">
            <?if (strlen($arResult["DETAIL_TEXT"]) > 0):?>
                <?if ($arResult['DETAIL_TEXT_TYPE'] == 'text'):?>
                    <p><?=$arResult['DETAIL_TEXT'];?></p>
                <?else:?>
                    <?=$arResult['DETAIL_TEXT'];?>
                <?endif;?>
            <?endif;?>
        </div>
        <?if (!empty($arResult['CUSTOM']['GALLERY'])):?>
            <div class="news-content__gallery">
                <div class="gallery">
                    <?foreach ($arResult['CUSTOM']['GALLERY'] as $arItem):?>
                        <a data-fancybox="news-item-gallery" href="<?=$arItem['SRC'];?>" class="gallery-flex">
                            <img src="<?=$arItem['THUMB']['SRC'];?>" alt="<?=$arItem['THUMB']['DESCRIPTION'];?>" class="gallery-photo__item">
                        </a>
                    <?endforeach;?>
                </div>
            </div>
        <?endif;?>
    </div>
    <a href="<?=$arResult['LIST_PAGE_URL'];?>" class="news-body__link"><i class="fa fa-long-arrow-left" aria-hidden="true"></i><?=Loc::getMessage('BND_BACK_TO_LIST');?></a>
</div>