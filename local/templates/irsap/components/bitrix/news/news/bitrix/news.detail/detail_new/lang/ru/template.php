<?
$MESS['BND_BACK_TO_LIST'] = 'К списку новостей';
$MESS['COLLECTION_TITLE'] = 'Обратите внимание на наши коллекции:';
$MESS['SUBSCRIBE_TITLE1'] = 'Хотите знать больше об Irsap и акциях?';
$MESS['SUBSCRIBE_TITLE2'] = 'Оставляйте ваш email, мы расскажем';
$MESS['SUBSCRIBE_BUTTON'] = 'Подписаться';

$MESS['WHATSAPP_TITLE'] = 'Появились вопросы? Нужна консультация?';
$MESS['WHATSAPP_BUTTON'] = 'Напишите нам';
?>