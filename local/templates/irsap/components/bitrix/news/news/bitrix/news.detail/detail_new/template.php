<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$this->setFrameMode(true);
?>
<?ob_start();?>
	<div class="whatsapp-block">	
		<div class="container_new">	
			<div class="b-whatsapp">		
				<span class="bold uppercase type-title whatsapp-title"><?=Loc::getMessage('WHATSAPP_TITLE');?></span>			
				<a href="https://wa.me/79037203651" target="_blank" class="b-textfield__input whatsapp-input"
				onclick="ym(62433241,'reachGoal','whatsapp-blog'); return true;"
				alt="Написать в Whatsapp" title="Написать в Whatsapp"><img src="/local/templates/irsap/images/w_icon.png" class="w-icon"/><?=Loc::getMessage('WHATSAPP_BUTTON');?></a>
				
			</div>	
		</div>
	</div>
<?
$whatsapp = ob_get_contents();
ob_end_clean();
?>
	
	<?ob_start();?>
		<div class="subscribe-block">	
		<div class="container_new">	
			<div class="b-subscribe">		
				<span class="bold uppercase type-title subscribe-title"><?=Loc::getMessage('SUBSCRIBE_TITLE1');?></span>
				<span class="bold uppercase type-title subscribe-title"><?=Loc::getMessage('SUBSCRIBE_TITLE2');?></span>
			</div>
			<div class="b-subscribe b-subscribe2">
				<div class="b-social__title">
					<form action="#" method="post" class="b-form b-subscribe__form">
						<input type="email" class="b-textfield__input subscribe-input js-textfield__inputmask" autocomplete="off" name="sub_email"
						placeholder="Ваш email*" required="">
						<button class="b-button-s" type="submit"><?=Loc::getMessage('SUBSCRIBE_BUTTON');?></button>
						<div class="b-form__result -error"></div>
						<div class="b-form__result -success"></div>
						
					</form>
				</div>
			</div>	
		</div>
	</div>
<?
$subscribe = ob_get_contents();
ob_end_clean();
?>
<?ob_start();?>
	<?if (!empty($arResult['CUSTOM']['GALLERY'])):?>
		<div class="news-content__gallery">
			<div class="gallery">
				<?foreach ($arResult['CUSTOM']['GALLERY'] as $arItem):?>
					<a data-fancybox="news-item-gallery" href="<?=$arItem['SRC'];?>" class="gallery-flex">
						<img src="<?=$arItem['THUMB']['SRC'];?>" alt="<?=$arItem['THUMB']['DESCRIPTION'];?>" class="gallery-photo__item">
					</a>
				<?endforeach;?>
			</div>
		</div>
	<?endif;?>
<?
$images = ob_get_contents();
ob_end_clean();
?>
	
	
    <div class="news-content__title"><?=$arResult['NAME'];?></div>
    <div class="news-content__box">
        <div class="news-content__image">
            <?if ($arParams['DISPLAY_DATE'] != 'N' && $arResult['DISPLAY_ACTIVE_FROM']):?>
                <span class="news-content__date date"><?=strtolower($arResult['DISPLAY_ACTIVE_FROM']);?></span>
            <?endif;?>
            <?if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arResult["DETAIL_PICTURE"])):?>
                <img
                    class="news-image"
                    src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>"
                    alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>"
                    title="<?=$arResult["DETAIL_PICTURE"]["TITLE"]?>"
                />
            <?endif?>
        </div>
        <div class="news-content__text">
            <?if (strlen($arResult["DETAIL_TEXT"]) > 0):
			 $count_images = substr_count($arResult['DETAIL_TEXT'], "<!--images-->");
			 $arResult['DETAIL_TEXT'] = str_replace("<!--whatsapp-->", $whatsapp, $arResult['DETAIL_TEXT']);
			 $arResult['DETAIL_TEXT'] = str_replace("<!--subscribe-->", $subscribe, $arResult['DETAIL_TEXT']);
			 $arResult['DETAIL_TEXT'] = str_replace("<!--images-->", $images, $arResult['DETAIL_TEXT']);			
			?>
                <?if ($arResult['DETAIL_TEXT_TYPE'] == 'text'):?>
                    <p><?=$arResult['DETAIL_TEXT'];?></p>
                <?else:?>
                   <?=$arResult['DETAIL_TEXT'];?>
                <?endif;?>
            <?endif;?>
        </div>
         <?if (!empty($arResult['CUSTOM']['GALLERY']) && $count_images == 0):?>
            <div class="news-content__gallery">
                <div class="gallery">
                    <?foreach ($arResult['CUSTOM']['GALLERY'] as $arItem):?>
                        <a data-fancybox="news-item-gallery" href="<?=$arItem['SRC'];?>" class="gallery-flex">
                            <img src="<?=$arItem['THUMB']['SRC'];?>" alt="<?=$arItem['THUMB']['DESCRIPTION'];?>" class="gallery-photo__item">
                        </a>
                    <?endforeach;?>
                </div>
            </div>
        <?endif;?>
    </div>
	
   
