<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if (!count($arResult['ITEMS'])) {
    return;
}?>

<section class="news-list">
    <div class="container">
        <?if($arParams["DISPLAY_TOP_PAGER"]):?>
            <?=$arResult["NAV_STRING"]?>
        <?endif;?>
        <div class="news-list-wrap">
            <?foreach($arResult["ITEMS"] as $arItem):?>
            <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
                <div id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="news-list-box">
                    <div class="news-list-box__item flex49 hidden-img__des">
						<a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
							<img
								class="news-list__img"
								src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
								alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
								title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
							/>
						</a>
                    </div>
                    <div class="news-list-box__item flex49 info-block">
                        <div class="news-list-box__info">
                            <div class="news-list__title"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem['PROPERTIES']['NAME_FOR_LIST']['VALUE'];?></a></div>
                            <?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
                                <span class="date"><?=strtolower($arItem["DISPLAY_ACTIVE_FROM"])?></span>
                            <?endif;?>
                            <img
                                class="news-list__img hidden-img__mob"
                                src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
                                alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
                                title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
                            />
                            <p class="news-list-box__text"><?=$arItem['PREVIEW_TEXT'];?></p>
                        </div>
                        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="news-list__link common-link"><?=Loc::getMessage('CT_BNL_SEE_FULL');?></a>
                    </div>
                </div>
            <?endforeach;?>
        </div>
        <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
            <?=$arResult["NAV_STRING"]?>
        <?endif;?>
    </div>
</section>
