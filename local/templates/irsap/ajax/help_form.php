<?

?>
<?$APPLICATION->IncludeComponent(
	"bitrix:form",
	"help",
	array(
		"AJAX_MODE" => "Y",
		"SEF_MODE" => "N",
		"WEB_FORM_ID" => 2,
		"RESULT_ID" => $_REQUEST["RESULT_ID"],
		"START_PAGE" => "new",
		"SHOW_LIST_PAGE" => "N",
		"SHOW_EDIT_PAGE" => "N",
		"SHOW_VIEW_PAGE" => "N",
		"SUCCESS_URL" => "",
		"SUCCESS_TEXT" => "Ваше сообщение успешно отправлено. Наши менеджеры ответят Вам в ближайшее время.",
		"SHOW_ANSWER_VALUE" => "Y",
		"SHOW_ADDITIONAL" => "Y",
		"SHOW_STATUS" => "Y",
		"EDIT_ADDITIONAL" => "Y",
		"EDIT_STATUS" => "Y",
		"NOT_SHOW_FILTER" => array(
			0 => "",
			1 => "",
		),
		"NOT_SHOW_TABLE" => array(
			0 => "",
			1 => "",
		),
		"CHAIN_ITEM_TEXT" => "",
		"CHAIN_ITEM_LINK" => "",
		"IGNORE_CUSTOM_TEMPLATE" => "Y",
		"USE_EXTENDED_ERRORS" => "Y",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"SEF_FOLDER" => "/communication/web-forms/",
		"AJAX_OPTION_ADDITIONAL" => "",
		"VARIABLE_ALIASES" => array(
			"action" => "action",
		),
		"PRIVACY_LINK" => "/privacy/",
		"TARGET_URL" => $_REQUEST["TARGET_URL"],
	),
	false
);?>
<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");