<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line","mobile",Array(
        "HIDE_ON_BASKET_PAGES" => "Y",
        "PATH_TO_BASKET" => SITE_DIR."personal/cart/",
        "SHOW_AUTHOR" => "Y",
        "SHOW_DELAY" => "N",
        "SHOW_EMPTY_VALUES" => "Y",
        "SHOW_IMAGE" => "N",
        "SHOW_NOTAVAIL" => "N",
        "SHOW_NUM_PRODUCTS" => "N",
        "SHOW_PERSONAL_LINK" => "N",
        "SHOW_PRICE" => "N",
        "SHOW_PRODUCTS" => "N",
        "SHOW_SUMMARY" => "Y",
        "SHOW_TOTAL_PRICE" => "N"
    )
);?>