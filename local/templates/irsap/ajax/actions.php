<?php

define('BX_SECURITY_SESSION_READONLY', true);
define("PUBLIC_AJAX_MODE", true);
define("NO_KEEP_STATISTIC", "Y");
define("NO_AGENT_STATISTIC","Y");
define("NO_AGENT_CHECK", true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use App\Support\AjaxActionsController;

header('Content-Type: application/json');

$context = Bitrix\Main\Application::getInstance()->getContext();
$request = $context->getRequest();

$controller = new AjaxActionsController;
$action = $request->get('action');

if (method_exists($controller, $action)) {
    $response = call_user_func([$controller, $action], $request);
} else {
    $response = [
        'data' => null,
        'successful' => false,
        'errors' => [
            [
                'code' => '',
                'message' => 'Action is not supported'
            ]
        ]
    ];
}

echo json_encode($response);