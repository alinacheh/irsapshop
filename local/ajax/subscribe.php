<?
if (!(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')) {
    header('HTTP/1.0 403 Forbidden');
    return;
}

define('NO_KEEP_STATISTIC', true);
define('NOT_CHECK_FILE_PERMISSIONS', true);
define('DisableEventsCheck', true);
define('AJAX_REQUEST', "Y");
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
header('Content-Type: application/json; charset=utf-8');

use \Bitrix\Main\Loader;
use \Bitrix\Main\Web\Json;
use \Bitrix\Main\Type\DateTime;
use \Bitrix\Main\Application;

//subscribe.php
$request = Application::getInstance()->getContext()->getRequest();
$post = $request->getPostList()->toArray();
$arOut = array('status' => false);

if(!empty($post) && strlen($post['sub_email']) > 0 && CModule::IncludeModule('subscribe')) {

    $email = $post['sub_email'];
    $arRubId = ['1'];
    $format = 'html';

    global $USER;
    $arFields = Array(
        "USER_ID" => ($USER->IsAuthorized()? $USER->GetID():false),
        "FORMAT" => ($format <> "html"? "text":"html"),
        "EMAIL" => $email,
        "ACTIVE" => "Y",
        //"SEND_CONFIRM" => "N",
        "CONFIRMED" => "Y",
        "RUB_ID" => $arRubId
    );
    $subscr = new CSubscription;

    $ID = $subscr->Add($arFields);
    if($ID>0) {
        CSubscription::Authorize($ID);
        $arOut['status'] = true;
        //$arOut['message'] = "На Ваш e-mail выслано письмо. Для подтверждения подписки перейдите по указанной в нем ссылке.";
        $arOut['message'] = "Ваш e-mail добавлен. Спасибо.";
    }else {
        $arOut['errors'] = $subscr->LAST_ERROR;
    }
}

echo Json::encode($arOut);
die();