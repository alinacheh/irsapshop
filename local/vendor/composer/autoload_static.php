<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit572344b1ce0c554ebd198cc110cd75b8
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/php_interface/lib/app',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit572344b1ce0c554ebd198cc110cd75b8::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit572344b1ce0c554ebd198cc110cd75b8::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
