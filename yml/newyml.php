<? header("Content-Type: text/xml; charset=windows-1251");?><? echo "<"."?xml version=\"1.0\" encoding=\"windows-1251\"?".">"?>
<!DOCTYPE yml_catalog SYSTEM "shops.dtd">
<yml_catalog date="2020-11-25 11:53">
<shop>
<name>irsapshop</name>
<company>irsapshop.ru</company>
<url>https://irsapshop.ru</url>
<currencies>
<currency id="RUB" rate="1"/>
</currencies>
<delivery-options>
<option cost="700" days="1-2"/>
</delivery-options>
<categories>
<category id="369">Tesi</category>
<category id="1" parentId="369">Tesi 2</category>
<category id="30" parentId="1">1000 �������</category>
<category id="31" parentId="1">1000 ������</category>
<category id="32" parentId="1">1200 �������</category>
<category id="33" parentId="1">1200 ������</category>
<category id="34" parentId="1">1500 �������</category>
<category id="35" parentId="1">1500 ������</category>
<category id="2" parentId="1">1800 �������</category>
<category id="3" parentId="1">1800 ������</category>
<category id="390" parentId="1">1800 ������ ���������</category>
<category id="339" parentId="1">2000 �������</category>
<category id="340" parentId="1">2000 ������</category>
<category id="36" parentId="1">2200 �������</category>
<category id="37" parentId="1">2200 ������</category>
<category id="38" parentId="1">2500 �������</category>
<category id="39" parentId="1">2500 ������</category>
<category id="8" parentId="1">260 �������</category>
<category id="9" parentId="1">260 ������</category>
<category id="10" parentId="1">300 �������</category>
<category id="11" parentId="1">300 ������</category>
<category id="12" parentId="1">350 �������</category>
<category id="13" parentId="1">350 ������</category>
<category id="4" parentId="1">365 �������</category>
<category id="5" parentId="1">365 ������</category>
<category id="14" parentId="1">400 �������</category>
<category id="15" parentId="1">400 ������</category>
<category id="16" parentId="1">450 �������</category>
<category id="17" parentId="1">450 ������</category>
<category id="18" parentId="1">500 �������</category>
<category id="19" parentId="1">500 ������</category>
<category id="20" parentId="1">550 �������</category>
<category id="21" parentId="1">550 ������</category>
<category id="6" parentId="1">565 �������</category>
<category id="7" parentId="1">565 ������</category>
<category id="22" parentId="1">600 �������</category>
<category id="23" parentId="1">600 ������</category>
<category id="24" parentId="1">650 �������</category>
<category id="25" parentId="1">650 ������</category>
<category id="26" parentId="1">750 �������</category>
<category id="27" parentId="1">750 ������</category>
<category id="28" parentId="1">900 �������</category>
<category id="29" parentId="1">900 ������</category>
<category id="40" parentId="369">Tesi 3</category>
<category id="67" parentId="40">1000 �������</category>
<category id="68" parentId="40">1000 ������</category>
<category id="69" parentId="40">1200 �������</category>
<category id="70" parentId="40">1200 ������</category>
<category id="71" parentId="40">1500 �������</category>
<category id="72" parentId="40">1500 ������</category>
<category id="73" parentId="40">1800 �������</category>
<category id="74" parentId="40">1800 ������</category>
<category id="75" parentId="40">2000 �������</category>
<category id="76" parentId="40">2000 ������</category>
<category id="77" parentId="40">2200 �������</category>
<category id="78" parentId="40">2200 ������</category>
<category id="79" parentId="40">2500 �������</category>
<category id="80" parentId="40">2500 ������</category>
<category id="41" parentId="40">260 �������</category>
<category id="42" parentId="40">260 ������</category>
<category id="43" parentId="40">300 �������</category>
<category id="44" parentId="40">300 ������</category>
<category id="45" parentId="40">350 �������</category>
<category id="46" parentId="40">350 ������</category>
<category id="47" parentId="40">365 �������</category>
<category id="48" parentId="40">365 ������</category>
<category id="49" parentId="40">400 �������</category>
<category id="50" parentId="40">400 ������</category>
<category id="51" parentId="40">450 �������</category>
<category id="52" parentId="40">450 ������</category>
<category id="53" parentId="40">500 �������</category>
<category id="54" parentId="40">500 ������</category>
<category id="55" parentId="40">550 �������</category>
<category id="56" parentId="40">550 ������</category>
<category id="57" parentId="40">565 �������</category>
<category id="58" parentId="40">565 ������</category>
<category id="59" parentId="40">600 �������</category>
<category id="60" parentId="40">600 ������</category>
<category id="61" parentId="40">650 �������</category>
<category id="62" parentId="40">650 ������</category>
<category id="63" parentId="40">750 �������</category>
<category id="64" parentId="40">750 ������</category>
<category id="65" parentId="40">900 �������</category>
<category id="66" parentId="40">900 ������</category>
<category id="81" parentId="369">Tesi 4</category>
<category id="110" parentId="81">1200 �������</category>
<category id="111" parentId="81">1200 ������</category>
<category id="106" parentId="81">1500 �������</category>
<category id="107" parentId="81">1500 ������</category>
<category id="112" parentId="81">1800 �������</category>
<category id="113" parentId="81">1800 ������</category>
<category id="82" parentId="81">200 �������</category>
<category id="83" parentId="81">200 ������</category>
<category id="114" parentId="81">2000 �������</category>
<category id="115" parentId="81">2000 ������</category>
<category id="116" parentId="81">2200 �������</category>
<category id="117" parentId="81">2200 ������</category>
<category id="118" parentId="81">2500 �������</category>
<category id="119" parentId="81">2500 ������</category>
<category id="84" parentId="81">260 �������</category>
<category id="85" parentId="81">260 ������</category>
<category id="86" parentId="81">350 �������</category>
<category id="87" parentId="81">350 ������</category>
<category id="88" parentId="81">365 �������</category>
<category id="89" parentId="81">365 ������</category>
<category id="90" parentId="81">400 �������</category>
<category id="91" parentId="81">400 ������</category>
<category id="92" parentId="81">450 �������</category>
<category id="93" parentId="81">450 ������</category>
<category id="94" parentId="81">500 �������</category>
<category id="95" parentId="81">500 ������</category>
<category id="96" parentId="81">550 �������</category>
<category id="97" parentId="81">550 ������</category>
<category id="98" parentId="81">565 �������</category>
<category id="99" parentId="81">565 ������</category>
<category id="100" parentId="81">600 �������</category>
<category id="101" parentId="81">600 ������</category>
<category id="102" parentId="81">650 �������</category>
<category id="103" parentId="81">650 ������</category>
<category id="108" parentId="81">750 �������</category>
<category id="109" parentId="81">750 ������</category>
<category id="104" parentId="81">900 �������</category>
<category id="105" parentId="81">900 ������</category>
<category id="120" parentId="369">Tesi 5</category>
<category id="147" parentId="120">1000 �������</category>
<category id="148" parentId="120">1000 ������</category>
<category id="149" parentId="120">1200 �������</category>
<category id="150" parentId="120">1200 ������</category>
<category id="151" parentId="120">1500 �������</category>
<category id="152" parentId="120">1500 ������</category>
<category id="159" parentId="120">1800 �������</category>
<category id="160" parentId="120">1800 ������</category>
<category id="121" parentId="120">200 �������</category>
<category id="122" parentId="120">200 ������</category>
<category id="172" parentId="120">2000 �������</category>
<category id="173" parentId="120">2000 ������</category>
<category id="174" parentId="120">2200 �������</category>
<category id="175" parentId="120">2200 ������</category>
<category id="176" parentId="120">2500 �������</category>
<category id="177" parentId="120">2500 ������</category>
<category id="123" parentId="120">260 �������</category>
<category id="124" parentId="120">260 ������</category>
<category id="125" parentId="120">350 �������</category>
<category id="126" parentId="120">350 ������</category>
<category id="127" parentId="120">365 �������</category>
<category id="128" parentId="120">365 ������</category>
<category id="129" parentId="120">400 �������</category>
<category id="130" parentId="120">400 ������</category>
<category id="131" parentId="120">450 �������</category>
<category id="132" parentId="120">450 ������</category>
<category id="133" parentId="120">500 �������</category>
<category id="134" parentId="120">500 ������</category>
<category id="135" parentId="120">550 �������</category>
<category id="136" parentId="120">550 ������</category>
<category id="137" parentId="120">565 �������</category>
<category id="138" parentId="120">565 ������</category>
<category id="139" parentId="120">600 �������</category>
<category id="140" parentId="120">600 ������</category>
<category id="141" parentId="120">650 �������</category>
<category id="142" parentId="120">650 ������</category>
<category id="143" parentId="120">750 �������</category>
<category id="144" parentId="120">750 ������</category>
<category id="145" parentId="120">900 �������</category>
<category id="146" parentId="120">900 ������</category>
<category id="196" parentId="369">Tesi 2 Clean</category>
<category id="199" parentId="196"> 350 �������</category>
<category id="209" parentId="196">1000 �������</category>
<category id="210" parentId="196">1200 �������</category>
<category id="211" parentId="196">1500 �������</category>
<category id="212" parentId="196">1800 �������</category>
<category id="213" parentId="196">2000 �������</category>
<category id="214" parentId="196">2200 �������</category>
<category id="215" parentId="196">2500 �������</category>
<category id="198" parentId="196">300 �������</category>
<category id="200" parentId="196">365 �������</category>
<category id="201" parentId="196">400 �������</category>
<category id="202" parentId="196">450 �������</category>
<category id="203" parentId="196">500 �������</category>
<category id="204" parentId="196">550 �������</category>
<category id="205" parentId="196">600 �������</category>
<category id="206" parentId="196">650 �������</category>
<category id="207" parentId="196">750 �������</category>
<category id="208" parentId="196">900 �������</category>
<category id="337" parentId="369">Tesi 3 Bench ��������������</category>
<category id="216" parentId="369">Tesi 3 Clean</category>
<category id="228" parentId="216">1000 �������</category>
<category id="229" parentId="216">1200 �������</category>
<category id="241" parentId="216">1500 �������</category>
<category id="242" parentId="216">1800 �������</category>
<category id="243" parentId="216">2000 �������</category>
<category id="244" parentId="216">2200 �������</category>
<category id="245" parentId="216">2500 �������</category>
<category id="217" parentId="216">300 �������</category>
<category id="218" parentId="216">350 �������</category>
<category id="219" parentId="216">365 �������</category>
<category id="220" parentId="216">400 �������</category>
<category id="221" parentId="216">450 �������</category>
<category id="222" parentId="216">500 �������</category>
<category id="223" parentId="216">550 �������</category>
<category id="224" parentId="216">600 �������</category>
<category id="225" parentId="216">650 �������</category>
<category id="226" parentId="216">750 �������</category>
<category id="227" parentId="216">900 �������</category>
<category id="334" parentId="369">Tesi 4 Bench ������������</category>
<category id="338" parentId="369">Tesi 4 Ben�h ��������������</category>
<category id="246" parentId="369">Tesi 4 Clean</category>
<category id="274" parentId="246">1000 �������</category>
<category id="275" parentId="246">1200 �������</category>
<category id="276" parentId="246">1500 �������</category>
<category id="277" parentId="246">1800 �������</category>
<category id="278" parentId="246">2000 �������</category>
<category id="279" parentId="246">2200 �������</category>
<category id="280" parentId="246">2500 �������</category>
<category id="247" parentId="246">300 �������</category>
<category id="248" parentId="246">350 �������</category>
<category id="249" parentId="246">365 �������</category>
<category id="250" parentId="246">400 �������</category>
<category id="251" parentId="246">450 �������</category>
<category id="252" parentId="246">500 �������</category>
<category id="269" parentId="246">550 �������</category>
<category id="270" parentId="246">600 �������</category>
<category id="271" parentId="246">650 �������</category>
<category id="272" parentId="246">750 �������</category>
<category id="273" parentId="246">900 �������</category>
<category id="335" parentId="369">Tesi 5 Bench ������������</category>
<category id="341" parentId="369">Tesi 5 Ben�h �������������� </category>
<category id="281" parentId="369">Tesi 5 Clean</category>
<category id="293" parentId="281">1000 �������</category>
<category id="294" parentId="281">1200 �������</category>
<category id="295" parentId="281">1500 �������</category>
<category id="296" parentId="281">1800 �������</category>
<category id="297" parentId="281">2000 �������</category>
<category id="298" parentId="281">2200 �������</category>
<category id="299" parentId="281">2500 �������</category>
<category id="282" parentId="281">300 �������</category>
<category id="283" parentId="281">350 �������</category>
<category id="284" parentId="281">365 �������</category>
<category id="285" parentId="281">400 �������</category>
<category id="286" parentId="281">450 �������</category>
<category id="287" parentId="281">500 �������</category>
<category id="288" parentId="281">550 �������</category>
<category id="289" parentId="281">600 �������</category>
<category id="290" parentId="281">650 �������</category>
<category id="291" parentId="281">750 �������</category>
<category id="292" parentId="281">900 �������</category>
<category id="184" parentId="369">Tesi 6</category>
<category id="267" parentId="184">1000 �������</category>
<category id="268" parentId="184">1000 ������</category>
<category id="320" parentId="184">1200 �������</category>
<category id="321" parentId="184">1200 ������</category>
<category id="322" parentId="184">1500 �������</category>
<category id="323" parentId="184">1500 ������</category>
<category id="324" parentId="184">1800 �������</category>
<category id="325" parentId="184">1800 ������</category>
<category id="185" parentId="184">200 �������</category>
<category id="186" parentId="184">200 ������</category>
<category id="326" parentId="184">2000 �������</category>
<category id="327" parentId="184">2000 ������</category>
<category id="328" parentId="184">2200 �������</category>
<category id="329" parentId="184">2200 ������</category>
<category id="330" parentId="184">2500 �������</category>
<category id="331" parentId="184">2500 ������</category>
<category id="187" parentId="184">260 �������</category>
<category id="188" parentId="184">260 ������</category>
<category id="189" parentId="184">350 �������</category>
<category id="190" parentId="184">350 ������</category>
<category id="191" parentId="184">365 �������</category>
<category id="192" parentId="184">365 ������</category>
<category id="235" parentId="184">400 �������</category>
<category id="236" parentId="184">400 ������</category>
<category id="237" parentId="184">450 �������</category>
<category id="238" parentId="184">450 ������</category>
<category id="239" parentId="184">500 �������</category>
<category id="240" parentId="184">500 ������</category>
<category id="255" parentId="184">550 �������</category>
<category id="256" parentId="184">550 ������</category>
<category id="257" parentId="184">565 �������</category>
<category id="258" parentId="184">565 ������</category>
<category id="259" parentId="184">600 �������</category>
<category id="260" parentId="184">600 ������</category>
<category id="261" parentId="184">650 �������</category>
<category id="262" parentId="184">650 ������</category>
<category id="263" parentId="184">750 �������</category>
<category id="264" parentId="184">750 ������</category>
<category id="265" parentId="184">900 �������</category>
<category id="266" parentId="184">900 ������</category>
<category id="336" parentId="369">Tesi 6 Bench ������������</category>
<category id="342" parentId="369">Tesi 6 Ben�h �������������� </category>
<category id="300" parentId="369">Tesi 6 Clean</category>
<category id="312" parentId="300">1000 �������</category>
<category id="313" parentId="300">1200 �������</category>
<category id="314" parentId="300">1500 �������</category>
<category id="315" parentId="300">1800 �������</category>
<category id="316" parentId="300">2000 �������</category>
<category id="317" parentId="300">2200 �������</category>
<category id="318" parentId="300">2500 �������</category>
<category id="301" parentId="300">300 �������</category>
<category id="302" parentId="300">350 �������</category>
<category id="303" parentId="300">365 �������</category>
<category id="304" parentId="300">400 �������</category>
<category id="305" parentId="300">450 �������</category>
<category id="306" parentId="300">500 �������</category>
<category id="307" parentId="300">550 �������</category>
<category id="308" parentId="300">600 �������</category>
<category id="309" parentId="300">650 �������</category>
<category id="310" parentId="300">750 �������</category>
<category id="311" parentId="300">900 �������</category>
<category id="333" parentId="369">Tesi Cruise</category>
<category id="319" parentId="369">Tesi Join</category>
<category id="332" parentId="369">Tesi Memory</category>
<category id="194">Ares</category>
<category id="387" parentId="194">Ares</category>
<category id="385" parentId="194">Chrome</category>
<category id="386" parentId="194">White</category>
<category id="232">Bella</category>
<category id="379">Dedalo</category>
<category id="360" parentId="379">Dedalo</category>
<category id="372" parentId="379">Dedalo ���</category>
<category id="367" parentId="379">Dedalo �������������</category>
<category id="377">Face</category>
<category id="343" parentId="377">Face</category>
<category id="345" parentId="377">Face Zero</category>
<category id="393" parentId="377">Face Zero ������������</category>
<category id="346" parentId="377">Face Zero_Air</category>
<category id="394" parentId="377">Face Zero_Air ������������</category>
<category id="392" parentId="377">Face ������������</category>
<category id="344" parentId="377">Face_Air</category>
<category id="391" parentId="377">Face_Air ������������</category>
<category id="183">Filo</category>
<category id="378">Flauto</category>
<category id="169" parentId="378">Flauto</category>
<category id="170" parentId="378">Flauto 2</category>
<category id="376" parentId="378">Flauto �������������</category>
<category id="381">Funky</category>
<category id="161" parentId="381">Funky</category>
<category id="162" parentId="381">Funky Air Mix</category>
<category id="382">Get Up</category>
<category id="153" parentId="382">Get Up</category>
<category id="154" parentId="382">Get Up Air Mix</category>
<category id="383">It is</category>
<category id="358" parentId="383">It is (������������� ������� ������� �����)</category>
<category id="357" parentId="383">It is �������������</category>
<category id="384">Jazz</category>
<category id="155" parentId="384">Jazz</category>
<category id="156" parentId="384">Jazz Air Mix</category>
<category id="168">Kart</category>
<category id="165">Like</category>
<category id="359">M&apos;ama</category>
<category id="234">Minuette</category>
<category id="388" parentId="234">���</category>
<category id="389" parentId="234">�������������</category>
<category id="166">Net</category>
<category id="167">Net Air Mix</category>
<category id="180">Novo</category>
<category id="163">Novo Cult</category>
<category id="375">Novo Cult �������������</category>
<category id="164">Oddo</category>
<category id="374">Orimono</category>
<category id="365">Quadraqua</category>
<category id="371">Quadraqua �������������</category>
<category id="181">Quadre</category>
<category id="380">Relax</category>
<category id="362">Relax Immagina</category>
<category id="182">Rigo</category>
<category id="364">Sequenze</category>
<category id="373">Sequenze ���</category>
<category id="368">Sequenze �������������</category>
<category id="230">Serenade</category>
<category id="157">Soul</category>
<category id="158">Soul Air Mix</category>
<category id="370">Step</category>
<category id="231" parentId="370">Step</category>
<category id="354" parentId="370">Step_B</category>
<category id="356" parentId="370">Step_B (������������� ������� ������� �����) </category>
<category id="355" parentId="370">Step_B (������������� �������)</category>
<category id="366" parentId="370">STEP_E �������������</category>
<category id="351" parentId="370">Step_H</category>
<category id="352" parentId="370">Step_H ������������� �������</category>
<category id="353" parentId="370">Step_H ������������� ������� ������� �����</category>
<category id="350" parentId="370">Step_V</category>
<category id="348" parentId="370">Step_V ������������� �������</category>
<category id="349" parentId="370">Step_V ������������� ������� ������� �����</category>
<category id="233">Tole</category>
<category id="363">Tratto</category>
<category id="193">Vela</category>
<category id="195">Venus</category>
<category id="171">Xilo</category>
<category id="178">Xilo 2</category>
<category id="179">Xilo Air Mix</category>
</categories>
<offers>
<offer id="14" available="true">
<url>https://irsapshop.ru/catalog/tesi-2/#14</url>
<price>10596.63</price>
<currencyId>RUB</currencyId>
<categoryId>2</categoryId>
<vendorCode>RR 2 1800 04 01 A4 02 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/867/86788360104a0cad641a0bd8aa6905a3.png</picture>
<name>Tesi 2 1800 4 �������</name>
<model>Tesi 2 1800 4 �������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">1800</param>
<param name="���������� ������">4</param>
<param name="��� �����������">�������</param>
</offer>
<offer id="15" available="true">
<url>https://irsapshop.ru/catalog/tesi-2/#15</url>
<price>21193.27</price>
<currencyId>RUB</currencyId>
<categoryId>2</categoryId>
<vendorCode>RR 2 1800 08 01 A4 02 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/867/86788360104a0cad641a0bd8aa6905a3.png</picture>
<name>Tesi 2 1800 8 �������</name>
<model>Tesi 2 1800 8 �������</model>
<description>����������� ������������ �������� �� ������ ������ ����������� � �������� ���������, �� � �������� � ������� ������� ��� �����. ��������� �������� ������� �� �����.</description>
<param name="������">1800</param>
<param name="���������� ������">8</param>
<param name="��� �����������">�������</param>
</offer>
<offer id="17" available="true">
<url>https://irsapshop.ru/catalog/tesi-2/#17</url>
<price>15894.95</price>
<currencyId>RUB</currencyId>
<categoryId>2</categoryId>
<vendorCode>RR 2 1800 06 01 A4 02 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/867/86788360104a0cad641a0bd8aa6905a3.png</picture>
<name>Tesi 2 1800 6 �������</name>
<model>Tesi 2 1800 6 �������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">1800</param>
<param name="���������� ������">6</param>
<param name="��� �����������">�������</param>
</offer>
<offer id="96" available="true">
<url>https://irsapshop.ru/catalog/tesi-2/#96</url>
<price>26518.55</price>
<currencyId>RUB</currencyId>
<categoryId>2</categoryId>
<vendorCode>RR 2 1800 10 01 A4 02 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/867/86788360104a0cad641a0bd8aa6905a3.png</picture>
<name>Tesi 2 1800 10 �������</name>
<model>Tesi 2 1800 10 �������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">1800</param>
<param name="���������� ������">10</param>
<param name="��� �����������">�������</param>
</offer>
<offer id="97" available="true">
<url>https://irsapshop.ru/catalog/tesi-2/#97</url>
<price>31822.26</price>
<currencyId>RUB</currencyId>
<categoryId>2</categoryId>
<vendorCode>RR 2 1800 12 01 A4 02 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/867/86788360104a0cad641a0bd8aa6905a3.png</picture>
<name>Tesi 2 1800 12 �������</name>
<model>Tesi 2 1800 12 �������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">1800</param>
<param name="���������� ������">12</param>
<param name="��� �����������">�������</param>
</offer>
<offer id="24" available="true">
<url>https://irsapshop.ru/catalog/tesi-2/#24</url>
<price>19657.89</price>
<currencyId>RUB</currencyId>
<categoryId>3</categoryId>
<vendorCode>RR 2 1800 04 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/867/86788360104a0cad641a0bd8aa6905a3.png</picture>
<name>Tesi 2 1800 4 ������</name>
<model>Tesi 2 1800 4 ������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">1800</param>
<param name="���������� ������">4</param>
<param name="��� �����������">������</param>
</offer>
<offer id="25" available="true">
<url>https://irsapshop.ru/catalog/tesi-2/#25</url>
<price>24956.21</price>
<currencyId>RUB</currencyId>
<categoryId>3</categoryId>
<vendorCode>RR 2 1800 06 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/867/86788360104a0cad641a0bd8aa6905a3.png</picture>
<name>Tesi 2 1800 6 ������</name>
<model>Tesi 2 1800 6 ������</model>
<description>����������� ��������� Irsap ����� ������� ����������� � �������������� ������, ��� ��� ����������� ������� ��������� � ��������� �������� ���������� �������. </description>
<param name="������">1800</param>
<param name="���������� ������">6</param>
<param name="��� �����������">������</param>
</offer>
<offer id="26" available="true">
<url>https://irsapshop.ru/catalog/tesi-2/#26</url>
<price>30254.52</price>
<currencyId>RUB</currencyId>
<categoryId>3</categoryId>
<vendorCode>RR 2 1800 08 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/867/86788360104a0cad641a0bd8aa6905a3.png</picture>
<name>Tesi 2 1800 8 ������</name>
<model>Tesi 2 1800 8 ������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">1800</param>
<param name="���������� ������">8</param>
<param name="��� �����������">������</param>
</offer>
<offer id="87" available="true">
<url>https://irsapshop.ru/catalog/tesi-2/#87</url>
<price>35597.79</price>
<currencyId>RUB</currencyId>
<categoryId>3</categoryId>
<vendorCode>RR 2 1800 10 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/867/86788360104a0cad641a0bd8aa6905a3.png</picture>
<name>Tesi 2 1800 10 ������</name>
<model>Tesi 2 1800 10 ������</model>
<description>������������ ������������ ��������� �������� �� ������ �������� ���������, �� � ������� �������� ����������� � ����� ������ ���������. �������� ������� ������� ������.</description>
<param name="������">1800</param>
<param name="���������� ������">10</param>
<param name="��� �����������">������</param>
</offer>
<offer id="88" available="true">
<url>https://irsapshop.ru/catalog/tesi-2/#88</url>
<price>40901.50</price>
<currencyId>RUB</currencyId>
<categoryId>3</categoryId>
<vendorCode>RR 2 1800 12 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/867/86788360104a0cad641a0bd8aa6905a3.png</picture>
<name>Tesi 2 1800 12 ������</name>
<model>Tesi 2 1800 12 ������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">1800</param>
<param name="���������� ������">12</param>
<param name="��� �����������">������</param>
</offer>
<offer id="21" available="true">
<url>https://irsapshop.ru/catalog/tesi-2/#21</url>
<price>13124.44</price>
<currencyId>RUB</currencyId>
<categoryId>6</categoryId>
<vendorCode>RR 2 565 10 01 A4 02 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/867/86788360104a0cad641a0bd8aa6905a3.png</picture>
<name>Tesi 2 565 10 �������</name>
<model>Tesi 2 565 10 �������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">565</param>
<param name="���������� ������">10</param>
<param name="��� �����������">�������</param>
</offer>
<offer id="100" available="true">
<url>https://irsapshop.ru/catalog/tesi-2/#100</url>
<price>23641.96</price>
<currencyId>RUB</currencyId>
<categoryId>6</categoryId>
<vendorCode>RR 2 565 18 01 A4 02 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/867/86788360104a0cad641a0bd8aa6905a3.png</picture>
<name>Tesi 2 565 18 �������</name>
<model>Tesi 2 565 18 �������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">565</param>
<param name="���������� ������">18</param>
<param name="��� �����������">�������</param>
</offer>
<offer id="101" available="true">
<url>https://irsapshop.ru/catalog/tesi-2/#101</url>
<price>26248.87</price>
<currencyId>RUB</currencyId>
<categoryId>6</categoryId>
<vendorCode>RR 2 565 20 01 A4 02 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/867/86788360104a0cad641a0bd8aa6905a3.png</picture>
<name>Tesi 2 565 20 �������</name>
<model>Tesi 2 565 20 �������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">565</param>
<param name="���������� ������">20</param>
<param name="��� �����������">�������</param>
</offer>
<offer id="102" available="true">
<url>https://irsapshop.ru/catalog/tesi-2/#102</url>
<price>28855.78</price>
<currencyId>RUB</currencyId>
<categoryId>6</categoryId>
<vendorCode>RR 2 565 22 01 A4 02 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/867/86788360104a0cad641a0bd8aa6905a3.png</picture>
<name>Tesi 2 565 22 �������</name>
<model>Tesi 2 565 22 �������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">565</param>
<param name="���������� ������">22</param>
<param name="��� �����������">�������</param>
</offer>
<offer id="103" available="true">
<url>https://irsapshop.ru/catalog/tesi-2/#103</url>
<price>31462.69</price>
<currencyId>RUB</currencyId>
<categoryId>6</categoryId>
<vendorCode>RR 2 565 24 01 A4 02 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/867/86788360104a0cad641a0bd8aa6905a3.png</picture>
<name>Tesi 2 565 24 �������</name>
<model>Tesi 2 565 24 �������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">565</param>
<param name="���������� ������">24</param>
<param name="��� �����������">�������</param>
</offer>
<offer id="104" available="true">
<url>https://irsapshop.ru/catalog/tesi-2/#104</url>
<price>34069.60</price>
<currencyId>RUB</currencyId>
<categoryId>6</categoryId>
<vendorCode>RR 2 565 26 01 A4 02 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/867/86788360104a0cad641a0bd8aa6905a3.png</picture>
<name>Tesi 2 565 26 �������</name>
<model>Tesi 2 565 26 �������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">565</param>
<param name="���������� ������">26</param>
<param name="��� �����������">�������</param>
</offer>
<offer id="105" available="true">
<url>https://irsapshop.ru/catalog/tesi-2/#105</url>
<price>36676.51</price>
<currencyId>RUB</currencyId>
<categoryId>6</categoryId>
<vendorCode>RR 2 565 28 01 A4 02 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/867/86788360104a0cad641a0bd8aa6905a3.png</picture>
<name>Tesi 2 565 28 �������</name>
<model>Tesi 2 565 28 �������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">565</param>
<param name="���������� ������">28</param>
<param name="��� �����������">�������</param>
</offer>
<offer id="107" available="true">
<url>https://irsapshop.ru/catalog/tesi-2/#107</url>
<price>41890.32</price>
<currencyId>RUB</currencyId>
<categoryId>6</categoryId>
<vendorCode>RR 2 565 32 01 A4 02 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/867/86788360104a0cad641a0bd8aa6905a3.png</picture>
<name>Tesi 2 565 32 �������</name>
<model>Tesi 2 565 32 �������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">565</param>
<param name="���������� ������">32</param>
<param name="��� �����������">�������</param>
</offer>
<offer id="27" available="true">
<url>https://irsapshop.ru/catalog/tesi-2/#27</url>
<price>27507.38</price>
<currencyId>RUB</currencyId>
<categoryId>7</categoryId>
<vendorCode>RR 2 565 14 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/867/86788360104a0cad641a0bd8aa6905a3.png</picture>
<name>Tesi 2 565 14 ������</name>
<model>Tesi 2 565 14 ������</model>
<description>��������� �������� �������� ��� �������� �������� ���������. �� ���������� ����������� �����, �� ����������� ������. ������ �������� ��� ��������� ��� ����������� ����.</description>
<param name="������">565</param>
<param name="���������� ������">14</param>
<param name="��� �����������">������</param>
</offer>
<offer id="28" available="true">
<url>https://irsapshop.ru/catalog/tesi-2/#28</url>
<price>24810.58</price>
<currencyId>RUB</currencyId>
<categoryId>7</categoryId>
<vendorCode>RR 2 565 12 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/867/86788360104a0cad641a0bd8aa6905a3.png</picture>
<name>Tesi 2 565 12 ������</name>
<model>Tesi 2 565 12 ������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">565</param>
<param name="���������� ������">12</param>
<param name="��� �����������">������</param>
</offer>
<offer id="29" available="true">
<url>https://irsapshop.ru/catalog/tesi-2/#29</url>
<price>22203.67</price>
<currencyId>RUB</currencyId>
<categoryId>7</categoryId>
<vendorCode>RR 2 565 10 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/867/86788360104a0cad641a0bd8aa6905a3.png</picture>
<name>Tesi 2 565 10 ������</name>
<model>Tesi 2 565 10 ������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">565</param>
<param name="���������� ������">10</param>
<param name="��� �����������">������</param>
</offer>
<offer id="90" available="true">
<url>https://irsapshop.ru/catalog/tesi-2/#90</url>
<price>30024.40</price>
<currencyId>RUB</currencyId>
<categoryId>7</categoryId>
<vendorCode>RR 2 565 16 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/867/86788360104a0cad641a0bd8aa6905a3.png</picture>
<name>Tesi 2 565 16 ������</name>
<model>Tesi 2 565 16 ������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">565</param>
<param name="���������� ������">16</param>
<param name="��� �����������">������</param>
</offer>
<offer id="91" available="true">
<url>https://irsapshop.ru/catalog/tesi-2/#91</url>
<price>32721.20</price>
<currencyId>RUB</currencyId>
<categoryId>7</categoryId>
<vendorCode>RR 2 565 18 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/867/86788360104a0cad641a0bd8aa6905a3.png</picture>
<name>Tesi 2 565 18 ������</name>
<model>Tesi 2 565 18 ������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">565</param>
<param name="���������� ������">18</param>
<param name="��� �����������">������</param>
</offer>
<offer id="92" available="true">
<url>https://irsapshop.ru/catalog/tesi-2/#92</url>
<price>35238.21</price>
<currencyId>RUB</currencyId>
<categoryId>7</categoryId>
<vendorCode>RR 2 565 20 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/867/86788360104a0cad641a0bd8aa6905a3.png</picture>
<name>Tesi 2 565 20 ������</name>
<model>Tesi 2 565 20 ������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">565</param>
<param name="���������� ������">20</param>
<param name="��� �����������">������</param>
</offer>
<offer id="93" available="true">
<url>https://irsapshop.ru/catalog/tesi-2/#93</url>
<price>37935.01</price>
<currencyId>RUB</currencyId>
<categoryId>7</categoryId>
<vendorCode>RR 2 565 22 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/867/86788360104a0cad641a0bd8aa6905a3.png</picture>
<name>Tesi 2 565 22 ������</name>
<model>Tesi 2 565 22 ������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">565</param>
<param name="���������� ������">22</param>
<param name="��� �����������">������</param>
</offer>
<offer id="94" available="true">
<url>https://irsapshop.ru/catalog/tesi-2/#94</url>
<price>40541.92</price>
<currencyId>RUB</currencyId>
<categoryId>7</categoryId>
<vendorCode>RR 2 565 24 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/867/86788360104a0cad641a0bd8aa6905a3.png</picture>
<name>Tesi 2 565 24 ������</name>
<model>Tesi 2 565 24 ������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">565</param>
<param name="���������� ������">24</param>
<param name="��� �����������">������</param>
</offer>
<offer id="95" available="true">
<url>https://irsapshop.ru/catalog/tesi-2/#95</url>
<price>43148.83</price>
<currencyId>RUB</currencyId>
<categoryId>7</categoryId>
<vendorCode>RR 2 565 26 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/867/86788360104a0cad641a0bd8aa6905a3.png</picture>
<name>Tesi 2 565 26 ������</name>
<model>Tesi 2 565 26 ������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">565</param>
<param name="���������� ������">26</param>
<param name="��� �����������">������</param>
</offer>
<offer id="8840" available="true">
<url>https://irsapshop.ru/catalog/tesi-2/#8840</url>
<price>45755.74</price>
<currencyId>RUB</currencyId>
<categoryId>7</categoryId>
<vendorCode>RR 2 565 28 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/867/86788360104a0cad641a0bd8aa6905a3.png</picture>
<name>Tesi 2 565 28 ������</name>
<model>Tesi 2 565 28 ������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">565</param>
<param name="���������� ������">28</param>
<param name="��� �����������">������</param>
</offer>
<offer id="8842" available="true">
<url>https://irsapshop.ru/catalog/tesi-2/#8842</url>
<price>48362.65</price>
<currencyId>RUB</currencyId>
<categoryId>7</categoryId>
<vendorCode>RR 2 565 30 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/867/86788360104a0cad641a0bd8aa6905a3.png</picture>
<name>Tesi 2 565 30 ������</name>
<model>Tesi 2 565 30 ������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">565</param>
<param name="���������� ������">30</param>
<param name="��� �����������">������</param>
</offer>
<offer id="621" available="true">
<url>https://irsapshop.ru/catalog/tesi-2/#621</url>
<price>44946.70</price>
<currencyId>RUB</currencyId>
<categoryId>15</categoryId>
<vendorCode>RR 2 400 30 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/867/86788360104a0cad641a0bd8aa6905a3.png</picture>
<name>Tesi 2 400 30 ������</name>
<model>Tesi 2 400 30 ������</model>
<description></description>
<param name="������">400</param>
<param name="���������� ������">30</param>
<param name="��� �����������">������</param>
</offer>
<offer id="1404" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#1404</url>
<price>46834.46</price>
<currencyId>RUB</currencyId>
<categoryId>44</categoryId>
<vendorCode>RR 3 300 30 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 300 30 ������</name>
<model>Tesi 3 300 30 ������</model>
<description></description>
<param name="������">300</param>
<param name="���������� ������">30</param>
<param name="��� �����������">������</param>
</offer>
<offer id="138" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#138</url>
<price>10697.31</price>
<currencyId>RUB</currencyId>
<categoryId>47</categoryId>
<vendorCode>RR 3 365 08 01 A4 02 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 365 8 �������</name>
<model>Tesi 3 365 8 �������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">365</param>
<param name="���������� ������">8</param>
<param name="��� �����������">�������</param>
</offer>
<offer id="139" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#139</url>
<price>13304.22</price>
<currencyId>RUB</currencyId>
<categoryId>47</categoryId>
<vendorCode>RR 3 365 10 01 A4 02 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 365 10 �������</name>
<model>Tesi 3 365 10 �������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">365</param>
<param name="���������� ������">10</param>
<param name="��� �����������">�������</param>
</offer>
<offer id="140" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#140</url>
<price>16001.03</price>
<currencyId>RUB</currencyId>
<categoryId>47</categoryId>
<vendorCode>RR 3 365 12 01 A4 02 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 365 12 �������</name>
<model>Tesi 3 365 12 �������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">365</param>
<param name="���������� ������">12</param>
<param name="��� �����������">�������</param>
</offer>
<offer id="141" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#141</url>
<price>18607.93</price>
<currencyId>RUB</currencyId>
<categoryId>47</categoryId>
<vendorCode>RR 3 365 14 01 A4 02 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 365 14 �������</name>
<model>Tesi 3 365 14 �������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">365</param>
<param name="���������� ������">14</param>
<param name="��� �����������">�������</param>
</offer>
<offer id="142" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#142</url>
<price>21304.74</price>
<currencyId>RUB</currencyId>
<categoryId>47</categoryId>
<vendorCode>RR 3 365 16 01 A4 02 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 365 16 �������</name>
<model>Tesi 3 365 16 �������</model>
<description>������ ������� �������� ����� ������� ����������� � �������� ��� ��������� � ��������� � �������� ������. ��������� ������ � ������ �������� ���������� ������� ������� � ��������.</description>
<param name="������">365</param>
<param name="���������� ������">16</param>
<param name="��� �����������">�������</param>
</offer>
<offer id="143" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#143</url>
<price>23911.64</price>
<currencyId>RUB</currencyId>
<categoryId>47</categoryId>
<vendorCode>RR 3 365 18 01 A4 02 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 365 18 �������</name>
<model>Tesi 3 365 18 �������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">365</param>
<param name="���������� ������">18</param>
<param name="��� �����������">�������</param>
</offer>
<offer id="144" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#144</url>
<price>26608.45</price>
<currencyId>RUB</currencyId>
<categoryId>47</categoryId>
<vendorCode>RR 3 365 20 01 A4 02 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 365 20 �������</name>
<model>Tesi 3 365 20 �������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">365</param>
<param name="���������� ������">20</param>
<param name="��� �����������">�������</param>
</offer>
<offer id="145" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#145</url>
<price>29215.36</price>
<currencyId>RUB</currencyId>
<categoryId>47</categoryId>
<vendorCode>RR 3 365 22 01 A4 02 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 365 22 �������</name>
<model>Tesi 3 365 22 �������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">365</param>
<param name="���������� ������">22</param>
<param name="��� �����������">�������</param>
</offer>
<offer id="146" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#146</url>
<price>31912.16</price>
<currencyId>RUB</currencyId>
<categoryId>47</categoryId>
<vendorCode>RR 3 365 24 01 A4 02 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 365 24 �������</name>
<model>Tesi 3 365 24 �������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">365</param>
<param name="���������� ������">24</param>
<param name="��� �����������">�������</param>
</offer>
<offer id="147" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#147</url>
<price>34608.96</price>
<currencyId>RUB</currencyId>
<categoryId>47</categoryId>
<vendorCode>RR 3 365 26 01 A4 02 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 365 26 �������</name>
<model>Tesi 3 365 26 �������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">365</param>
<param name="���������� ������">26</param>
<param name="��� �����������">�������</param>
</offer>
<offer id="148" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#148</url>
<price>37215.87</price>
<currencyId>RUB</currencyId>
<categoryId>47</categoryId>
<vendorCode>RR 3 365 28 01 A4 02 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 365 28 �������</name>
<model>Tesi 3 365 28 �������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">365</param>
<param name="���������� ������">28</param>
<param name="��� �����������">�������</param>
</offer>
<offer id="149" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#149</url>
<price>39912.67</price>
<currencyId>RUB</currencyId>
<categoryId>47</categoryId>
<vendorCode>RR 3 365 30 01 A4 02 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 365 30 �������</name>
<model>Tesi 3 365 30 �������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">365</param>
<param name="���������� ������">30</param>
<param name="��� �����������">�������</param>
</offer>
<offer id="52" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#52</url>
<price>27687.17</price>
<currencyId>RUB</currencyId>
<categoryId>48</categoryId>
<vendorCode>RR 3 365 14 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 365 14 ������</name>
<model>Tesi 3 365 14 ������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">365</param>
<param name="���������� ������">14</param>
<param name="��� �����������">������</param>
</offer>
<offer id="53" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#53</url>
<price>22383.46</price>
<currencyId>RUB</currencyId>
<categoryId>48</categoryId>
<vendorCode>RR 3 365 10 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 365 10 ������</name>
<model>Tesi 3 365 10 ������</model>
<description>������� ������� ��� ��������� � ������� ������. �������� 45 ������ �� ����� � ����������� ����� � ��������� ��������� � �������.</description>
<param name="������">365</param>
<param name="���������� ������">10</param>
<param name="��� �����������">������</param>
</offer>
<offer id="54" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#54</url>
<price>25080.26</price>
<currencyId>RUB</currencyId>
<categoryId>48</categoryId>
<vendorCode>RR 3 365 12 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 365 12 ������</name>
<model>Tesi 3 365 12 ������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">365</param>
<param name="���������� ������">12</param>
<param name="��� �����������">������</param>
</offer>
<offer id="111" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#111</url>
<price>19776.55</price>
<currencyId>RUB</currencyId>
<categoryId>48</categoryId>
<vendorCode>RR 3 365 08 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 365 8 ������</name>
<model>Tesi 3 365 8 ������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">365</param>
<param name="���������� ������">8</param>
<param name="��� �����������">������</param>
</offer>
<offer id="112" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#112</url>
<price>30383.97</price>
<currencyId>RUB</currencyId>
<categoryId>48</categoryId>
<vendorCode>RR 3 365 16 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 365 16 ������</name>
<model>Tesi 3 365 16 ������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">365</param>
<param name="���������� ������">16</param>
<param name="��� �����������">������</param>
</offer>
<offer id="113" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#113</url>
<price>32990.88</price>
<currencyId>RUB</currencyId>
<categoryId>48</categoryId>
<vendorCode>RR 3 365 18 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 365 18 ������</name>
<model>Tesi 3 365 18 ������</model>
<description>������� ����������� �������� �������� ��������� ������ ��� ��������� � ������� �������������. ������ �������� ���� �������, �� ��������� ��� ���� ������. </description>
<param name="������">365</param>
<param name="���������� ������">18</param>
<param name="��� �����������">������</param>
</offer>
<offer id="114" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#114</url>
<price>35687.68</price>
<currencyId>RUB</currencyId>
<categoryId>48</categoryId>
<vendorCode>RR 3 365 20 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 365 20 ������</name>
<model>Tesi 3 365 20 ������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">365</param>
<param name="���������� ������">20</param>
<param name="��� �����������">������</param>
</offer>
<offer id="115" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#115</url>
<price>38384.48</price>
<currencyId>RUB</currencyId>
<categoryId>48</categoryId>
<vendorCode>RR 3 365 22 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 365 22 ������</name>
<model>Tesi 3 365 22 ������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">365</param>
<param name="���������� ������">22</param>
<param name="��� �����������">������</param>
</offer>
<offer id="116" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#116</url>
<price>40991.39</price>
<currencyId>RUB</currencyId>
<categoryId>48</categoryId>
<vendorCode>RR 3 365 24 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 365 24 ������</name>
<model>Tesi 3 365 24 ������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">365</param>
<param name="���������� ������">24</param>
<param name="��� �����������">������</param>
</offer>
<offer id="117" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#117</url>
<price>43688.19</price>
<currencyId>RUB</currencyId>
<categoryId>48</categoryId>
<vendorCode>RR 3 365 26 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 365 26 ������</name>
<model>Tesi 3 365 26 ������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">365</param>
<param name="���������� ������">26</param>
<param name="��� �����������">������</param>
</offer>
<offer id="118" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#118</url>
<price>46295.10</price>
<currencyId>RUB</currencyId>
<categoryId>48</categoryId>
<vendorCode>RR 3 365 28 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 365 28 ������</name>
<model>Tesi 3 365 28 ������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">365</param>
<param name="���������� ������">28</param>
<param name="��� �����������">������</param>
</offer>
<offer id="119" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#119</url>
<price>48902.01</price>
<currencyId>RUB</currencyId>
<categoryId>48</categoryId>
<vendorCode>RR 3 365 30 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 365 30 ������</name>
<model>Tesi 3 365 30 ������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">365</param>
<param name="���������� ������">30</param>
<param name="��� �����������">������</param>
</offer>
<offer id="155" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#155</url>
<price>17978.68</price>
<currencyId>RUB</currencyId>
<categoryId>57</categoryId>
<vendorCode>RR 3 565 12 01 A4 02 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 565 12 �������</name>
<model>Tesi 3 565 12 �������</model>
<description>��������� �������� �� ������������ ������ ����� ����������� ������� �����������. �������� �� ������ � ������� ��������, �� � ��� ������������ �������.</description>
<param name="������">565</param>
<param name="���������� ������">12</param>
<param name="��� �����������">�������</param>
</offer>
<offer id="156" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#156</url>
<price>21035.06</price>
<currencyId>RUB</currencyId>
<categoryId>57</categoryId>
<vendorCode>RR 3 565 14 01 A4 02 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 565 14 �������</name>
<model>Tesi 3 565 14 �������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">565</param>
<param name="���������� ������">14</param>
<param name="��� �����������">�������</param>
</offer>
<offer id="157" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#157</url>
<price>24001.54</price>
<currencyId>RUB</currencyId>
<categoryId>57</categoryId>
<vendorCode>RR 3 565 16 01 A4 02 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 565 16 �������</name>
<model>Tesi 3 565 16 �������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">565</param>
<param name="���������� ������">16</param>
<param name="��� �����������">�������</param>
</offer>
<offer id="158" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#158</url>
<price>26968.02</price>
<currencyId>RUB</currencyId>
<categoryId>57</categoryId>
<vendorCode>RR 3 565 18 01 A4 02 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 565 18 �������</name>
<model>Tesi 3 565 18 �������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">565</param>
<param name="���������� ������">18</param>
<param name="��� �����������">�������</param>
</offer>
<offer id="159" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#159</url>
<price>30024.40</price>
<currencyId>RUB</currencyId>
<categoryId>57</categoryId>
<vendorCode>RR 3 565 20 01 A4 02 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 565 20 �������</name>
<model>Tesi 3 565 20 �������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">565</param>
<param name="���������� ������">20</param>
<param name="��� �����������">�������</param>
</offer>
<offer id="160" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#160</url>
<price>32990.88</price>
<currencyId>RUB</currencyId>
<categoryId>57</categoryId>
<vendorCode>RR 3 565 22 01 A4 02 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 565 22 �������</name>
<model>Tesi 3 565 22 �������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">565</param>
<param name="���������� ������">22</param>
<param name="��� �����������">�������</param>
</offer>
<offer id="161" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#161</url>
<price>35957.36</price>
<currencyId>RUB</currencyId>
<categoryId>57</categoryId>
<vendorCode>RR 3 565 24 01 A4 02 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 565 24 �������</name>
<model>Tesi 3 565 24 �������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">565</param>
<param name="���������� ������">24</param>
<param name="��� �����������">�������</param>
</offer>
<offer id="162" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#162</url>
<price>39013.74</price>
<currencyId>RUB</currencyId>
<categoryId>57</categoryId>
<vendorCode>RR 3 565 26 01 A4 02 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 565 26 �������</name>
<model>Tesi 3 565 26 �������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">565</param>
<param name="���������� ������">26</param>
<param name="��� �����������">�������</param>
</offer>
<offer id="163" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#163</url>
<price>41980.22</price>
<currencyId>RUB</currencyId>
<categoryId>57</categoryId>
<vendorCode>RR 3 565 28 01 A4 02 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 565 28 �������</name>
<model>Tesi 3 565 28 �������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">565</param>
<param name="���������� ������">28</param>
<param name="��� �����������">�������</param>
</offer>
<offer id="164" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#164</url>
<price>44946.70</price>
<currencyId>RUB</currencyId>
<categoryId>57</categoryId>
<vendorCode>RR 3 565 30 01 A4 02 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 565 30 �������</name>
<model>Tesi 3 565 30 �������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">565</param>
<param name="���������� ������">30</param>
<param name="��� �����������">�������</param>
</offer>
<offer id="1734" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#1734</url>
<price>8989.34</price>
<currencyId>RUB</currencyId>
<categoryId>57</categoryId>
<vendorCode>RR 3 565 06 01 A4 02 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 565 6 �������</name>
<model>Tesi 3 565 6 �������</model>
<description></description>
<param name="������">565</param>
<param name="���������� ������">6</param>
<param name="��� �����������">�������</param>
</offer>
<offer id="1736" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#1736</url>
<price>12045.72</price>
<currencyId>RUB</currencyId>
<categoryId>57</categoryId>
<vendorCode>RR 3 565 08 01 A4 02 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 565 8 �������</name>
<model>Tesi 3 565 8 �������</model>
<description></description>
<param name="������">565</param>
<param name="���������� ������">8</param>
<param name="��� �����������">�������</param>
</offer>
<offer id="58" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#58</url>
<price>30114.29</price>
<currencyId>RUB</currencyId>
<categoryId>58</categoryId>
<vendorCode>RR 3 565 14 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 565 14 ������</name>
<model>Tesi 3 565 14 ������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">565</param>
<param name="���������� ������">14</param>
<param name="��� �����������">������</param>
</offer>
<offer id="59" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#59</url>
<price>24091.43</price>
<currencyId>RUB</currencyId>
<categoryId>58</categoryId>
<vendorCode>RR 3 565 10 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 565 10 ������</name>
<model>Tesi 3 565 10 ������</model>
<description>��������� �������� �� 10 ������ ������ �������� ��� ��������� ����� �������. ������ � 56 ����������� �������� ����������� ������� ��� ����������� �����.</description>
<param name="������">565</param>
<param name="���������� ������">10</param>
<param name="��� �����������">������</param>
</offer>
<offer id="124" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#124</url>
<price>33080.77</price>
<currencyId>RUB</currencyId>
<categoryId>58</categoryId>
<vendorCode>RR 3 565 16 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 565 16 ������</name>
<model>Tesi 3 565 16 ������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">565</param>
<param name="���������� ������">16</param>
<param name="��� �����������">������</param>
</offer>
<offer id="125" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#125</url>
<price>36137.15</price>
<currencyId>RUB</currencyId>
<categoryId>58</categoryId>
<vendorCode>RR 3 565 18 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 565 18 ������</name>
<model>Tesi 3 565 18 ������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">565</param>
<param name="���������� ������">18</param>
<param name="��� �����������">������</param>
</offer>
<offer id="131" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#131</url>
<price>54025.93</price>
<currencyId>RUB</currencyId>
<categoryId>58</categoryId>
<vendorCode>RR 3 565 30 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 565 30 ������</name>
<model>Tesi 3 565 30 ������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">565</param>
<param name="���������� ������">30</param>
<param name="��� �����������">������</param>
</offer>
<offer id="134" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#134</url>
<price>63015.27</price>
<currencyId>RUB</currencyId>
<categoryId>58</categoryId>
<vendorCode>RR 3 565 36 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 565 36 ������</name>
<model>Tesi 3 565 36 ������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">565</param>
<param name="���������� ������">36</param>
<param name="��� �����������">������</param>
</offer>
<offer id="1752" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#1752</url>
<price>18068.57</price>
<currencyId>RUB</currencyId>
<categoryId>58</categoryId>
<vendorCode>RR 3 565 06 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 565 6 ������</name>
<model>Tesi 3 565 6 ������</model>
<description></description>
<param name="������">565</param>
<param name="���������� ������">6</param>
<param name="��� �����������">������</param>
</offer>
<offer id="1754" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#1754</url>
<price>21124.95</price>
<currencyId>RUB</currencyId>
<categoryId>58</categoryId>
<vendorCode>RR 3 565 08 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 565 8 ������</name>
<model>Tesi 3 565 8 ������</model>
<description></description>
<param name="������">565</param>
<param name="���������� ������">8</param>
<param name="��� �����������">������</param>
</offer>
<offer id="1766" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#1766</url>
<price>27057.91</price>
<currencyId>RUB</currencyId>
<categoryId>58</categoryId>
<vendorCode>RR 3 565 12 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 565 12 ������</name>
<model>Tesi 3 565 12 ������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">565</param>
<param name="���������� ������">12</param>
<param name="��� �����������">������</param>
</offer>
<offer id="1767" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#1767</url>
<price>51059.45</price>
<currencyId>RUB</currencyId>
<categoryId>58</categoryId>
<vendorCode>RR 3 565 28 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 565 28 ������</name>
<model>Tesi 3 565 28 ������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">565</param>
<param name="���������� ������">28</param>
<param name="��� �����������">������</param>
</offer>
<offer id="1768" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#1768</url>
<price>39013.74</price>
<currencyId>RUB</currencyId>
<categoryId>58</categoryId>
<vendorCode>RR 3 565 20 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 565 20 ������</name>
<model>Tesi 3 565 20 ������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">565</param>
<param name="���������� ������">20</param>
<param name="��� �����������">������</param>
</offer>
<offer id="1769" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#1769</url>
<price>42070.11</price>
<currencyId>RUB</currencyId>
<categoryId>58</categoryId>
<vendorCode>RR 3 565 22 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 565 22 ������</name>
<model>Tesi 3 565 22 ������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">565</param>
<param name="���������� ������">22</param>
<param name="��� �����������">������</param>
</offer>
<offer id="1770" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#1770</url>
<price>45126.49</price>
<currencyId>RUB</currencyId>
<categoryId>58</categoryId>
<vendorCode>RR 3 565 24 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 565 24 ������</name>
<model>Tesi 3 565 24 ������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">565</param>
<param name="���������� ������">24</param>
<param name="��� �����������">������</param>
</offer>
<offer id="1771" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#1771</url>
<price>48092.97</price>
<currencyId>RUB</currencyId>
<categoryId>58</categoryId>
<vendorCode>RR 3 565 26 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 565 26 ������</name>
<model>Tesi 3 565 26 ������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">565</param>
<param name="���������� ������">26</param>
<param name="��� �����������">������</param>
</offer>
<offer id="135" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#135</url>
<price>15641.45</price>
<currencyId>RUB</currencyId>
<categoryId>73</categoryId>
<vendorCode>RR 3 1800 04 01 A4 02 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 1800 4 �������</name>
<model>Tesi 3 1800 4 �������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">1800</param>
<param name="���������� ������">4</param>
<param name="��� �����������">�������</param>
</offer>
<offer id="136" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#136</url>
<price>23372.28</price>
<currencyId>RUB</currencyId>
<categoryId>73</categoryId>
<vendorCode>RR 3 1800 06 01 A4 02 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 1800 6 �������</name>
<model>Tesi 3 1800 6 �������</model>
<description>������������ ��������� �������� ������ ��������� ������ �������. ����� ��������� �� ������ ����������� ����� ����, �� � �������, ���������� ��� ��� ������-������.</description>
<param name="������">1800</param>
<param name="���������� ������">6</param>
<param name="��� �����������">�������</param>
</offer>
<offer id="137" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#137</url>
<price>31193.01</price>
<currencyId>RUB</currencyId>
<categoryId>73</categoryId>
<vendorCode>RR 3 1800 08 01 A4 02 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 1800 8 �������</name>
<model>Tesi 3 1800 8 �������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">1800</param>
<param name="���������� ������">8</param>
<param name="��� �����������">�������</param>
</offer>
<offer id="2184" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#2184</url>
<price>38923.84</price>
<currencyId>RUB</currencyId>
<categoryId>73</categoryId>
<vendorCode>RR 3 1800 10 01 A4 02 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 1800 10 �������</name>
<model>Tesi 3 1800 10 �������</model>
<description></description>
<param name="������">1800</param>
<param name="���������� ������">10</param>
<param name="��� �����������">�������</param>
</offer>
<offer id="2186" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#2186</url>
<price>46744.57</price>
<currencyId>RUB</currencyId>
<categoryId>73</categoryId>
<vendorCode>RR 3 1800 12 01 A4 02 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 1800 12 �������</name>
<model>Tesi 3 1800 12 �������</model>
<description></description>
<param name="������">1800</param>
<param name="���������� ������">12</param>
<param name="��� �����������">�������</param>
</offer>
<offer id="46" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#46</url>
<price>40272.24</price>
<currencyId>RUB</currencyId>
<categoryId>74</categoryId>
<vendorCode>RR 3 1800 08 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 1800 8 ������</name>
<model>Tesi 3 1800 8 ������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">1800</param>
<param name="���������� ������">8</param>
<param name="��� �����������">������</param>
</offer>
<offer id="47" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#47</url>
<price>24720.69</price>
<currencyId>RUB</currencyId>
<categoryId>74</categoryId>
<vendorCode>RR 3 1800 04 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 1800 4 ������</name>
<model>Tesi 3 1800 4 ������</model>
<description>����� �������� � ������� ������������ ����� ������ ���������� � ����������� ������� ���������. �������� ��� ��������� ������� ��������� ������������ � ������.</description>
<param name="������">1800</param>
<param name="���������� ������">4</param>
<param name="��� �����������">������</param>
</offer>
<offer id="48" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#48</url>
<price>32451.52</price>
<currencyId>RUB</currencyId>
<categoryId>74</categoryId>
<vendorCode>RR 3 1800 06 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 1800 6 ������</name>
<model>Tesi 3 1800 6 ������</model>
<description>�������� ����������� ��������� ��������� Irsap �������� ��������� ������������ � �������������� ��������, ��� ��� ����������� �� ������ ������� ����� ���������, �� � ��������� �������� ���������� �������. </description>
<param name="������">1800</param>
<param name="���������� ������">6</param>
<param name="��� �����������">������</param>
</offer>
<offer id="2210" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#2210</url>
<price>48003.08</price>
<currencyId>RUB</currencyId>
<categoryId>74</categoryId>
<vendorCode>RR 3 1800 10 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 1800 10 ������</name>
<model>Tesi 3 1800 10 ������</model>
<description></description>
<param name="������">1800</param>
<param name="���������� ������">10</param>
<param name="��� �����������">������</param>
</offer>
<offer id="2212" available="true">
<url>https://irsapshop.ru/catalog/tesi-3/#2212</url>
<price>55823.80</price>
<currencyId>RUB</currencyId>
<categoryId>74</categoryId>
<vendorCode>RR 3 1800 12 01 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/693/6933f6fe5ef3226f9b768c442c1c557d.jpg</picture>
<name>Tesi 3 1800 12 ������</name>
<model>Tesi 3 1800 12 ������</model>
<description></description>
<param name="������">1800</param>
<param name="���������� ������">12</param>
<param name="��� �����������">������</param>
</offer>
<offer id="209" available="true">
<url>https://irsapshop.ru/catalog/step-e/#209</url>
<price>132772.55</price>
<currencyId>RUB</currencyId>
<categoryId>231</categoryId>
<vendorCode>SEP050T 50 IR 01 NNN</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/879/Step_E-0.jpg</picture>
<name>Step_E ������������� 775/500 7 �� ����</name>
<model>Step_E ������������� 775/500 7 �� ����</model>
<description></description>
<param name="������">775</param>
<param name="���������� ������">7</param>
<param name="��� �����������">������</param>
</offer>
<offer id="234" available="true">
<url>https://irsapshop.ru/catalog/step-e-chernyy-khrom/#234</url>
<price>218225.22</price>
<currencyId>RUB</currencyId>
<categoryId>231</categoryId>
<vendorCode>SEL050T 2E IR 01 NNN</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/b64/Step_E-0.jpg</picture>
<name>Step_E ������������� 1255/500 11 �� ������ ����</name>
<model>Step_E ������������� 1255/500 11 �� ������ ����</model>
<description>����������������� ������� ����� �������� � �������� ������ �� ������������� �������. ������ �������� ������ ��������� ��������� ������������ � ���������� ���������.</description>
<param name="������">1255</param>
<param name="���������� ������">11</param>
<param name="��� �����������">������</param>
</offer>
<offer id="235" available="true">
<url>https://irsapshop.ru/catalog/step-e-chernyy-khrom/#235</url>
<price>158320.26</price>
<currencyId>RUB</currencyId>
<categoryId>231</categoryId>
<vendorCode>SEP050T 2E IR 01 NNN</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/b64/Step_E-0.jpg</picture>
<name>Step_E ������������� 775/500 7 �� ������ ����</name>
<model>Step_E ������������� 775/500 7 �� ������ ����</model>
<description></description>
<param name="������">775</param>
<param name="���������� ������">7</param>
<param name="��� �����������">������</param>
</offer>
<offer id="213" available="true">
<url>https://irsapshop.ru/catalog/bella-elettrico/#213</url>
<price>52416.84</price>
<currencyId>RUB</currencyId>
<categoryId>232</categoryId>
<vendorCode>BES053 I 50 IR 01 NNN</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/9bf/Bella_Elettrico-2.jpg</picture>
<name>Bella ������������� DHW 550 - 532 (connection centre 500) (����)</name>
<model>Bella ������������� DHW 550 - 532 (connection centre 500) (����)</model>
<description></description>
<param name="������">550</param>
<param name="���������� ������">4</param>
<param name="��� �����������">������</param>
</offer>
<offer id="214" available="true">
<url>https://irsapshop.ru/catalog/bella-elettrico/#214</url>
<price>88850.64</price>
<currencyId>RUB</currencyId>
<categoryId>232</categoryId>
<vendorCode>BEP053 I 50 IR 01 NNN</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/9bf/Bella_Elettrico-2.jpg</picture>
<name>Bella ������������� DHW 875 - 532 (connection centre 500) (����)</name>
<model>Bella ������������� DHW 875 - 532 (connection centre 500) (����)</model>
<description></description>
<param name="������">875</param>
<param name="���������� ������">6</param>
<param name="��� �����������">������</param>
</offer>
<offer id="215" available="true">
<url>https://irsapshop.ru/catalog/bella-elettrico/#215</url>
<price>113832.01</price>
<currencyId>RUB</currencyId>
<categoryId>232</categoryId>
<vendorCode>BEM053 I 50 IR 01 NNN</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/9bf/Bella_Elettrico-2.jpg</picture>
<name>Bella ������������� DHW 1200 - 532 (connection centre 500) (����)</name>
<model>Bella ������������� DHW 1200 - 532 (connection centre 500) (����)</model>
<description>������� ������� ��� ������ ������, � ������� ���������� �������� ����� � ����� ��������� �����������������. �������� ���������� ������ � ����� ����.</description>
<param name="������">1200</param>
<param name="���������� ������">8</param>
<param name="��� �����������">������</param>
</offer>
<offer id="237" available="true">
<url>https://irsapshop.ru/catalog/tole/#237</url>
<price>87529.20</price>
<currencyId>RUB</currencyId>
<categoryId>233</categoryId>
<vendorCode>TCM052 B 50 IR 01 NNN</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/fda/Tole-2.jpg</picture>
<name>TOLE ��� ��� 1310/525 CL.50 15 ������, (����) chrome</name>
<model>TOLE ��� ��� 1310/525 CL.50 15 ������, (����) chrome</model>
<description></description>
<param name="������">1310</param>
<param name="���������� ������">15</param>
<param name="��� �����������">������</param>
</offer>
<offer id="238" available="true">
<url>https://irsapshop.ru/catalog/tole/#238</url>
<price>71294.46</price>
<currencyId>RUB</currencyId>
<categoryId>233</categoryId>
<vendorCode>TSP052 B 50 IR 01 NNN</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/fda/Tole-2.jpg</picture>
<name>TOLE ��� ��� 842/525 CL.50 11 ������, (����) chrome</name>
<model>TOLE ��� ��� 842/525 CL.50 11 ������, (����) chrome</model>
<description>������������� ������ ����������������� ��������� ������������ ��� � ����� ������ �������. ������������ ������������� ���� �������� � ����� ��������.</description>
<param name="������">842</param>
<param name="���������� ������">11</param>
<param name="��� �����������">������</param>
</offer>
<offer id="9285" available="true">
<url>https://irsapshop.ru/catalog/step-e-elektricheskiy/#9285</url>
<price>102945.92</price>
<currencyId>RUB</currencyId>
<categoryId>366</categoryId>
<vendorCode>SEP050T XX IR 01 NNN</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/9ad/Step_E_06.jpg</picture>
<name>Step_E ������������� 775 7 ��</name>
<model>Step_E ������������� 775 7 ��</model>
<description></description>
<param name="������">775</param>
<param name="���������� ������">7</param>
<param name="��� �����������">������</param>
</offer>
<offer id="197" available="true">
<url>https://irsapshop.ru/catalog/ares-electric/#197</url>
<price>61352.25</price>
<currencyId>RUB</currencyId>
<categoryId>385</categoryId>
<vendorCode>EIL058 H 50 IR 01 NNN</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/f4d/Ares-02.jpg</picture>
<name>����������������� ARES ELECTRIC - HEATER H 1462 - 580 (����)</name>
<model>����������������� ARES ELECTRIC - HEATER H 1462 - 580 (����)</model>
<description></description>
<param name="������">1462</param>
<param name="���������� ������">28</param>
<param name="��� �����������">������</param>
</offer>
<offer id="199" available="true">
<url>https://irsapshop.ru/catalog/ares-electric/#199</url>
<price>54556.30</price>
<currencyId>RUB</currencyId>
<categoryId>385</categoryId>
<vendorCode>EIM058 H 50 IR 01 NNN</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/f4d/Ares-02.jpg</picture>
<name>����������������� ARES ELECTRIC - HEATER H 1118 - 580 (����)</name>
<model>����������������� ARES ELECTRIC - HEATER H 1118 - 580 (����)</model>
<description></description>
<param name="������">1118</param>
<param name="���������� ������">22</param>
<param name="��� �����������">������</param>
</offer>
<offer id="201" available="true">
<url>https://irsapshop.ru/catalog/ares-electric/#201</url>
<price>50906.63</price>
<currencyId>RUB</currencyId>
<categoryId>385</categoryId>
<vendorCode>EIS058 H 50 IR 01 NNN</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/f4d/Ares-02.jpg</picture>
<name>����������������� ARES ELECTRIC - HEATER H 818 - 580 (����)</name>
<model>����������������� ARES ELECTRIC - HEATER H 818 - 580 (����)</model>
<description>�������� � ������������� � ����������� ������ ���� ����������������� ������������� ������� ��� ����� ������ �������. �������� � ������������ ������������� �����.</description>
<param name="������">818</param>
<param name="���������� ������">15</param>
<param name="��� �����������">������</param>
</offer>
<offer id="198" available="true">
<url>https://irsapshop.ru/catalog/ares-electric-belyy/#198</url>
<price>26554.51</price>
<currencyId>RUB</currencyId>
<categoryId>386</categoryId>
<vendorCode>EIM058 H 01 IR 01 NNN</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/7dc/Ares_Elettrico-2.jpg</picture>
<name>����������������� ARES ELECTRIC - HEATER H 1118 - 580 (�����)</name>
<model>����������������� ARES ELECTRIC - HEATER H 1118 - 580 (�����)</model>
<description>����������� ������ ������ ������� � ������������������ � ����� �����. �� �� ������ ������� ��������, �� � ������������� �������� ���������. �� ������� ������� ���������.</description>
<param name="������">1118</param>
<param name="���������� ������">22</param>
<param name="��� �����������">������</param>
</offer>
<offer id="200" available="true">
<url>https://irsapshop.ru/catalog/ares-electric-belyy/#200</url>
<price>21709.26</price>
<currencyId>RUB</currencyId>
<categoryId>386</categoryId>
<vendorCode>EIM058 I 01 IR 01 NNN</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/7dc/Ares_Elettrico-2.jpg</picture>
<name>����������������� ARES ELECTRIC - HEATER I 1118 - 580 (�����)</name>
<model>����������������� ARES ELECTRIC - HEATER I 1118 - 580 (�����)</model>
<description></description>
<param name="������">1118</param>
<param name="���������� ������">22</param>
<param name="��� �����������">������</param>
</offer>
<offer id="202" available="true">
<url>https://irsapshop.ru/catalog/ares-electric-belyy/#202</url>
<price>18562.99</price>
<currencyId>RUB</currencyId>
<categoryId>386</categoryId>
<vendorCode>EIS058 I 01 IR 01 NNN</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/7dc/Ares_Elettrico-2.jpg</picture>
<name>����������������� ARES ELECTRIC - HEATER I 818 - 580 (�����)</name>
<model>����������������� ARES ELECTRIC - HEATER I 818 - 580 (�����)</model>
<description></description>
<param name="������">818</param>
<param name="���������� ������">15</param>
<param name="��� �����������">������</param>
</offer>
<offer id="9436" available="true">
<url>https://irsapshop.ru/catalog/ares-electric-belyy/#9436</url>
<price>22716.06</price>
<currencyId>RUB</currencyId>
<categoryId>386</categoryId>
<vendorCode>EIM058 K 01 IR 01 NNN</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/7dc/Ares_Elettrico-2.jpg</picture>
<name>����������������� ARES ELECTRIC - HEATER K 1118 - 580 (�����)</name>
<model>����������������� ARES ELECTRIC - HEATER K 1118 - 580 (�����)</model>
<description></description>
<param name="������">1118</param>
<param name="���������� ������">22</param>
<param name="��� �����������">������</param>
</offer>
<offer id="9437" available="true">
<url>https://irsapshop.ru/catalog/ares-electric-belyy/#9437</url>
<price>23408.24</price>
<currencyId>RUB</currencyId>
<categoryId>386</categoryId>
<vendorCode>EIS058 H 01 IR 01 NNN</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/7dc/Ares_Elettrico-2.jpg</picture>
<name>����������������� ARES ELECTRIC - HEATER H 818 - 580 (�����)</name>
<model>����������������� ARES ELECTRIC - HEATER H 818 - 580 (�����)</model>
<description></description>
<param name="������">818</param>
<param name="���������� ������">15</param>
<param name="��� �����������">������</param>
</offer>
<offer id="9438" available="true">
<url>https://irsapshop.ru/catalog/ares-electric-belyy/#9438</url>
<price>19569.79</price>
<currencyId>RUB</currencyId>
<categoryId>386</categoryId>
<vendorCode>EIS058 K 01 IR 01 NNN</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/7dc/Ares_Elettrico-2.jpg</picture>
<name>����������������� ARES ELECTRIC - HEATER K 818 - 580 (�����)</name>
<model>����������������� ARES ELECTRIC - HEATER K 818 - 580 (�����)</model>
<description></description>
<param name="������">818</param>
<param name="���������� ������">15</param>
<param name="��� �����������">������</param>
</offer>
<offer id="218" available="true">
<url>https://irsapshop.ru/catalog/minuette-zoloto/#218</url>
<price>98226.52</price>
<currencyId>RUB</currencyId>
<categoryId>388</categoryId>
<vendorCode>MGU054 B 52 IR 01 NNN</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/3c6/Minuette-2.jpg</picture>
<name>����������������� IRSAP Minuette ��� ��� 414/540 CL.52 T01 3 ������, (������) gold</name>
<model>����������������� IRSAP Minuette ��� ��� 414/540 CL.52 T01 3 ������, (������) gold</model>
<description></description>
<param name="������">414</param>
<param name="���������� ������">3</param>
<param name="��� �����������">������</param>
</offer>
<offer id="219" available="true">
<url>https://irsapshop.ru/catalog/minuette-starinnaya-bronza/#219</url>
<price>92752.01</price>
<currencyId>RUB</currencyId>
<categoryId>388</categoryId>
<vendorCode>MGU054 B 54 IR 01 NNN</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/642/Minuette-2.jpg</picture>
<name>Minuette ��� ��� 414/540 CL.54 T01 3 ������, (������) bronze</name>
<model>Minuette ��� ��� 414/540 CL.54 T01 3 ������, (������) bronze</model>
<description></description>
<param name="������">414</param>
<param name="���������� ������">3</param>
<param name="��� �����������">������</param>
</offer>
<offer id="220" available="true">
<url>https://irsapshop.ru/catalog/minuette-starinnaya-bronza/#220</url>
<price>125284.43</price>
<currencyId>RUB</currencyId>
<categoryId>388</categoryId>
<vendorCode>MGS054 B 54 IR 01 NNN</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/642/Minuette-2.jpg</picture>
<name>Minuette ��� ��� 596/540 CL.54 T01 4 ������, (������) bronze</name>
<model>Minuette ��� ��� 596/540 CL.54 T01 4 ������, (������) bronze</model>
<description></description>
<param name="������">596</param>
<param name="���������� ������">4</param>
<param name="��� �����������">������</param>
</offer>
<offer id="221" available="true">
<url>https://irsapshop.ru/catalog/minuette-starinnaya-bronza/#221</url>
<price>157879.78</price>
<currencyId>RUB</currencyId>
<categoryId>388</categoryId>
<vendorCode>MGP054 B 54 IR 01 NNN</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/642/Minuette-2.jpg</picture>
<name>Minuette ��� ��� 778/540 CL.54 T01 5 ������, (������) bronze</name>
<model>Minuette ��� ��� 778/540 CL.54 T01 5 ������, (������) bronze</model>
<description></description>
<param name="������">778</param>
<param name="���������� ������">5</param>
<param name="��� �����������">������</param>
</offer>
<offer id="222" available="true">
<url>https://irsapshop.ru/catalog/minuette-starinnaya-bronza/#222</url>
<price>198844.20</price>
<currencyId>RUB</currencyId>
<categoryId>388</categoryId>
<vendorCode>MGM054 B 54 IR 01 NNN</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/642/Minuette-2.jpg</picture>
<name>Minuette ��� ��� 960/540 CL.54 T01 6 ������, (������) bronze</name>
<model>Minuette ��� ��� 960/540 CL.54 T01 6 ������, (������) bronze</model>
<description></description>
<param name="������">960</param>
<param name="���������� ������">6</param>
<param name="��� �����������">������</param>
</offer>
<offer id="223" available="true">
<url>https://irsapshop.ru/catalog/minuette-khrom/#223</url>
<price>65505.32</price>
<currencyId>RUB</currencyId>
<categoryId>388</categoryId>
<vendorCode>MGU054 B 50 IR 01 NNN</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/10c/Minuette-2.jpg</picture>
<name>����������������� IRSAP Minuette ��� ��� 414/540 CL.50 T01 3 ������, (����) chrome</name>
<model>����������������� IRSAP Minuette ��� ��� 414/540 CL.50 T01 3 ������, (����) chrome</model>
<description>����������������� � ���������� ������� �������������� ����� �������� � ����� �������� ������ �������. �������� �������� ��������� ��� ���� � ��������� ���������.</description>
<param name="������">414</param>
<param name="���������� ������">3</param>
<param name="��� �����������">������</param>
</offer>
<offer id="224" available="true">
<url>https://irsapshop.ru/catalog/minuette-khrom/#224</url>
<price>88410.16</price>
<currencyId>RUB</currencyId>
<categoryId>388</categoryId>
<vendorCode>MGS054 B 50 IR 01 NNN</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/10c/Minuette-2.jpg</picture>
<name>����������������� IRSAP Minuette ��� ��� 596/540 CL.50 T01 4 ������, (����) chrome</name>
<model>����������������� IRSAP Minuette ��� ��� 596/540 CL.50 T01 4 ������, (����) chrome</model>
<description></description>
<param name="������">596</param>
<param name="���������� ������">4</param>
<param name="��� �����������">������</param>
</offer>
<offer id="225" available="true">
<url>https://irsapshop.ru/catalog/minuette-khrom/#225</url>
<price>111440.85</price>
<currencyId>RUB</currencyId>
<categoryId>388</categoryId>
<vendorCode>MGP054 B 50 IR 01 NNN</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/10c/Minuette-2.jpg</picture>
<name>����������������� IRSAP Minuette ��� ��� 778/540 CL.50 T01 5 ������, (����) chrome</name>
<model>����������������� IRSAP Minuette ��� ��� 778/540 CL.50 T01 5 ������, (����) chrome</model>
<description></description>
<param name="������">778</param>
<param name="���������� ������">5</param>
<param name="��� �����������">������</param>
</offer>
<offer id="9411" available="true">
<url>https://irsapshop.ru/catalog/minuette-khrom/#9411</url>
<price>140386.52</price>
<currencyId>RUB</currencyId>
<categoryId>388</categoryId>
<vendorCode>MGM054 B 50 IR 01 NNN</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/10c/Minuette-2.jpg</picture>
<name>����������������� IRSAP Minuette ��� ��� 960/540 CL.50 T01 5 ������, (����) chrome</name>
<model>����������������� IRSAP Minuette ��� ��� 960/540 CL.50 T01 5 ������, (����) chrome</model>
<description></description>
<param name="������">960</param>
<param name="���������� ������">6</param>
<param name="��� �����������">������</param>
</offer>
<offer id="226" available="true">
<url>https://irsapshop.ru/catalog/minuette-electric-bronza/#226</url>
<price>204696.26</price>
<currencyId>RUB</currencyId>
<categoryId>389</categoryId>
<vendorCode>MGS054 I 54 IR 01 NNN</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/352/Minuette-8.jpg</picture>
<name>����������������� IRSAP Minuette/E 596/540 �������������, 6 ������, CL.54 T01, (��������� ������)</name>
<model>����������������� IRSAP Minuette/E 596/540 �������������, 6 ������, CL.54 T01, (��������� ������)</model>
<description>���������� ���� ��� ������������� �������. ������ �������� � ������ �������, ��������� ��� ������� ��� ����. ������� ���������� ��������� ���������� ����������� � ������.</description>
<param name="������">596</param>
<param name="���������� ������">6</param>
<param name="��� �����������">������</param>
</offer>
<offer id="9414" available="true">
<url>https://irsapshop.ru/catalog/tesi-2-sredniy-seryy/#9414</url>
<price>40159.88</price>
<currencyId>RUB</currencyId>
<categoryId>390</categoryId>
<vendorCode>RR 2 1800 06 4D A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/7d4/sred-ser-2.png</picture>
<name>Tesi 2 1800 6 ������ �����-�����</name>
<model>Tesi 2 1800 6 ������ �����-�����</model>
<description>������������ ������������ �������� � ������������ ������-����� �����. �������� ��� ������ ������� �������� � ����</description>
<param name="������">1800</param>
<param name="���������� ������">6</param>
<param name="��� �����������">������</param>
</offer>
<offer id="9415" available="true">
<url>https://irsapshop.ru/catalog/tesi-2-zhemchuzhno-seryy/#9415</url>
<price>40159.88</price>
<currencyId>RUB</currencyId>
<categoryId>390</categoryId>
<vendorCode>RR 2 1800 06 L6 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/d98/gemch-ser-2.png</picture>
<name>Tesi 2 1800 6 ������ �������� �����</name>
<model>Tesi 2 1800 6 ������ �������� �����</model>
<description>���������������� ���� �������� �������� ������������ ���������� �� ����������� ���������. ������� ��������� �����.</description>
<param name="������">1800</param>
<param name="���������� ������">6</param>
<param name="��� �����������">������</param>
</offer>
<offer id="9416" available="true">
<url>https://irsapshop.ru/catalog/tesi-2-matovyy-chernyy/#9416</url>
<price>40159.88</price>
<currencyId>RUB</currencyId>
<categoryId>390</categoryId>
<vendorCode>RR 2 1800 06 30 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/38d/matt-black-2.png</picture>
<name>Tesi 2 1800 6 ������ ������� ������</name>
<model>Tesi 2 1800 6 ������ ������� ������</model>
<description>������������� ������ �������� �������� ��� � ������������ ������������ ��������, ��� � � ��������� � ����� ����.</description>
<param name="������">1800</param>
<param name="���������� ������">6</param>
<param name="��� �����������">������</param>
</offer>
<offer id="9417" available="true">
<url>https://irsapshop.ru/catalog/tesi-2-matovyy-chernyy/#9417</url>
<price>49351.48</price>
<currencyId>RUB</currencyId>
<categoryId>390</categoryId>
<vendorCode>RR 2 1800 08 30 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/38d/matt-black-2.png</picture>
<name>Tesi 2 1800 8 ������ ������� ������</name>
<model>Tesi 2 1800 8 ������ ������� ������</model>
<description>������������ �������� ����������� � ������� ������ �����, �� ������� �������� � ������������ ������� ��������� ���� � �����. ������� �������� ��������.</description>
<param name="������">1800</param>
<param name="���������� ������">8</param>
<param name="��� �����������">������</param>
</offer>
<offer id="9418" available="true">
<url>https://irsapshop.ru/catalog/tesi-2-zhemchuzhno-seryy/#9418</url>
<price>49351.48</price>
<currencyId>RUB</currencyId>
<categoryId>390</categoryId>
<vendorCode>RR 2 1800 08 L6 A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/d98/gemch-ser-2.png</picture>
<name>Tesi 2 1800 8 ������ �������� �����</name>
<model>Tesi 2 1800 8 ������ �������� �����</model>
<description>�������� ������������ �������� ��� ���� � ����� � ������������ ���������� ��������-������ �����. ������ ���������� ��������� � �� ����� ������.</description>
<param name="������">1800</param>
<param name="���������� ������">8</param>
<param name="��� �����������">������</param>
</offer>
<offer id="9419" available="true">
<url>https://irsapshop.ru/catalog/tesi-2-sredniy-seryy/#9419</url>
<price>49351.48</price>
<currencyId>RUB</currencyId>
<categoryId>390</categoryId>
<vendorCode>RR 2 1800 08 4D A4 25 N</vendorCode>
<delivery>true</delivery>
<sales_notes>��������� ��� ���������, ������ �� �����</sales_notes>
<picture>https://irsapshop.ru/upload/iblock/7d4/sred-ser-2.png</picture>
<name>Tesi 2 1800 8 ������ �����-�����</name>
<model>Tesi 2 1800 8 ������ �����-�����</model>
<description>�������� ������� ��������, ������� �������� ��� ����� ������ ���������. ������������ ���� � ����������� ������������ ������������ ����������.</description>
<param name="������">1800</param>
<param name="���������� ������">8</param>
<param name="��� �����������">������</param>
</offer>
</offers>
</shop>
</yml_catalog>
