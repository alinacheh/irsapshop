<?
require_once($_SERVER['DOCUMENT_ROOT']. "/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule("catalog");
CModule::IncludeModule("sale");
CModule::IncludeModule("iblock");
CModule::IncludeModule("currency");

// - кондиционеры настенные Сплит системы, ок  1   элементов 1400  неактивных 316    1084
// - радиаторы отопления, ок  83                   элементов 2080  неактивных 359    1721
// - внутрипольные конвекторы, ok 297              элементов 1184  неактивных 786    398
// - арматура для систем отопления, ok 156         элементов 309  неактивных 2       307
// - тёплый пол, ok 68                             элементов 173  неактивных 8       165
// - водонагреватели, ok 11                        элементов 243  неактивных 68      175
// - полотенцесушители, ok 178                     элементов 562  неактивных 5       457
// - Дренажные каналы, ok 288                      элементов 156  неактивных 42      114
// - инсталляции, ok 386                           элементов 540  неактивных 293     247
// - системы очистки воды, ok 153                  элементов 48  неактивных 0        48
// - системы защиты от протечек воды, ok 123       элементов 40  неактивных 0        40
// - трубы и фитинги, ok 925                       элементов 429  неактивных 88      341
// - Бытовые вентиляторы, ok 118                   элементов 66  неактивных 0        66
// - вент установки, ok 131                        элементов 62  неактивных 6        56
// - Канализационные установки. ok 143             элементов 24  неактивных 7        17

$date_today = date("y-m-d");
$date_vrema = date("H:i");

$file = fopen('newyml.xml', 'w');
fwrite($file, '<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE yml_catalog SYSTEM "shops.dtd">
<yml_catalog date="20'.$date_today.' '.$date_vrema.'">
<shop>
<name>IRSAP </name>
<company>IRSAP </company>
<url>https://'.$_SERVER['SERVER_NAME'].'</url>
<platform>BSM/Yandex/Market</platform>
<version>1.1.0</version>
<enable_auto_discounts>true</enable_auto_discounts>
<currencies>
<currency id="RUR" rate="1" />
</currencies>
<delivery-options>
<option cost="700" days="1-2"/>
</delivery-options>
<categories>
    ');


    $sectionsINYml = Array(1,83,297,156,68,11,178,288,386,123,925,118,131,143,1328);
    $sectionsNOYml = Array(3,4,15,30,35,36,46,62,92,96,119,126,131,143,151,153,189,193,202,215,264,287,494,562,575,576,604,605,773,911,925,990,1027,1233,1359);


fwrite($file, "\n");//создаю категории
for($f=0;$f<count($sectionsINYml);$f++){

    $res = CIBlockSection::GetByID($sectionsINYml[$f]);
    $ar_res = $res->GetNext();
      fwrite($file, '<category id="'.$sectionsINYml[$f].'">'.$ar_res['NAME'].'</category>' . "\n");//Создаю родительскую категорию

      $arrIdForYmlProducts[$sectionsINYml[$f]] = $sectionsINYml[$f];//кладу в массив IDшники разрешенных секций

$arFilter = array('IBLOCK_ID' => 1, 'SECTION_ID' => $sectionsINYml[$f], 'ACTIVE'=>'Y');
$rsSections = CIBlockSection::GetList(array('LEFT_MARGIN' => 'ASC'), $arFilter);
while ($arSection = $rsSections->Fetch())
{
    if(in_array($arSection['ID'],$sectionsNOYml)){continue;}
fwrite($file, '<category id="'.$arSection['ID'].'" parentId="'.$arSection['IBLOCK_SECTION_ID'].'">'.$arSection['NAME'].'</category>' . "\n");
//     echo '<pre>';
//        print_r($arSection);
// echo '</pre>';

$arrIdForYmlProducts[$arSection['ID']] = $arSection['ID'];//кладу в массив IDшники разрешенных секций

    $arFilter2 = array('IBLOCK_ID' => 1, 'SECTION_ID' => $arSection['ID'], 'ACTIVE'=>'Y');
    $rsSections2 = CIBlockSection::GetList(array('LEFT_MARGIN' => 'ASC'), $arFilter2);
    while ($arSection2 = $rsSections2->Fetch())
    {
        if(in_array($arSection2['ID'],$sectionsNOYml)){continue;}
        fwrite($file, '<category id="'.$arSection2['ID'].'" parentId="'.$arSection2['IBLOCK_SECTION_ID'].'">'.$arSection2['NAME'].'</category>' . "\n");
        $arrIdForYmlProducts[$arSection2['ID']] = $arSection2['ID'];//кладу в массив IDшники разрешенных секций

        $arFilter3 = array('IBLOCK_ID' => 1, 'SECTION_ID' => $arSection2['ID'], 'ACTIVE'=>'Y');
        $rsSections3 = CIBlockSection::GetList(array('LEFT_MARGIN' => 'ASC'), $arFilter3);
        while ($arSection3 = $rsSections3->Fetch())
        {
            if(in_array($arSection3['ID'],$sectionsNOYml)){continue;}
            fwrite($file, '<category id="'.$arSection3['ID'].'" parentId="'.$arSection3['IBLOCK_SECTION_ID'].'">'.$arSection3['NAME'].'</category>' . "\n");
            $arrIdForYmlProducts[$arSection3['ID']] = $arSection3['ID'];//кладу в массив IDшники разрешенных секций

            $arFilter4 = array('IBLOCK_ID' => 1, 'SECTION_ID' => $arSection3['ID'], 'ACTIVE'=>'Y');
            $rsSections4 = CIBlockSection::GetList(array('LEFT_MARGIN' => 'ASC'), $arFilter4);
            while ($arSection4 = $rsSections4->Fetch())
            {
                if(in_array($arSection4['ID'],$sectionsNOYml)){continue;}
                fwrite($file, '<category id="'.$arSection4['ID'].'" parentId="'.$arSection4['IBLOCK_SECTION_ID'].'">'.$arSection4['NAME'].'</category>' . "\n");
                $arrIdForYmlProducts[$arSection4['ID']] = $arSection4['ID'];//кладу в массив IDшники разрешенных секций

                $arFilter5 = array('IBLOCK_ID' => 1, 'SECTION_ID' => $arSection4['ID'], 'ACTIVE'=>'Y');
                $rsSections5 = CIBlockSection::GetList(array('LEFT_MARGIN' => 'ASC'), $arFilter5);
                while ($arSection5 = $rsSections5->Fetch())
                {
                    if(in_array($arSection5['ID'],$sectionsNOYml)){continue;}
                    fwrite($file, '<category id="'.$arSection5['ID'].'" parentId="'.$arSection5['IBLOCK_SECTION_ID'].'">'.$arSection5['NAME'].'</category>' . "\n");
                    $arrIdForYmlProducts[$arSection5['ID']] = $arSection5['ID'];//кладу в массив IDшники разрешенных секций

                    $arFilter6 = array('IBLOCK_ID' => 1, 'SECTION_ID' => $arSection5['ID'], 'ACTIVE'=>'Y');
                    $rsSections6 = CIBlockSection::GetList(array('LEFT_MARGIN' => 'ASC'), $arFilter6);
                    while ($arSection6 = $rsSections6->Fetch())
                    {
                        if(in_array($arSection6['ID'],$sectionsNOYml)){continue;}
                        fwrite($file, '<category id="'.$arSection6['ID'].'" parentId="'.$arSection6['IBLOCK_SECTION_ID'].'">'.$arSection6['NAME'].'</category>' . "\n");
                        $arrIdForYmlProducts[$arSection6['ID']] = $arSection6['ID'];//кладу в массив IDшники разрешенных секций
}
}
}
}
}
}
}

foreach($arrIdForYmlProducts as $k => $v){//прогоняю массив айдишников категорий для очистки и сборки в массив
$finalIdForProductsAccess[] = $k;
}




fwrite($file, '</categories>
<offers>' . "\n");


//$arID = [1624, 1855, 1870, 615, 616];
$arSelect = Array("ID", "NAME","IBLOCK_SECTION_ID","DETAIL_TEXT", "DETAIL_PAGE_URL","DETAIL_PICTURE", "DATE_ACTIVE_FROM","PROPERTY_*");
//$arFilter = Array("IBLOCK_ID"=>1, "ACTIVE"=>"Y", "ID" => $arID);
$arFilter = Array("IBLOCK_ID"=>1, "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array("ID" => "ASC"), $arFilter, false, Array("nPageSize"=>60), $arSelect);
while($ob = $res->GetNextElement()){
$arFields = $ob->GetFields();
$arProps = $ob->GetProperties();



// echo '<pre>';
// print_r($arFields);
// print_r($arProps);
// echo '</pre>';

if($arProps['IS_PRESENT']['VALUE']){
$arrPresents[] = $arProps['IS_PRESENT']['VALUE'];
}



if(!in_array($arFields['IBLOCK_SECTION_ID'],$finalIdForProductsAccess)){continue;}






$PathImg = CFile::GetPath($arFields['DETAIL_PICTURE']);//превью картинка
$rsPrices = CPrice::GetList(array(), array('PRODUCT_ID' => $arFields['ID'], 'CATALOG_GROUP_ID' => 1));//цена
    $arPrice = $rsPrices->Fetch();



$ar_res = CCatalogProduct::GetByID($arFields['ID']);//получаю инфу о продукте в том числе НДС 


if($ar_res['QUANTITY'] > 0){
	$avaliable = 'true';
	$sales_notes = '<sales_notes>Наличными при получении, картой на сайте</sales_notes>';//Бесплатная доставка при заказе от 10 000 рублей.
	$delivery_options = '';
}else{
	$avaliable = 'false';
	$sales_notes = '<sales_notes>Предоплата картой на сайте, безналичный расчет</sales_notes>';
	if($arProps['DELIVERY_OPTIONS']['VALUE']){
		$delivery_options = '
<delivery>true</delivery>
<delivery-options>
	<option cost="700" days="'.$arProps['DELIVERY_OPTIONS']['VALUE'].'"/>
</delivery-options>';
	}
	//if($arProps['SALES_NOTES']['VALUE']){
	//	$sales_notes = '<sales_notes>'.$arProps['SALES_NOTES']['VALUE'].'</sales_notes>';	
	//}
}


if($ar_res['VAT_INCLUDED'] == 'N' and $ar_res['VAT_ID']){//если есть ндс и он не включен в стоимость
$proc = ($arPrice['PRICE'] / 100) * 18;
$arPrice['PRICE'] = $arPrice['PRICE'] + $proc;
}




$dbPrice = \CPrice::GetList(array(),array("PRODUCT_ID" => $arFields['ID'],"CATALOG_GROUP_ID" => 1));//беру цену товара со скидкой если она у него есть
  $arPriceDisc = $dbPrice->Fetch();
  $priceDisc = $arPriceDisc["PRICE"];
  
  // это список доступных скидок
  $arDiscounts = \CCatalogDiscount::GetDiscountByProduct($arFields['ID'],$USER->GetUserGroupArray(),"N",1,SITE_ID);//1 это ID цены
  if($arDiscounts) {
    $priceDisc = \CCatalogProduct::CountPriceWithDiscount(
        $arPriceDisc["PRICE"],
        $arPriceDisc["CURRENCY"],
        $arDiscounts
    );

    $oldPrice = $arPrice['PRICE'];

$arPrice['PRICE'] = $priceDisc;
  }











if($arPrice['CURRENCY'] == 'EUR'){//переводим из евро
$arFilterCurEUR = array("CURRENCY" => "EUR");
$byCurEUR = "date";
$orderCurEUR = "desc";
$rowCurEUR = CCurrencyRates::GetList($byCurEUR, $orderCurEUR, $arFilterCurEUR)->Fetch();

$arPrice['PRICE'] = $arPrice['PRICE'] * $rowCurEUR['RATE'];

if($ar_res['VAT_INCLUDED'] == 'N' and $ar_res['VAT_ID']){//если есть ндс и он не включен в стоимость

    $proc = ($arPrice['PRICE'] / 100) * 18;
    $arPrice['PRICE'] = $arPrice['PRICE'] + $proc;
    }

$oldPrice = $oldPrice * $rowCurEUR['RATE'];
}


if($arPrice['CURRENCY'] == 'USD'){//переводим из долларов
$arFilterCurUSD = array("CURRENCY" => "USD");
$byCurUSD = "date";
$orderCurUSD = "desc";
$rowCurUSD = CCurrencyRates::GetList($byCurUSD, $orderCurUSD, $arFilterCurUSD)->Fetch();
$arPrice['PRICE'] = $arPrice['PRICE'] * $rowCurUSD['RATE'];

if($ar_res['VAT_INCLUDED'] == 'N' and $ar_res['VAT_ID']){//если есть ндс и он не включен в стоимость

    $proc = ($arPrice['PRICE'] / 100) * 18;
    $arPrice['PRICE'] = $arPrice['PRICE'] + $proc;
    }

$oldPrice = $oldPrice * $rowCurUSD['RATE'];
}


if($oldPrice){$oldPrice1 = '<oldprice>'.floor($oldPrice).'</oldprice>';}
if($arProps['PROP_TYPE_PREFIX']['VALUE']){
    $typePrefix = '<typePrefix>'.$arProps['PROP_TYPE_PREFIX']['VALUE'].'</typePrefix>';
}else{
    $typePrefix = '<typePrefix>'.implode(' ', (array_slice(explode(' ', $arFields['NAME']), 0, 2))).'</typePrefix>';
}


$arPrice['PRICE'] = floor($arPrice['PRICE']);//округляем в меньшую сторону
if($arPrice['PRICE'] > 999999){continue;}
if($arPrice['PRICE'] < 5){continue;}

if($arProps['ARTICUL']['VALUE']){$prop000 = ''.$arProps['ARTICUL']['VALUE'].'';}

if($arProps['COUNTRY_OF_ORIGIN']['VALUE']){$prop00 = ''.$arProps['COUNTRY_OF_ORIGIN']['VALUE'].''; $propParam00 = '<param name="'.$arProps['COUNTRY_OF_ORIGIN']['NAME'].'">'.$arProps['COUNTRY_OF_ORIGIN']['VALUE'].'</param>';}

if($arProps['ATT_PROIZV_OBOGREV']['VALUE']){$prop0 = ''.$arProps['ATT_PROIZV_OBOGREV']['NAME'].': '.$arProps['ATT_PROIZV_OBOGREV']['VALUE'].'';$propParam0 = '<param name="'.$arProps['ATT_PROIZV_OBOGREV']['NAME'].'">'.$arProps['ATT_PROIZV_OBOGREV']['VALUE'].'</param>';}

if($arProps['ATT_PROIZV_OHLOGD']['VALUE']){$prop1 = ''.$arProps['ATT_PROIZV_OHLOGD']['NAME'].': '.$arProps['ATT_PROIZV_OHLOGD']['VALUE'].'';$propParam1 = '<param name="'.$arProps['ATT_PROIZV_OHLOGD']['NAME'].'">'.$arProps['ATT_PROIZV_OHLOGD']['VALUE'].'</param>';}

if($arProps['ATT_GUARANTY']['VALUE']){$prop2 = ''.$arProps['ATT_GUARANTY']['NAME'].': '.$arProps['ATT_GUARANTY']['VALUE'].'';$propParam2 = '<param name="'.$arProps['ATT_GUARANTY']['NAME'].'">'.$arProps['ATT_GUARANTY']['VALUE'].'</param>';}

if($arProps['ATT_PLOCHAD_OBS']['VALUE']){$prop3 = ''.$arProps['ATT_PLOCHAD_OBS']['NAME'].': '.$arProps['ATT_PLOCHAD_OBS']['VALUE'].'';$propParam3 = '<param name="'.$arProps['ATT_PLOCHAD_OBS']['NAME'].'">'.$arProps['ATT_PLOCHAD_OBS']['VALUE'].'</param>';}

if($arProps['ATT_AUTOREGIM']['VALUE']){$prop4 = ''.$arProps['ATT_AUTOREGIM']['NAME'].': '.$arProps['ATT_AUTOREGIM']['VALUE'].'';$propParam4 = '<param name="'.$arProps['ATT_AUTOREGIM']['NAME'].'">'.$arProps['ATT_AUTOREGIM']['VALUE'].'</param>';}

if($arProps['ATT_SAMODIAGNOSTIK']['VALUE']){$prop5 = ''.$arProps['ATT_SAMODIAGNOSTIK']['NAME'].': '.$arProps['ATT_SAMODIAGNOSTIK']['VALUE'].'';$propParam5 = '<param name="'.$arProps['ATT_SAMODIAGNOSTIK']['NAME'].'">'.$arProps['ATT_SAMODIAGNOSTIK']['VALUE'].'</param>';}

if($arProps['ATT_SIZES_OUT']['VALUE']){$prop6 = ''.$arProps['ATT_SIZES_OUT']['NAME'].': '.$arProps['ATT_SIZES_OUT']['VALUE'].'';$propParam6 = '<param name="'.$arProps['ATT_SIZES_OUT']['NAME'].'">'.$arProps['ATT_SIZES_OUT']['VALUE'].'</param>';}

if($arProps['ATT_SIZES_IN']['VALUE']){$prop7 = ''.$arProps['ATT_SIZES_IN']['NAME'].': '.$arProps['ATT_SIZES_IN']['VALUE'].'';$propParam7 = '<param name="'.$arProps['ATT_SIZES_IN']['NAME'].'">'.$arProps['ATT_SIZES_IN']['VALUE'].'</param>';}

if($arProps['ATT_WORKS_TEMRERAT']['VALUE']){$prop8 = ''.$arProps['ATT_WORKS_TEMRERAT']['NAME'].': '.$arProps['ATT_WORKS_TEMRERAT']['VALUE'].'';$propParam8 = '<param name="'.$arProps['ATT_WORKS_TEMRERAT']['NAME'].'">'.$arProps['ATT_WORKS_TEMRERAT']['VALUE'].'</param>';}

if($arProps['ATT_MATERIAL']['VALUE']){$prop9 = ''.$arProps['ATT_MATERIAL']['NAME'].': '.$arProps['ATT_MATERIAL']['VALUE'].'';$propParam9 = '<param name="'.$arProps['ATT_MATERIAL']['NAME'].'">'.$arProps['ATT_MATERIAL']['VALUE'].'</param>';}

if($arProps['ATT_MONTAGNOE_POADK']['VALUE']){$prop10 = ''.$arProps['ATT_MONTAGNOE_POADK']['NAME'].': '.$arProps['ATT_MONTAGNOE_POADK']['VALUE'][0].'';
    $propParam10 = '<param name="'.$arProps['ATT_MONTAGNOE_POADK']['NAME'].'">'.$arProps['ATT_MONTAGNOE_POADK']['VALUE'][0].'</param>';}

if($arProps['COLOR']['VALUE']){$prop11 = ''.$arProps['COLOR']['NAME'].': '.$arProps['COLOR']['VALUE'].'';$propParam11 = '<param name="'.$arProps['COLOR']['NAME'].'">'.$arProps['COLOR']['VALUE'].'</param>';}

if($arProps['PROP_MATERIAL_30']['VALUE']){$prop12 = ''.$arProps['PROP_MATERIAL_30']['NAME'].': '.$arProps['PROP_MATERIAL_30']['VALUE'].'';$propParam12 = '<param name="'.$arProps['PROP_MATERIAL_30']['NAME'].'">'.$arProps['PROP_MATERIAL_30']['VALUE'].'</param>';}

if($arProps['PROP_MATERIAL_23']['VALUE']){$prop13 = ''.$arProps['PROP_MATERIAL_23']['NAME'].': '.$arProps['PROP_MATERIAL_23']['VALUE'].'';$propParam13 = '<param name="'.$arProps['PROP_MATERIAL_23']['NAME'].'">'.$arProps['PROP_MATERIAL_23']['VALUE'].'</param>';}

if($arProps['PROP_MATERIAL_26']['VALUE']){$prop14 = ''.$arProps['PROP_MATERIAL_26']['NAME'].': '.$arProps['PROP_MATERIAL_26']['VALUE'].'';$propParam14 = '<param name="'.$arProps['PROP_MATERIAL_26']['NAME'].'">'.$arProps['PROP_MATERIAL_26']['VALUE'].'</param>';}

if($arProps['SECTION_QT']['VALUE']){$prop15 = ''.$arProps['SECTION_QT']['NAME'].': '.$arProps['SECTION_QT']['VALUE'].'';$propParam15 = '<param name="'.$arProps['SECTION_QT']['NAME'].'">'.$arProps['SECTION_QT']['VALUE'].'</param>';}

if($arProps['NN_SIZES']['VALUE']){$prop16 = ''.$arProps['NN_SIZES']['NAME'].': '.$arProps['NN_SIZES']['VALUE'].''; $propParam16 = '<param name="'.$arProps['NN_SIZES']['NAME'].'">'.$arProps['NN_SIZES']['VALUE'].'</param>';}

if($arProps['PROP_MATERIAL_19']['VALUE']){$prop17 = ''.$arProps['PROP_MATERIAL_19']['NAME'].': '.$arProps['PROP_MATERIAL_19']['VALUE'].'';$propParam17 = '<param name="'.$arProps['PROP_MATERIAL_19']['NAME'].'">'.$arProps['PROP_MATERIAL_19']['VALUE'].'</param>';}

if($arProps['ATT_MASH_NAGREVA']['VALUE']){$prop18 = ''.$arProps['ATT_MASH_NAGREVA']['NAME'].': '.$arProps['ATT_MASH_NAGREVA']['VALUE'].'';$propParam18 = '<param name="'.$arProps['ATT_MASH_NAGREVA']['NAME'].'">'.$arProps['ATT_MASH_NAGREVA']['VALUE'].'</param>';}

if($arProps['WEIGHT_PROP']['VALUE']){$prop19 = ''.$arProps['WEIGHT_PROP']['NAME'].': '.$arProps['WEIGHT_PROP']['VALUE'].'';$propParam19 = '<param name="'.$arProps['WEIGHT_PROP']['NAME'].'">'.$arProps['WEIGHT_PROP']['VALUE'].'</param>';}

if($arProps['PROP_MATERIAL_14']['VALUE']){$prop20 = ''.$arProps['PROP_MATERIAL_14']['NAME'].': '.$arProps['PROP_MATERIAL_14']['VALUE'].'';$propParam20 = '<param name="'.$arProps['PROP_MATERIAL_14']['NAME'].'">'.$arProps['PROP_MATERIAL_14']['VALUE'].'</param>';}

if($arProps['PROP_MATERIAL_15']['VALUE']){$prop21 = ''.$arProps['PROP_MATERIAL_15']['NAME'].': '.$arProps['PROP_MATERIAL_15']['VALUE'].'';$propParam21 = '<param name="'.$arProps['PROP_MATERIAL_15']['NAME'].'">'.$arProps['PROP_MATERIAL_15']['VALUE'].'</param>';}

if($arProps['NN_PREDNAZ']['VALUE']){$prop22 = ''.$arProps['NN_PREDNAZ']['NAME'].': '.implode(", ", $arProps['NN_PREDNAZ']['VALUE']).'';$propParam22 = '<param name="'.$arProps['NN_PREDNAZ']['NAME'].'">'.implode(", ", $arProps['NN_PREDNAZ']['VALUE']).'</param>';}

if($arProps['PROP_MATERIAL_4']['VALUE']){$prop23 = ''.$arProps['PROP_MATERIAL_4']['NAME'].': '.$arProps['PROP_MATERIAL_4']['VALUE'].'';$propParam23 = '<param name="'.$arProps['PROP_MATERIAL_4']['NAME'].'">'.$arProps['PROP_MATERIAL_4']['VALUE'].'</param>';}

if($arProps['PROP_MATERIAL_6']['VALUE']){$prop24 = ''.$arProps['PROP_MATERIAL_6']['NAME'].': '.$arProps['PROP_MATERIAL_6']['VALUE'].'';$propParam24 = '<param name="'.$arProps['PROP_MATERIAL_6']['NAME'].'">'.$arProps['PROP_MATERIAL_6']['VALUE'].'</param>';}

if($arProps['ATT_POVERHNOST']['VALUE']){$prop25 = ''.$arProps['ATT_POVERHNOST']['NAME'].': '.$arProps['ATT_POVERHNOST']['VALUE'].'';$propParam25 = '<param name="'.$arProps['ATT_POVERHNOST']['NAME'].'">'.$arProps['ATT_POVERHNOST']['VALUE'].'</param>';}

if($arProps['ATT_NIGHT_REGIM']['VALUE']){$prop26 = ''.$arProps['ATT_NIGHT_REGIM']['NAME'].': '.$arProps['ATT_NIGHT_REGIM']['VALUE'].'';$propParam26 = '<param name="'.$arProps['ATT_NIGHT_REGIM']['NAME'].'">'.$arProps['ATT_NIGHT_REGIM']['VALUE'].'</param>';}

if($arProps['ATT_AUTOPEREZ']['VALUE']){$prop27 = ''.$arProps['ATT_AUTOPEREZ']['NAME'].': '.$arProps['ATT_AUTOPEREZ']['VALUE'].'';$propParam27 = '<param name="'.$arProps['ATT_AUTOPEREZ']['NAME'].'">'.$arProps['ATT_AUTOPEREZ']['VALUE'].'</param>';}

if($arProps['ATT_FILTERS_CLEAR']['VALUE']){$prop28 = ''.$arProps['ATT_FILTERS_CLEAR']['NAME'].': '.$arProps['ATT_FILTERS_CLEAR']['VALUE'].'';$propParam28 = '<param name="'.$arProps['ATT_FILTERS_CLEAR']['NAME'].'">'.$arProps['ATT_FILTERS_CLEAR']['VALUE'].'</param>';}

if($arProps['ATT_SET_PODKL']['VALUE']){$prop29 = ''.$arProps['ATT_SET_PODKL']['NAME'].': '.$arProps['ATT_SET_PODKL']['VALUE'].'';$propParam29 = '<param name="'.$arProps['ATT_SET_PODKL']['NAME'].'">'.$arProps['ATT_SET_PODKL']['VALUE'].'</param>';}

if($arProps['ATT_PROIZV_MCHAS']['VALUE']){$prop30 = ''.$arProps['ATT_PROIZV_MCHAS']['NAME'].': '.$arProps['ATT_PROIZV_MCHAS']['VALUE'].'';$propParam30 = '<param name="'.$arProps['ATT_PROIZV_MCHAS']['NAME'].'">'.$arProps['ATT_PROIZV_MCHAS']['VALUE'].'</param>';}

if($arProps['ATT_UROVEN_SHUMA']['VALUE']){$prop31 = ''.$arProps['ATT_UROVEN_SHUMA']['NAME'].': '.$arProps['ATT_UROVEN_SHUMA']['VALUE'].'';$propParam31 = '<param name="'.$arProps['ATT_UROVEN_SHUMA']['NAME'].'">'.$arProps['ATT_UROVEN_SHUMA']['VALUE'].'</param>';}

if($arProps['ATT_SKOROSTI_VENT']['VALUE']){$prop32 = ''.$arProps['ATT_SKOROSTI_VENT']['NAME'].': '.$arProps['ATT_SKOROSTI_VENT']['VALUE'].'';$propParam32 = '<param name="'.$arProps['ATT_SKOROSTI_VENT']['NAME'].'">'.$arProps['ATT_SKOROSTI_VENT']['VALUE'].'</param>';}


if($arProps['CML2_ATTRIBUTES']['VALUE']){

for($s=0;$s<count($arProps['CML2_ATTRIBUTES']['VALUE']);$s++){
    $propHAR[$s] = ''.$arProps['CML2_ATTRIBUTES']['VALUE'][$s].': '.$arProps['CML2_ATTRIBUTES']['DESCRIPTION'][$s].'';

    $propHARparam[$s] = '<param name="'.$arProps['CML2_ATTRIBUTES']['VALUE'][$s].'">'.$arProps['CML2_ATTRIBUTES']['DESCRIPTION'][$s].'</param>';
}

}

   
    // $description = strip_tags($arFields['DETAIL_TEXT']);//обрезаю текста до 3000 символов и последней запятой
    // $countWordsInString = (int)iconv_strlen($description,'UTF-8');
    // if($countWordsInString > 3000){
    //     $description = substr(strip_tags($arFields['DETAIL_TEXT']), 0, 2999);
    //     $description = substr($description, 0, strrpos($description, '.') + 1);
    // } <model>'.$prop000.'</model>





fwrite($file, '<offer id="'.$arFields['ID'].'" type="vendor.model" available="'.$avaliable.'">
<url>https://'.$_SERVER['SERVER_NAME'].''.$arFields['DETAIL_PAGE_URL'].'?utm_source=market.yandex.ru&utm_term='.$arFields['ID'].'</url>
<price>'.$arPrice['PRICE'].'</price>
'.$oldPrice1.'
<currencyId>RUR</currencyId>
<categoryId>'.$arFields['IBLOCK_SECTION_ID'].'</categoryId>
'.$delivery_options.'
'.$sales_notes.'
<manufacturer_warranty>true</manufacturer_warranty>
<country_of_origin>'.$prop00.'</country_of_origin>
<picture>https://'.$_SERVER['SERVER_NAME'].''.$PathImg.'</picture>
<vendor>'.$arFields2['NAME'].'</vendor>
<vendorCode>'.$prop000.'</vendorCode>
'.$typePrefix.'
<model>'.$arFields['NAME'].'</model>
<description>

'.$prop0.'
'.$prop1.'
'.$prop3.'
'.$prop4.'
'.$prop5.'
'.$prop6.'
'.$prop7.'
'.$prop8.'
'.$prop9.'
'.$prop10.'
'.$prop11.'
'.$prop12.'
'.$prop13.'
'.$prop14.'
'.$prop15.'
'.$prop16.'
'.$prop17.'
'.$prop18.'
'.$prop19.'
'.$prop20.'
'.$prop21.'
'.$prop22.'
'.$prop23.'
'.$prop24.'
'.$prop25.'
'.$prop26.'
'.$prop27.'
'.$prop28.'
'.$prop29.'
'.$prop30.'
'.$prop31.'
'.$prop32.'
'.$propHAR[0].'
'.$propHAR[1].'
'.$propHAR[2].'
'.$propHAR[3].'
'.$propHAR[4].'
'.$propHAR[5].'
'.$propHAR[6].'
'.$propHAR[7].'
</description>


'.$propParam00.'
'.$propParam0.'
'.$propParam1.'
'.$propParam2.'
'.$propParam3.'
'.$propParam4.'
'.$propParam5.'
'.$propParam6.'
'.$propParam7.'
'.$propParam8.'
'.$propParam9.'
'.$propParam10.'
'.$propParam11.'
'.$propParam12.'
'.$propParam13.'
'.$propParam14.'
'.$propParam15.'
'.$propParam16.'
'.$propParam17.'
'.$propParam18.'
'.$propParam19.'
'.$propParam20.'
'.$propParam21.'
'.$propParam22.'
'.$propParam23.'
'.$propParam24.'
'.$propParam25.'
'.$propParam26.'
'.$propParam27.'
'.$propParam28.'
'.$propParam29.'
'.$propParam30.'
'.$propParam31.'
'.$propParam32.'
'.$propHARparam[0].'
'.$propHARparam[1].'
'.$propHARparam[2].'
'.$propHARparam[3].'
'.$propHARparam[4].'
'.$propHARparam[5].'
'.$propHARparam[6].'
'.$propHARparam[7].'
</offer>
' . "\n");
unset($propParam00);
unset($propParam0);
unset($propParam1);
unset($propParam2);
unset($propParam3);
unset($propParam4);
unset($propParam5);
unset($propParam6);
unset($propParam7);
unset($propParam8);
unset($propParam9);
unset($propParam10);
unset($propParam11);
unset($propParam12);
unset($propParam13);
unset($propParam14);
unset($propParam15);
unset($propParam16);
unset($propParam17);
unset($propParam18);
unset($propParam19);
unset($propParam20);
unset($propParam21);
unset($propParam22);
unset($propParam23);
unset($propParam24);
unset($propParam25);
unset($propParam26);
unset($propParam27);
unset($propParam28);
unset($propParam29);
unset($propParam30);
unset($propParam31);
unset($propParam32);
unset($propHARparam[0]);
unset($propHARparam[1]);
unset($propHARparam[2]);
unset($propHARparam[3]);
unset($propHARparam[4]);
unset($propHARparam[5]);
unset($propHARparam[6]);
unset($propHARparam[7]);



unset($prop000);
unset($prop00);
unset($prop0);
unset($prop1);
unset($prop2);
unset($prop3);
unset($prop4);
unset($prop5);
unset($prop6);
unset($prop7);
unset($prop8);
unset($prop9);
unset($prop10);
unset($prop11);
unset($prop12);
unset($prop13);
unset($prop14);
unset($prop15);
unset($prop16);
unset($prop17);
unset($prop18);
unset($prop19);
unset($prop20);
unset($prop21);
unset($prop22);
unset($prop23);
unset($prop24);
unset($prop25);
unset($prop26);
unset($prop27);
unset($prop28);
unset($prop29);
unset($prop30);
unset($prop31);
unset($prop32);
unset($propHAR[0]);
unset($propHAR[1]);
unset($propHAR[2]);
unset($propHAR[3]);
unset($propHAR[4]);
unset($propHAR[5]);
unset($propHAR[6]);
unset($propHAR[7]);

unset($oldPrice);
unset($oldPrice1);
unset($typePrefix);
}



$arrPresentsFin = array_unique($arrPresents);

// echo '<pre>';
// print_r($arrPresentsFin);
// echo '</pre>';

foreach($arrPresentsFin as $k => $v){
$count[]=$v;

$promos1 = '
<promo id="'.count($count).'" type="gift with purchase">
<purchase>
<required-quantity>1</required-quantity>';

$arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_*");
$arFilter = Array("IBLOCK_ID"=>1,array('PROPERTY_IS_PRESENT' => $v),"ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
while($ob = $res->GetNextElement()){
$arFields = $ob->GetFields();
$arProps = $ob->GetProperties();
// echo '<pre>';
// print_r($arFields);
// echo '</pre>';



$promos2[] = '<product offer-id="'.$arFields['ID'].'"/>'. "\n";



}

$promos3 = '</purchase>
<promo-gifts>
<promo-gift offer-id="'.$v.'"/>
</promo-gifts>
</promo>';


$sdsfsdfsd[] = ''.$promos1.'
'.implode('',$promos2).'
'.$promos3.'';

unset($promos1);
unset($promos2);
unset($promos3);
}

fwrite($file, '</offers>
<promos>



'.implode('',$sdsfsdfsd).'






</promos>
</shop>
</yml_catalog>
' . "\n");

fclose($file);






$base = file_get_contents("newyml.xml");
$base = str_replace("&","&amp;",$base);
$base = rtrim(preg_replace("/[\r\n]+/m","\r\n", $base));
$base = str_replace("&nbsp;","",$base);
$base = str_replace("<br>","<br />",$base);
$fp = fopen("newyml.xml","w");
fwrite($fp, "$base");
fclose($fp);

