<?
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

set_time_limit(0);
$_SERVER["DOCUMENT_ROOT"] = "/home/o/onlineclim/irsapshop.ru/public_html";
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");


class CSettings
{
    static function PriceFormat($price)
    {
        return "<span>".number_format($price, 0, ".", " ")."</span> руб.";
    }

    static function PriceConvert($price, $currency)
    {
        $price = CCurrencyRates::ConvertCurrency($price, $currency, "RUB");
        
       // if ($price > 100)
        //{
            //цена заканчивается на 0 и без копеек
            //$price = intval($price / 10) * 10;
       // }

        //Bitrix\Catalog\Product\Price::roundValue

        return $price;
    }
}

if (!function_exists("yandex_text2xml"))
{
    function yandex_text2xml($text, $bHSC = false, $bDblQuote = false)
    {
        global $APPLICATION;

        $bHSC = (true == $bHSC ? true : false);
        $bDblQuote = (true == $bDblQuote ? true: false);

        if ($bHSC)
        {
            $text = htmlspecialcharsbx($text);
            if ($bDblQuote)
                $text = str_replace('&quot;', '"', $text);
        }
        $text = preg_replace("/[\x1-\x8\xB-\xC\xE-\x1F]/", "", $text);
        $text = str_replace("'", "&apos;", $text);
        $text = $APPLICATION->ConvertCharset($text, LANG_CHARSET, 'windows-1251');
        return $text;
    }
}

CModule::IncludeModule("iblock");

$arResult = array();
$arID = array();

$rsSection = CIBlockSection::GetList(array("left_margin"=>"asc"), array("IBLOCK_ID"=>2, "ACTIVE"=>"Y"));
while ($arSection = $rsSection->Fetch())
{
	$arResult['CATEGORIES'][$arSection["ID"]] = array(
		"ID" => $arSection["ID"],
		"IBLOCK_SECTION_ID" => $arSection["IBLOCK_SECTION_ID"],
		"NAME" => $arSection["NAME"]
	);
}

$rsElement = CIBlockElement::GetList(array(), array("IBLOCK_ID"=>1, "ACTIVE"=>"Y"), false,  Array("nPageSize"=>1000),
 array("ID", "NAME", "IBLOCK_ID", "IBLOCK_SECTION_ID", "DETAIL_PICTURE", "DETAIL_PAGE_URL", "PROPERTY_DETAIL_PICTURE_DESC_TOP", "PROPERTY_COLLECTION", "PROPERTY_FUNCTIONALITY"));
while ($arElement = $rsElement->GetNext())
{
   // if (isset($arResult['CATEGORIES'][$arElement['IBLOCK_SECTION_ID']]))
   // {
        $arElement['DETAIL_PICTURE'] = CFile::GetFileArray($arElement['DETAIL_PICTURE']);
        $arElement['AVAILABLE'] = 1;

        $arResult['ITEMS'][$arElement['ID']] = $arElement;

        $arID[] = $arElement['ID'];
  //  }
}

if (count($arID) > 0)
{
	$rsOffers = CIBlockElement::GetList(array(), array("IBLOCK_ID"=>2, "ACTIVE"=>"Y", "PROPERTY_CML2_LINK"=>$arID, ">CATALOG_PRICE_1"=>0, ">CATALOG_QUANTITY"=>0), 
	false,  Array("nPageSize"=>6000), array("ID", "NAME", "CATALOG_GROUP_1", "IBLOCK_SECTION_ID",
	"PREVIEW_TEXT", "PREVIEW_PICTURE", "PROPERTY_CML2_LINK", 
	"PROPERTY_ARTNUMBER", "PROPERTY_KOLICHESTVO_SEKCIY", "PROPERTY_VISOTA", "PROPERTY_TIP_PODCKLUCHENIA"));
	while ($arOffers = $rsOffers->GetNext())
	{

		$arElement = $arResult['ITEMS'][$arOffers['PROPERTY_CML2_LINK_VALUE']];

		$arOffers['PRICE'] = CSettings::PriceConvert($arOffers['CATALOG_PRICE_1'], $arOffers['CATALOG_CURRENCY_1']);
		//$arOffers['PRICE'] = number_format($arOffers['PRICE'], 0, ".", "");
        $arOffers['PRICE'] = number_format($arOffers['PRICE'], 2, ".", "");
		
		$arOffers['DETAIL_PAGE_URL'] = $arElement['DETAIL_PAGE_URL']."?OFFER_ID=".$arOffers['ID'];
		$arOffers['IBLOCK_SECTION_ID'] = $arOffers['IBLOCK_SECTION_ID'];
		$arOffers['PREVIEW_TEXT'] = $arOffers['PREVIEW_TEXT'];
		$arOffers['DETAIL_PICTURE'] = CFile::GetFileArray($arOffers['PREVIEW_PICTURE']);
		if(!empty($arOffers['PREVIEW_PICTURE'])){
			$arOffers['DETAIL_PICTURE'] = CFile::GetFileArray($arOffers['PREVIEW_PICTURE']);
		}else{
			$arOffers['DETAIL_PICTURE'] = $arElement['DETAIL_PICTURE'];
		}		
		
		
		$arOffers['ARTNUMBER'] = $arOffers['PROPERTY_ARTNUMBER_VALUE'];
		$arOffers['KOLICHESTVO_SEKCIY'] = $arOffers['PROPERTY_KOLICHESTVO_SEKCIY_VALUE'];
		$arOffers['VISOTA'] = $arOffers['PROPERTY_VISOTA_VALUE'];
		$arOffers['TIP_PODCKLUCHENIA'] = $arOffers['PROPERTY_TIP_PODCKLUCHENIA_VALUE'];
		
		$arOffers['COLLECTION'] = $arElement['PROPERTY_COLLECTION_VALUE'];
		$arOffers['FUNCTIONALITY'] = $arElement['PROPERTY_FUNCTIONALITY_VALUE'];
		$arOffers['DETAIL_PICTURE_DESC_TOP'] = $arElement['PROPERTY_DETAIL_PICTURE_DESC_TOP_VALUE'];
		



		//$ar_res = CCatalogProduct::GetByID($arOffers['ID']);//получаю инфу о продукте в том числе НДС 
		//if($ar_res['QUANTITY'] > 0){
		if($arOffers['CATALOG_QUANTITY'] > 0){
			$arOffers['AVALIABLE'] = 'true';
			$arOffers['SALES_NOTES'] = 'Наличными при получении, картой на сайте';
			$arOffers['DELIVERY_OPTIONS'] = '';
			
			$avaliable = 'true';
			$sales_notes = '<sales_notes>Наличными при получении, картой на сайте</sales_notes>';//Бесплатная доставка при заказе от 10 000 рублей.
			$delivery_options = '';
		}else{
			$arOffers['AVALIABLE'] = 'false';
			$arOffers['SALES_NOTES'] = 'Предоплата картой на сайте, безналичный расчет';
			$arOffers['DELIVERY_OPTIONS'] = '';
		}		
		
		if (!isset($arResult['ITEMS'][$arOffers['PROPERTY_CML2_LINK_VALUE']]["PRICE"]) || $arResult['ITEMS'][$arOffers['PROPERTY_CML2_LINK_VALUE']]["PRICE"] > $arOffers['PRICE'])
		{
			$arResult['ITEMS'][$arOffers['PROPERTY_CML2_LINK_VALUE']]['PRICE'] = $arOffers['PRICE'];
		}
		
		$arResult['OFFERS'][] = $arOffers;
	}
}


$strTmpCat = "";
foreach ($arResult['CATEGORIES'] as $arCategory)
{
	$strTmpCat.= "<category id=\"".$arCategory["ID"]."\"".(intval($arCategory["IBLOCK_SECTION_ID"])>0?" parentId=\"".$arCategory["IBLOCK_SECTION_ID"]."\"":"").">".yandex_text2xml($arCategory["NAME"], true)."</category>\n";
}
		
$strTmpOff = "";

/*foreach ($arResult['ITEMS'] as $id=>$arElement)
{
	if ($arElement['PRICE'] > 0)
	{
		$available = "true";
		
		$strTmpOff.= "<offer id=\"".$arElement["ID"]."\" available=\"".$available."\">\n";
			$strTmpOff.= "<url>https://irsapshop.ru".$arElement['DETAIL_PAGE_URL']."</url>\n";
			$strTmpOff.= "<price>".$arElement['PRICE']."</price>\n";
			$strTmpOff.= "<currencyId>RUB</currencyId>\n";
			$strTmpOff.= "<categoryId>".$arElement['IBLOCK_SECTION_ID']."</categoryId>\n";
			$strTmpOff.= "<delivery>true</delivery>\n";
			$strTmpOff.= "<picture>https://irsapshop.ru".$arElement['DETAIL_PICTURE']['SRC']."</picture>\n";
			$strTmpOff.= "<name>".yandex_text2xml($arElement['NAME'], true)."</name>\n";
		$strTmpOff.= "</offer>\n";
	}
}
*/

foreach ($arResult['OFFERS'] as $id=>$arElement)
{
	if ($arElement['PRICE'] > 0)
	{
		$available = $arElement['AVALIABLE'];
			

		
		
		if(!empty($arElement['DETAIL_PICTURE_DESC_TOP'])){
			$arElement['NAME'] = $arElement['DETAIL_PICTURE_DESC_TOP']." ".$arElement['NAME'];
		}else{
				
							
			//1) Радиаторы , водяные = "Стальной трубчатый радиатор Irsap название тп"
			//2) Радиаторы , Электрические = ?
			//3) дизайн, водяные = ?
			//4) дизайн, электрические = ?
			//5) Полотенцесушители" и "Электрические модели" = "Стальной трубчатый радиатор Irsap название тп"
			//6)"Полотенцесушители" и "Водяные модели" = "Водяной полотенцесушитель Irsap название тп"

		
			/*if($arElement['COLLECTION'] == "Радиаторы" && $arElement['FUNCTIONALITY'] == "Водяные модели"){
				$arElement['NAME'] = "Стальной трубчатый радиатор Irsap ".$arElement['NAME'];
				
			}elseif($arElement['COLLECTION'] == "Полотенцесушители" && $arElement['FUNCTIONALITY'] == "Водяные модели"){
				$arElement['NAME'] = "Водяной полотенцесушитель Irsap ".$arElement['NAME'];
				
			}elseif($arElement['COLLECTION'] == "Полотенцесушители" && $arElement['FUNCTIONALITY'] == "Электрические модели"){
				$arElement['NAME'] = "Электрический полотенцесушитель Irsap ".$arElement['NAME'];
			
			}elseif($arElement['COLLECTION'] == "Полотенцесушители"){
				$arElement['NAME'] = "Полотенцесушитель Irsap ".$arElement['NAME'];
			}
			elseif($arElement['COLLECTION'] == "Радиаторы"){
				$arElement['NAME'] = "Радиатор Irsap ".$arElement['NAME'];
			}*/
			
		}


        global $USER;
        $productID = $arElement["ID"];
        $quantity = 1;
        $renewal = 'N';
        $arPrice = CCatalogProduct::GetOptimalPrice(
            $productID,
            $quantity,
            $USER->GetUserGroupArray(),
            $renewal
        );

        if (!$arPrice || count($arPrice) <= 0) {
            if ($nearestQuantity = CCatalogProduct::GetNearestQuantityPrice($productID, $quantity, $USER->GetUserGroupArray())) {
                $quantity = $nearestQuantity;
                $arPrice = CCatalogProduct::GetOptimalPrice($productID, $quantity, $USER->GetUserGroupArray(), $renewal);
            }
        }

        $discountPrice = $arPrice["DISCOUNT_PRICE"];
        $resultPrice = round($discountPrice / 0.8695651951488153, 2);

	    //echo "<pre>";
	    //var_dump($arElement);
	    //echo "</pre>";
			
		$strTmpOff.= "<offer id=\"".$arElement["ID"]."\" available=\"".$available."\">\n";
			$strTmpOff.= "<url>https://irsapshop.ru".$arElement['DETAIL_PAGE_URL']."</url>\n";
			$strTmpOff.= "<price>".$discountPrice."</price>\n";
			$strTmpOff.= "<currencyId>RUB</currencyId>\n";
			$strTmpOff.= "<categoryId>".$arElement['IBLOCK_SECTION_ID']."</categoryId>\n";
			$strTmpOff.= "<vendorCode>".$arElement['ARTNUMBER']."</vendorCode>\n";
			$strTmpOff.= "<vendor>".yandex_text2xml('Irsap', true)."</vendor>\n";
			$strTmpOff.= "<delivery>true</delivery>\n";
			$strTmpOff.= "<sales_notes>".yandex_text2xml($arElement['SALES_NOTES'], true)."</sales_notes>\n";
			$strTmpOff.= "<picture>https://irsapshop.ru".$arElement['DETAIL_PICTURE']['SRC']."</picture>\n";
			$strTmpOff.= "<name>".yandex_text2xml($arElement['NAME'], true)."</name>\n";
			$strTmpOff.= "<model>".yandex_text2xml($arElement['NAME'], true)."</model>\n";
			$strTmpOff.= "<description>".yandex_text2xml($arElement['PREVIEW_TEXT'], true)."</description>\n";

			if(!empty($arElement['VISOTA']))
				$strTmpOff.= "<param name=\"".yandex_text2xml('Высота', true)."\">".$arElement['VISOTA']."</param>\n";
			if(!empty($arElement['KOLICHESTVO_SEKCIY']))
				$strTmpOff.= "<param name=\"".yandex_text2xml('Количество секций', true)."\">".$arElement['KOLICHESTVO_SEKCIY']."</param>\n";
			if(!empty($arElement['TIP_PODCKLUCHENIA']))
				$strTmpOff.= "<param name=\"".yandex_text2xml('Тип подключения', true)."\">".yandex_text2xml($arElement['TIP_PODCKLUCHENIA'], true)."</param>\n";
		
		if(!empty($arElement['COLLECTION']))
				$strTmpOff.= "<param name=\"".yandex_text2xml('Коллекции', true)."\">".yandex_text2xml($arElement['COLLECTION'], true)."</param>\n";
			if(!empty($arElement['FUNCTIONALITY']))
				$strTmpOff.= "<param name=\"".yandex_text2xml('Функционирование', true)."\">".yandex_text2xml($arElement['FUNCTIONALITY'], true)."</param>\n";
		
		$strTmpOff.= "</offer>\n";
	}
}
		
$fp = fopen($_SERVER["DOCUMENT_ROOT"]."/yml/newyml.xml", "wb");
@fwrite($fp, '<?xml version="1.0" encoding="windows-1251"?>');
@fwrite($fp, "\n<!DOCTYPE yml_catalog SYSTEM \"shops.dtd\">\n");

@fwrite($fp, "<yml_catalog date=\"".date("Y-m-d H:i")."\">\n");
@fwrite($fp, "<shop>\n");
@fwrite($fp, "<name>".yandex_text2xml("irsapshop", true)."</name>\n");
@fwrite($fp, "<company>irsapshop.ru</company>\n");
@fwrite($fp, "<url>https://irsapshop.ru</url>\n");

@fwrite($fp, "<currencies>\n");
@fwrite($fp, "<currency id=\"RUB\" rate=\"1\"/>\n");
@fwrite($fp, "</currencies>\n");


@fwrite($fp, "<delivery-options>\n");
@fwrite($fp, "<option cost=\"700\" days=\"1-2\"/>\n");
@fwrite($fp, "</delivery-options>\n");

@fwrite($fp, "<categories>\n");
@fwrite($fp, $strTmpCat);
@fwrite($fp, "</categories>\n");

@fwrite($fp, "<offers>\n");
@fwrite($fp, $strTmpOff);
@fwrite($fp, "</offers>\n");

@fwrite($fp, "</shop>\n");
@fwrite($fp, "</yml_catalog>\n");
fclose($fp);
			

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>